package com.hyphenate.easeui.widget.chatrow;

import android.content.Context;
import android.content.Intent;
import android.text.Spannable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMMessage.ChatType;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.easeui.R;
import com.hyphenate.easeui.ui.LookAppointMentActicity;
import com.hyphenate.easeui.utils.EaseSmileUtils;
import com.hyphenate.exceptions.HyphenateException;

import java.util.Arrays;
import java.util.List;

public class EaseChatRowCustomer extends EaseChatRow {
    private TextView contentView;
    private TextView tv_cardtime;
    private TextView tv_poi;
//    private ImageView iv_cardhead;

    private LinearLayout ll_click;
    private String toChatUsername;

    public EaseChatRowCustomer(Context context, EMMessage message, int position, BaseAdapter adapter, String toChatUsername) {
        super(context, message, position, adapter);
        this.toChatUsername = toChatUsername;
        this.message = message;
    }

    @Override
    protected void onInflateView() {
        inflater.inflate(message.direct() == EMMessage.Direct.RECEIVE ? R.layout.customer_chatrow_recive : R.layout.customer_chatrow_send, this);
        initCustomerView();
    }


    private String cardTime;
    private String cardPoint;
    private String EMurl;
    private String cardName;


    private void initCustomerView() {

//        iv_cardhead = (ImageView) findViewById(R.id.iv_cardhead);
        tv_cardtime = (TextView) findViewById(R.id.tv_cardtime);
        tv_poi = (TextView) findViewById(R.id.tv_poi);
        ll_click = (LinearLayout) findViewById(R.id.ll_click);

        try {
            cardTime = message.getStringAttribute("cardTime");
            cardPoint = message.getStringAttribute("cardPoint");
            EMurl = message.getStringAttribute("EMurl");
            Log.i("EMurl","EaseChatRowCustomer------>"+EMurl);
            cardName = message.getStringAttribute("cardName");
        } catch (HyphenateException e) {
            e.printStackTrace();
        }


        tv_poi.setText(cardPoint);
        if (!TextUtils.isEmpty(cardTime)) {
            List<String> _list = Arrays.asList(cardTime.split(","));
            tv_cardtime.setText(_list.get(0));
        }
//        Glide.with(context).load(EMurl).into(iv_cardhead);


        ll_click.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LookAppointMentActicity.class);
                intent.putExtra("cardName", cardName);
                intent.putExtra("cardPoint", cardPoint);
                intent.putExtra("cardTime", cardTime);
                intent.putExtra("EMurl", EMurl);
                intent.putExtra("cardUsername", toChatUsername);
                context.startActivity(intent);
            }
        });
    }


    @Override
    protected void onFindViewById() {
        contentView = (TextView) findViewById(R.id.tv_chatcontent);
    }

    @Override
    public void onSetUpView() {
        EMTextMessageBody txtBody = (EMTextMessageBody) message.getBody();
        Spannable span = EaseSmileUtils.getSmiledText(context, txtBody.getMessage());
        // 设置内容
        contentView.setText(span, TextView.BufferType.SPANNABLE);

        handleTextMessage();
    }

    protected void handleTextMessage() {
        if (message.direct() == EMMessage.Direct.SEND) {
            setMessageSendCallback();
            switch (message.status()) {
                case CREATE:
                    progressBar.setVisibility(View.GONE);
                    statusView.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    progressBar.setVisibility(View.GONE);
                    statusView.setVisibility(View.GONE);
                    break;
                case FAIL:
                    progressBar.setVisibility(View.GONE);
                    statusView.setVisibility(View.VISIBLE);
                    break;
                case INPROGRESS:
                    progressBar.setVisibility(View.VISIBLE);
                    statusView.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        } else {
            if (!message.isAcked() && message.getChatType() == ChatType.Chat) {
                try {
                    EMClient.getInstance().chatManager().ackMessageRead(message.getFrom(), message.getMsgId());
                } catch (HyphenateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onUpdateView() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onBubbleClick() {
        // TODO Auto-generated method stub

    }


}

