package com.hyphenate.easeui.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/10/27.
 */
public class Constant {

    public static final String MINGPIAN_TYPE = "myExtType";

    private static String url;
    private static String name;

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Constant.name = name;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Constant.url = url;
    }


    private static Map<String, String> map;

    public static String getNickName(String userId) {
        if (map == null) {
            return userId;
        }
        return map.get(userId);
    }

    public static void setNickName(String userId, String nickName) {
        map = new HashMap<>();
        map.put(userId, nickName);
    }
}
