package com.hyphenate.easeui.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.hyphenate.easeui.R;
import com.hyphenate.easeui.utils.ScrollListUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2016/10/25.
 */
public class LookAppointMentActicity extends Activity {


    private ListView lv_addtime;
    private TextView tv_create;
    private TextView tv_poi;

    private String cardUsername;
    private String EMurl;
    private String cardTime;
    private String cardName;
    private String cardPoint;


    private List<String> list_time = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiy_lookappointment);

        lv_addtime = (ListView) findViewById(R.id.lv_addtime);
        tv_poi = (TextView) findViewById(R.id.tv_poi);
        tv_create = (TextView) findViewById(R.id.tv_create);

        if (getIntent().hasExtra("cardUsername")) {
            cardUsername = getIntent().getStringExtra("cardUsername");
        }
        if (getIntent().hasExtra("EMurl")) {
            EMurl = getIntent().getStringExtra("EMurl");
            Log.i("EMurl","LookAppointMentActicity------>"+EMurl);
        }
        if (getIntent().hasExtra("cardTime")) {
            cardTime = getIntent().getStringExtra("cardTime");
            List<String> _list = Arrays.asList(cardTime.split(","));
            list_time.addAll(_list);
            initTimeListView();
        }
        if (getIntent().hasExtra("cardName")) {
            cardName = getIntent().getStringExtra("cardName");
        }
        if (getIntent().hasExtra("cardPoint")) {
            cardPoint = getIntent().getStringExtra("cardPoint");

            if (!TextUtils.isEmpty(cardPoint)) {
                tv_poi.setText(cardPoint);
            }
        }



        initHead();

    }

    private void initTimeListView() {

        AppointMentAdapter appointMentAdapter = new AppointMentAdapter();
        lv_addtime.setAdapter(appointMentAdapter);
        ScrollListUtil.setListViewHeightBasedOnChildren(lv_addtime);
    }

    private void initHead() {

        tv_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    class AppointMentAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return list_time.size();
        }

        @Override
        public Object getItem(int position) {
            return list_time.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(LookAppointMentActicity.this).inflate(R.layout.item_lookappoint_time, null);
                viewHolder.tv_item_time = (TextView) convertView.findViewById(R.id.tv_item_time);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tv_item_time.setText(list_time.get(position));
            return convertView;
        }

        class ViewHolder {
            TextView tv_item_time;
        }
    }
}
