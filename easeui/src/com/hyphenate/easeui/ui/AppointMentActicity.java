package com.hyphenate.easeui.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.R;
import com.hyphenate.easeui.common.Constant;
import com.hyphenate.easeui.model.MingPianConstant;
import com.hyphenate.easeui.utils.DateUtil;
import com.hyphenate.easeui.utils.ScrollListUtil;
import com.hyphenate.easeui.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/10/25.
 */
public class AppointMentActicity extends Activity {

    private ListView lv_addtime;
    private ListView lv_addpoint;
    private EditText et_additempoi;

    private String cardUsername;
    private String EMurl;
    private TextView tv_create;

    private List<String> list_time = new ArrayList<>();
    private List<String> list_point = new ArrayList<>();


    private AppointMentAdapter appointMentAdapter;

    private PopupWindow popupWindow;
    private PopupWindow popupWindowHour;

    private final int VALUE_YEAR = 1;
    private final int VALUE_MOUTH = 2;
    private final int VALUE_DAY = 3;
    private final int VALUE_HOUR = 4;

    private String remindStartYear;
    private String remindStartMouth;
    private String remindStartDay;
    private String remindStartHour;

    private String sendTime;
    private String sendPoi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiy_appointment);

        if (getIntent().hasExtra("cardUsername")) {
            cardUsername = getIntent().getStringExtra("cardUsername");
        }
        if (getIntent().hasExtra("EMurl")) {
            EMurl = getIntent().getStringExtra("EMurl");
        }

        lv_addtime = (ListView) findViewById(R.id.lv_addtime);
        lv_addpoint = (ListView) findViewById(R.id.lv_addtime);

        initHead();
        initBodyView();
        initTimeListView();
        initPoiListView();


    }

    private void initBodyView() {
        findViewById(R.id.tv_additem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPopuptWindow(v);
            }
        });

        et_additempoi = (EditText) findViewById(R.id.et_additempoi);
        et_additempoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_additempoi.setCursorVisible(true);
            }
        });
    }

    private void initTimeListView() {
        appointMentAdapter = new AppointMentAdapter();
        lv_addtime.setAdapter(appointMentAdapter);
        ScrollListUtil.setListViewHeightBasedOnChildren(lv_addtime);
    }

    private void initPoiListView() {
        appointMentAdapter = new AppointMentAdapter();
        lv_addpoint.setAdapter(appointMentAdapter);
        ScrollListUtil.setListViewHeightBasedOnChildren(lv_addpoint);
    }


    private void initHead() {
        tv_create = (TextView) findViewById(R.id.tv_create);
        tv_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InfoIsNull()) {
                    EMMessage message = EMMessage.createTxtSendMessage("医生发起预约", cardUsername);
                    message.setAttribute(MingPianConstant.myExtType, "myExtType");
                    message.setAttribute("EMurl", Constant.getUrl());
                    message.setAttribute("EMNickName", Constant.getName());
                    message.setAttribute("cardName", "cardName");
                    message.setAttribute("cardPoint", sendPoi);
                    message.setAttribute("cardTime", sendTime);
                    EMClient.getInstance().chatManager().sendMessage(message);
                    finish();
                }

            }
        });
    }

    private boolean InfoIsNull() {
        sendPoi = et_additempoi.getText().toString().trim();
        StringBuilder stringBuilder = new StringBuilder();

        if (list_time.size() > 0) {
            for (int i = 0; i < list_time.size(); i++) {
                stringBuilder.append(list_time.get(i) + "点,");
            }
            sendTime = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);
        }

        if (list_time.size() == 0) {
            return false;
        } else if (TextUtils.isEmpty(sendPoi)) {
            return false;
        }

        return true;
    }

    class AppointMentAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return list_time.size();
        }

        @Override
        public Object getItem(int position) {
            return list_time.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(AppointMentActicity.this).inflate(R.layout.item_appoint_time, null);
                viewHolder.tv_item_time = (TextView) convertView.findViewById(R.id.tv_item_time);
                viewHolder.re_del = (RelativeLayout) convertView.findViewById(R.id.re_del);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.re_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_time.remove(position);
                    initTimeListView();
                }
            });
            viewHolder.tv_item_time.setText(list_time.get(position));
            return convertView;
        }

        class ViewHolder {
            TextView tv_item_time;
            RelativeLayout re_del;
        }
    }


    private void initPopuptWindow(final View view) {
        View popupWindow_view = getLayoutInflater().inflate(R.layout.popwindow_wheelview, null, false);
        popupWindow = new PopupWindow(popupWindow_view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        initWheelView(popupWindow_view);

        popupWindow_view.findViewById(R.id.bt_commit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPopuptHour(view);
                popupWindow.dismiss();
            }
        });

        popupWindow_view.findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
    }

    private void initWheelView(View popupWindow_view) {
        WheelView wv_year = (WheelView) popupWindow_view.findViewById(R.id.wv_year);
        WheelView wv_mouth = (WheelView) popupWindow_view.findViewById(R.id.wv_mouth);
        WheelView wv_day = (WheelView) popupWindow_view.findViewById(R.id.wv_day);

        wv_year.setData(getStartData(VALUE_YEAR));
        wv_mouth.setData(getStartData(VALUE_MOUTH));
        wv_day.setData(getStartData(VALUE_DAY));

        String string_mouth = DateUtil.longToDateString(DateUtil.getDaysAfter(0), "MM");
        String string_day = DateUtil.longToDateString(DateUtil.getDaysAfter(0), "dd");

        wv_year.setDefault(0);
        wv_mouth.setDefault(Integer.parseInt(string_mouth) - 1);
        wv_day.setDefault(Integer.parseInt(string_day) - 1);

        selText(wv_year, VALUE_YEAR);
        selText(wv_mouth, VALUE_MOUTH);
        selText(wv_day, VALUE_DAY);


        remindStartYear = wv_year.getSelectedText();
        remindStartMouth = wv_mouth.getSelectedText();
        remindStartDay = wv_day.getSelectedText();

    }

    private ArrayList<String> getStartData(int start) {
        ArrayList<String> list = new ArrayList<>();
        long date;
        switch (start) {
            case VALUE_YEAR:
                date = DateUtil.getDaysAfter(0);
                String str_year = DateUtil.longToDateString(date, "yyyy");
                int year = Integer.parseInt(str_year);
                list.add((year) + "");
                list.add((year + 1) + "");
                break;
            case VALUE_MOUTH:
                for (int i = 1; i < 13; i++) {
                    list.add(String.format("%02d", i));
                }
                break;
            case VALUE_DAY:
                for (int i = 1; i < 32; i++) {
                    list.add(String.format("%02d", i));
                }
                break;
            case VALUE_HOUR:
                for (int i = 1; i < 25; i++) {
                    list.add(String.format("%02d", i));
                }
                break;
        }
        return list;
    }

    private void selText(WheelView wv, final int ymd) {
        wv.setOnSelectListener(new WheelView.OnSelectListener() {
            @Override
            public void endSelect(int id, String text) {
                if (ymd == VALUE_YEAR) {
                    remindStartYear = text;
                } else if (ymd == VALUE_MOUTH) {
                    remindStartMouth = text;
                } else if (ymd == VALUE_DAY) {
                    remindStartDay = text;
                } else if (ymd == VALUE_HOUR) {
                    remindStartHour = text;
                }
            }

            @Override
            public void selecting(int id, String text) {
            }
        });
    }


    private void initPopuptHour(View v) {
        View popupWindow_view = getLayoutInflater().inflate(R.layout.pop_item, null, false);
        popupWindowHour = new PopupWindow(popupWindow_view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        initWheelViewHour(popupWindow_view);

        popupWindow_view.findViewById(R.id.bt_commit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String time = remindStartYear + "-" + remindStartMouth + "." + remindStartDay + "    " + remindStartHour;
                list_time.add(time);
                initTimeListView();
                popupWindowHour.dismiss();
            }
        });

        popupWindow_view.findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPopuptWindow(v);
                popupWindowHour.dismiss();
            }
        });
        popupWindowHour.showAtLocation(v, Gravity.BOTTOM, 0, 0);
    }

    private void initWheelViewHour(View popupWindow_view) {
        WheelView wv_hour = (WheelView) popupWindow_view.findViewById(R.id.wv_hour);
        wv_hour.setData(getStartData(VALUE_HOUR));
        String string_hour = DateUtil.longToDateString(DateUtil.getDaysAfter(0), "HH");

        wv_hour.setDefault(Integer.parseInt(string_hour) - 1);
        selText(wv_hour, VALUE_HOUR);
        remindStartHour = wv_hour.getSelectedText();
    }

}
