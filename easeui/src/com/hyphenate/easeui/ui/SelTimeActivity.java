package com.hyphenate.easeui.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hyphenate.easeui.R;

/**
 * Created by Administrator on 2016/10/28.
 */
public class SelTimeActivity extends Activity {

    private EditText et_year;
    private EditText et_mouth;
    private EditText et_day;
    private EditText et_hour;
    private Button bt_commit;

    private String time;

    public static final int FROM_APPOINT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seltime);


        findViewById(R.id.bt_commit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getDate()){

                    setResult(FROM_APPOINT,getIntent().putExtra("time",time));
                    finish();
                }
            }
        });
    }

    private boolean getDate() {
//        private EditText et_year;
//        private EditText et_mouth;
//        private EditText et_day;
//        private EditText et_hour;
        String year = et_year.getText().toString().trim();
        String mouth = et_mouth.getText().toString().trim();
        String day = et_day.getText().toString().trim();
        String hour = et_hour.getText().toString().trim();
        if(TextUtils.isEmpty(year)){
            Toast.makeText(SelTimeActivity.this,"请填写年份",Toast.LENGTH_LONG).show();
            return false;
        }else if(TextUtils.isEmpty(mouth)){
            Toast.makeText(SelTimeActivity.this,"请填写月份",Toast.LENGTH_LONG).show();
            return false;
        }else if(TextUtils.isEmpty(day)){
            Toast.makeText(SelTimeActivity.this,"请填写日期",Toast.LENGTH_LONG).show();
            return false;
        }else if(TextUtils.isEmpty(hour)){
            Toast.makeText(SelTimeActivity.this,"请填写小时",Toast.LENGTH_LONG).show();
            return false;
        }

        time = year+"-"+mouth+"."+day+"    "+hour;
        return true;
    }
}
