package com.jkb.enjoyor.baledoctor.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.PatientRecord;
import com.jkb.enjoyor.baledoctor.utils.DateUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by YuanYuan on 2016/9/27.
 */
public class PatientRecordAdapter extends CommAdapter<PatientRecord> {

    private Context context;
    public PatientRecordAdapter(Context context, List<PatientRecord> datas, int layoutId) {
        super(context, datas, layoutId);
        this.context = context;
    }
    @Override
    public void convert(ViewHolder holder, PatientRecord patientRecord) {
        ImageView imageView = (ImageView) holder.getmConvertView().findViewById(R.id.home_fg_item_picture);
        if (!StringUtils.isEmpty(patientRecord.getUserName())) {
            holder.setText(R.id.home_fg_item_name, patientRecord.getUserName());
        }else {
            holder.setText(R.id.home_fg_item_name, "未知用户");
        }
        if (!StringUtils.isEmpty(patientRecord.getSex())) {
            holder.setText(R.id.home_fg_item_sex, ""+patientRecord.getSex());
        }else {
            holder.setText(R.id.home_fg_item_sex, "性别");
        }
        if (patientRecord.getAge() != null) {
            holder.setText(R.id.home_fg_item_age, patientRecord.getAge() + "岁");
        }else {
            holder.setText(R.id.home_fg_item_age, "18岁");
        }
        if (!StringUtils.isEmpty(patientRecord.getRecordTime())) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    java.util.Date dt = sdf.parse(patientRecord.getRecordTime());
                    String date = DateUtil.dateToString(dt, "yyyy-MM-dd");
                    holder.setText(R.id.home_fg_item_time, date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

        }
        if (patientRecord.getType() != null) {
            if (patientRecord.getType() == 1) {
                holder.setImageResource(R.id.home_fg_item_logo, R.mipmap.tijian);
            } else if (patientRecord.getType() == 2) {
                holder.setImageResource(R.id.home_fg_item_logo, R.mipmap.zice);
            } else if (patientRecord.getType() == 3) {
                holder.setImageResource(R.id.home_fg_item_logo, R.mipmap.suishouji);
            }
        }
        if (!StringUtils.isEmpty(patientRecord.getHeadImg())){
//            holder.setImageURL(R.id.home_fg_item_picture,Constant.TEST_URL+patientRecord.getHeadImg());
            ImageLoader.getInstance().displayImage(Constant.TEST_URL+patientRecord.getHeadImg(),imageView, EnjoyorApplication.option);
        }else {
            holder.setImageResource(R.id.home_fg_item_picture,R.mipmap.touxiang);
        }
    }
}
