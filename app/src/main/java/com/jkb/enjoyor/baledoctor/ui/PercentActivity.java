package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.entitry.DoctorInfo;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/8.
 * 个人中心页面
 */
public class PercentActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.iv_userhead)
    ImageView iv_userhead;
    @Bind(R.id.personal_lin_add)
    LinearLayout personal_lin_add;
    @Bind(R.id.personal_lin_setting)
    RelativeLayout personal_lin_setting;
    @Bind(R.id.percent_rel_suggest)
    RelativeLayout percent_rel_suggest;
    @Bind(R.id.percent_data_tv_name)
    TextView percent_data_tv_name;
    @Bind(R.id.percent_data_tv_domain)
    TextView percent_data_tv_domain;
    @Bind(R.id.percent_data_tv_hospital)
    TextView percent_data_tv_hospital;
    @Bind(R.id.percent_data_tv_part)
    TextView percent_data_tv_part;
    @Bind(R.id.percent_data_tv_address)
    TextView percent_data_tv_address;
    @Bind(R.id.percent_data_tv_duty)
    TextView percent_data_tv_duty;
    @Bind(R.id.percent_back)
    RelativeLayout percent_back;
    private UserInfo userInfo;
    private DoctorInfo doctorInfo;

    public static final int FROM_HOME = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.percent_ac_layout);
        ButterKnife.bind(this);
        initEvent();
        //初始化医生信息
        if ((userInfo = EnjoyorApplication.getInstance().getDBHelper().getUser()) != null) {
            initDoctorDate();
        }
    }

    private void initDoctorDate() {
        Subscription s;
        s = Server.HomeProtocolBuild(PercentActivity.this).getDoctorInfo(userInfo.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof DoctorInfo) {
            doctorInfo = (DoctorInfo) o;
            Log.d("doctorInfo", doctorInfo.toString());
            EnjoyorApplication.getInstance().getDBHelper().saveDoctorInfo(doctorInfo);
            initDoctorHeadInfo();
        }
    }

    private void initDoctorHeadInfo() {
        String name = StringUtils.finalName(doctorInfo.getName(), doctorInfo.getLoginName(), doctorInfo.getPhoneNumber());
        percent_data_tv_name.setText(name);
        String headimage = StringUtils.finalHeadImage(doctorInfo.getHeadImg(), doctorInfo.getDbHeadImage());
        if (!StringUtils.isEmpty(headimage)) {
            ImageLoader.getInstance().displayImage(headimage,iv_userhead,EnjoyorApplication.option);
//            Glide.with(PercentActivity.this).load(headimage).into(iv_userhead);
        }

        percent_data_tv_domain.setText(doctorInfo.getDomain());
        percent_data_tv_hospital.setText(doctorInfo.getHospital());
        percent_data_tv_part.setText(doctorInfo.getDept());
        percent_data_tv_address.setText(doctorInfo.getRegionName());
        percent_data_tv_duty.setText(doctorInfo.getProfTitle());
    }

    private void initEvent() {
        personal_lin_add.setOnClickListener(this);
        personal_lin_setting.setOnClickListener(this);
        percent_rel_suggest.setOnClickListener(this);
        percent_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.personal_lin_add:
//                ToastUtils.showToast(this, "您点击了添加其他资料按钮");
                Intent intent_data = new Intent(this, PercentDataActivity.class);
                startActivityForResult(intent_data, PercentDataActivity.FROM_PERCENT);
                break;
            case R.id.personal_lin_setting:
//                ToastUtils.showToast(this, "您点击了设置界面");
                Intent intent_setting = new Intent(this, SettingActivity.class);
                startActivity(intent_setting);
                break;
            case R.id.percent_rel_suggest:
//                ToastUtils.showToast(this, "您点击了意见反馈");
                Intent intent_suggest = new Intent(PercentActivity.this, SuggestActivity.class);
                startActivity(intent_suggest);
                break;
            case R.id.percent_back:
                Intent intent = getIntent();
                setResult(FROM_HOME, intent);
//                Intent intent = new Intent(PercentActivity.this,MainActivity.class);
//                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == PercentDataActivity.FROM_PERCENT) {
                if ((userInfo = EnjoyorApplication.getInstance().getDBHelper().getUser()) != null) {
                    initDoctorDate();
                }
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = getIntent();
            setResult(FROM_HOME, intent);
//            Intent intent = new Intent(PercentActivity.this,MainActivity.class);
//            startActivity(intent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
