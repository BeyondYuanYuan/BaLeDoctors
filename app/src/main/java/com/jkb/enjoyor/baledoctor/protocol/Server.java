package com.jkb.enjoyor.baledoctor.protocol;


import android.content.Context;

import com.jkb.enjoyor.baledoctor.common.Constant;

import retrofit.RestAdapter;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class Server {

    private static OtherProtocol otherProtocol;
    private static HomeProtocol homeProtocol;
    private static MineProtocol mineProtocol;
    private static GuideProtocol guideProtocol;

    public static OtherProtocol otherProtocolBuild(final Context context) {
        if (otherProtocol == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constant.TEST_URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            otherProtocol = restAdapter.create(OtherProtocol.class);
        }
        return otherProtocol;
    }

    public static HomeProtocol HomeProtocolBuild(final Context context) {
        if (homeProtocol == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constant.TEST_URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            homeProtocol = restAdapter.create(HomeProtocol.class);
        }
        return homeProtocol;
    }

    public static MineProtocol MineProtocolBuild(final Context context) {
        if (mineProtocol == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constant.TEST_URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            mineProtocol = restAdapter.create(MineProtocol.class);
        }
        return mineProtocol;
    }

    public static GuideProtocol GuideProtocolBuild(final Context context) {
        if (guideProtocol == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constant.TEST_URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            guideProtocol = restAdapter.create(GuideProtocol.class);
        }
        return guideProtocol;
    }
}
