package com.jkb.enjoyor.baledoctor.common;

import android.content.Context;

import com.jkb.enjoyor.baledoctor.utils.SharePreferenceUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.SortModel;

import java.util.List;

/**
 * Created by YuanYuan on 2016/8/24.
 */
public class BaseDate {
    public static Long accountId;

    public static  List<SortModel> EMList;

    public static List<SortModel> getEMList() {
        return EMList;
    }

    public static void setEMList(List<SortModel> EMList) {
        BaseDate.EMList = EMList;
    }

    public static Long getSessionId(Context context) {
        if (StringUtils.isBlank("" + accountId))
            accountId = new SharePreferenceUtil(context).getUserId();
        return accountId;
    }

    public static void setSessionId(Context context, Long _accountId) {
        accountId = _accountId;
        new SharePreferenceUtil(context).setUserId(_accountId);
    }

    private static int toolbarHeight;

    public static int getToolbarHeight() {
        return toolbarHeight;
    }

    public static void setToolbarHeight(int toolbarHeight) {
        BaseDate.toolbarHeight = toolbarHeight;
    }
}
