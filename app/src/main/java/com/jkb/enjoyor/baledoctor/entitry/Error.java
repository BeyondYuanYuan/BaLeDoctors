package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by YuanYuan on 2016/9/13.
 * 返回值错误类型
 */
public class Error {

    String code;
    String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
