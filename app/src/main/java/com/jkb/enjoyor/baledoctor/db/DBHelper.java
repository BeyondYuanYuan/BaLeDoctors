package com.jkb.enjoyor.baledoctor.db;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.jkb.enjoyor.baledoctor.entitry.DoctorInfo;
import com.jkb.enjoyor.baledoctor.entitry.EMModel;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;

import java.sql.SQLException;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class DBHelper {
    private DatabaseHelper mDBHelper;

    public DBHelper(Context context) {
        mDBHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    public boolean setEM(EMModel emModel) {
        clearEM();
        try {
            Dao<EMModel, Integer> dao = mDBHelper.getDao(EMModel.class);
            dao.createOrUpdate(emModel);
            Log.i("sql", "DBHelper--->>---EMModel success");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public EMModel getEM() {

        try {
            Dao<EMModel, Integer> dao = mDBHelper.getDao(EMModel.class);
            EMModel emModel = dao.queryForId(1);
            if (emModel == null) {
                Log.i("sql", "DBHelper emModel null");
                return null;
            }
            Log.i("sql", "emModel toString：" + emModel.toString());
            return emModel;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean clearEM() {
        try {
            Dao<EMModel, Integer> dao = mDBHelper.getDao(EMModel.class);
            dao.delete(getEM());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean saveUser(UserInfo user) {
//        clearUser();
        user.setUserId(1);
        try {
            Dao<UserInfo, Integer> dao = mDBHelper.getDao(UserInfo.class);
            dao.createOrUpdate(user);
            Log.i("sql", "DBHelper--->>---user saveUser success");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public UserInfo getUser() {

        try {
            Dao<UserInfo, Integer> dao = mDBHelper.getDao(UserInfo.class);
            UserInfo user = dao.queryForId(1);
            if (user == null) {
                Log.i("sql", "DBHelper getUser null");
                return null;
            }
            Log.i("sql", "getUser toString：" + user.toString());
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean clearUser() {
        try {
            Dao<UserInfo, Integer> dao = mDBHelper.getDao(UserInfo.class);
            dao.delete(getUser());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    //    医生信息
    public boolean saveDoctorInfo(DoctorInfo user) {
        user.setInfoid(1);
        try {
            Dao<DoctorInfo, Integer> dao = mDBHelper.getDao(DoctorInfo.class);
            dao.createOrUpdate(user);
            Log.i("sql", "DBHelper--->>---saveDoctorInfo success");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public DoctorInfo getDoctorInfo() {

        try {
            Dao<DoctorInfo, Integer> dao = mDBHelper.getDao(DoctorInfo.class);
            DoctorInfo user = dao.queryForId(1);
            if (user == null) {
                Log.i("sql", "DBHelper getsaveDoctorInfo null");
                return null;
            }
            Log.i("sql", "getsaveDoctorInfo toString：" + user.toString());
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean clearDoctorInfo() {
        try {
            Dao<DoctorInfo, Integer> dao = mDBHelper.getDao(DoctorInfo.class);
            dao.delete(getDoctorInfo());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
