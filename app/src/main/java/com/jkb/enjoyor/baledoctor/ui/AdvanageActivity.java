package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/9.
 * 擅长领域页面
 */
public class AdvanageActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.tv_right)
    TextView tv_right;
    @Bind(R.id.advantage_et)
    EditText advantage_et;
    @Bind(R.id.advantage_tv)
    TextView advantage_tv;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advantage_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
        domain();
    }

    private void domain() {
        advantage_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.d("wyy---beforeTextChanged", s.length() + "");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("wyy---onTextChanged", s.length() + "");
                advantage_tv.setText(s.length() + "/30");
                if (s.length() > 30) {
                    ToastUtils.showToast(AdvanageActivity.this, "输入字符长度超过限制");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void initEvent() {
        tv_right.setText("保存");
        tv_right.setVisibility(View.VISIBLE);
        navigation_name.setText("擅长领域");
        re_back.setOnClickListener(this);
        tv_right.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.tv_right:
                if (!StringUtils.isEmpty(advantage_et.getText().toString().trim())){
                    if (checkNameChese(advantage_et.getText().toString().trim())){
                        commitdomain();
                    }else {
                        ToastUtils.showToast(AdvanageActivity.this,"擅长领域只能是中文！");
                    }
                }else {
                    ToastUtils.showToast(AdvanageActivity.this,"擅长领域数据不能为空！");
                }
                break;
        }
    }

    private void commitdomain() {
        Intent intent = getIntent();
        intent.putExtra("domain",advantage_et.getText().toString().trim());
        setResult(Constant.DOMAIN,intent);
        finish();
    }
    /*
* 判断字符串是不是中文*/
    public boolean checkNameChese(String name) {
        boolean res = true;
        char[] cTemp = name.toCharArray();
        for (int i = 0; i < name.length(); i++) {
            if (!isChinese(cTemp[i])) {
                res = false;
                break;
            }
        }
        return res;

    }

    public boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }
}
