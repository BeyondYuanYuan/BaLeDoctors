package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.common.Constant;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/9.
 */
public class SexActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.tv_right)
    TextView tv_right;
    @Bind(R.id.sex_rl_mail)
    RelativeLayout sex_rl_mail;
    @Bind(R.id.sex_male_delete)
    ImageView sex_male_delete;
    @Bind(R.id.sex_female_)
    RelativeLayout sex_female_;
    @Bind(R.id.sex_female_delete)
    ImageView sex_female_delete;
    boolean TAG = true;
    String sex;
    int sexinfo = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sex_ac_layout);
        ButterKnife.bind(this);
        initClick();
        initEvent();
    }

    private void initEvent() {
        sex_rl_mail.setOnClickListener(this);
        sex_female_.setOnClickListener(this);
        tv_right.setOnClickListener(this);
        re_back.setOnClickListener(this);
    }

    private void initClick() {
        navigation_name.setText("性别");
        tv_right.setText("保存");
        tv_right.setVisibility(View.VISIBLE);
//        sex_male_delete.setVisibility(View.GONE);
        sex_female_delete.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        int key = v.getId();
        switch (key) {
            case R.id.sex_rl_mail:
                sex_male_delete.setVisibility(View.VISIBLE);
                sex_female_delete.setVisibility(View.GONE);
                TAG = true;
                break;
            case R.id.sex_female_:
                sex_male_delete.setVisibility(View.GONE);
                sex_female_delete.setVisibility(View.VISIBLE);
                TAG = false;
                break;
            case R.id.tv_right:
                saveSex();
                break;
            case R.id.re_back:
                finish();
                break;
        }
    }

    private void saveSex() {
        if (TAG) {
            sex = "男";
            sexinfo = 1;
        } else {
            sex = "女";
            sexinfo = 2;
        }

        Intent intent = getIntent();
        intent.putExtra("sex", sex);
        intent.putExtra("sexinfo", sexinfo+"");
        setResult(Constant.SEX, intent);
        finish();
    }
}
