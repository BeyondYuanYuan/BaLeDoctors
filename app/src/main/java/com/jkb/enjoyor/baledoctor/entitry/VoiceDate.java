package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by YuanYuan on 2016/11/3.
 */
public class VoiceDate {
    /**
     * ID : 2178
     * createTime : 2016-10-27 14:33:27
     * createUser : null
     * fileName : voice.amr
     * fileOrgs : 3
     * filePath : files/app/2016/10//4dd15141-b667-4add-b648-ec2b53118283.amr
     * fileSize : 2438
     * fileType : 0
     * states : 1
     */

    private int ID;
    private String createTime;
    private Object createUser;
    private String fileName;
    private int fileOrgs;
    private String filePath;
    private int fileSize;
    private int fileType;
    private int states;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getFileOrgs() {
        return fileOrgs;
    }

    public void setFileOrgs(int fileOrgs) {
        this.fileOrgs = fileOrgs;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public int getStates() {
        return states;
    }

    public void setStates(int states) {
        this.states = states;
    }
//    String filePath;//   "filePath": "files/unknow/44a65a9e-82be-4c43-a78d-7255f1cf53da.AMR",
//
//    public String getFilePath() {
//        return filePath;
//    }
//
//    public void setFilePath(String filePath) {
//        this.filePath = filePath;
//    }
}
