package com.jkb.enjoyor.baledoctor.entitry;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by YuanYuan on 2016/9/27.
 */
public class PatientRecord implements Parcelable {

    /**
     * accountId : 9
     * addressName : 银江软件园检测G点
     * age : 24
     * compLogo : files/res/app/start/124c6a53-134b-4239-af41-57a5a1a62f4c.JPG
     * compName : JKB
     * doctorState : 0
     * headImg : 
     * id : 4
     * idCardpic : 
     * nickName : 
     * recordId : 26
     * recordNo : 20160316160019
     * recordTime : 2016-05-17 16:04:46
     * reportTime : 2016-09-21 09:43:05
     * sex : 男
     * type : 1
     * userId : 78
     * userName : 神
     */

    private Integer accountId;
    private String addressName;
    private Integer age;
    private String compLogo;
    private String compName;
    private Integer doctorState;
    private String headImg;
    private Integer id;
    private String idCardpic;
    private String nickName;
    private Integer recordId;
    private String recordNo;
    private String recordTime;
    private String reportTime;
    private String sex;
    private Integer type;
    private Integer userId;
    private String userName;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "PatientRecord{" +
                "accountId=" + accountId +
                ", addressName='" + addressName + '\'' +
                ", age=" + age +
                ", compLogo='" + compLogo + '\'' +
                ", compName='" + compName + '\'' +
                ", doctorState=" + doctorState +
                ", headImg='" + headImg + '\'' +
                ", id=" + id +
                ", idCardpic='" + idCardpic + '\'' +
                ", nickName='" + nickName + '\'' +
                ", recordId=" + recordId +
                ", recordNo='" + recordNo + '\'' +
                ", recordTime='" + recordTime + '\'' +
                ", reportTime='" + reportTime + '\'' +
                ", sex='" + sex + '\'' +
                ", type=" + type +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                '}';
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCompLogo() {
        return compLogo;
    }

    public void setCompLogo(String compLogo) {
        this.compLogo = compLogo;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public Integer getDoctorState() {
        return doctorState;
    }

    public void setDoctorState(Integer doctorState) {
        this.doctorState = doctorState;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdCardpic() {
        return idCardpic;
    }

    public void setIdCardpic(String idCardpic) {
        this.idCardpic = idCardpic;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public String getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(String recordTime) {
        this.recordTime = recordTime;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.accountId);
        dest.writeString(this.addressName);
        dest.writeValue(this.age);
        dest.writeString(this.compLogo);
        dest.writeString(this.compName);
        dest.writeValue(this.doctorState);
        dest.writeString(this.headImg);
        dest.writeValue(this.id);
        dest.writeString(this.idCardpic);
        dest.writeString(this.nickName);
        dest.writeValue(this.recordId);
        dest.writeString(this.recordNo);
        dest.writeString(this.recordTime);
        dest.writeString(this.reportTime);
        dest.writeString(this.sex);
        dest.writeValue(this.type);
        dest.writeValue(this.userId);
        dest.writeString(this.userName);
    }

    public PatientRecord() {
    }

    protected PatientRecord(Parcel in) {
        this.accountId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.addressName = in.readString();
        this.age = (Integer) in.readValue(Integer.class.getClassLoader());
        this.compLogo = in.readString();
        this.compName = in.readString();
        this.doctorState = (Integer) in.readValue(Integer.class.getClassLoader());
        this.headImg = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idCardpic = in.readString();
        this.nickName = in.readString();
        this.recordId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.recordNo = in.readString();
        this.recordTime = in.readString();
        this.reportTime = in.readString();
        this.sex = in.readString();
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userName = in.readString();
    }

    public static final Parcelable.Creator<PatientRecord> CREATOR = new Parcelable.Creator<PatientRecord>() {
        @Override
        public PatientRecord createFromParcel(Parcel source) {
            return new PatientRecord(source);
        }

        @Override
        public PatientRecord[] newArray(int size) {
            return new PatientRecord[size];
        }
    };
}
