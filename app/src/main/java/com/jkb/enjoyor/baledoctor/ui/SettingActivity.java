package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hyphenate.chat.EMClient;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.utils.AppManagerUtil;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/9.
 * 个人中心设置界面
 */
public class SettingActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.setting_ac_rel_modifypwd)
    RelativeLayout setting_ac_rel_modifypwd;
    @Bind(R.id.setting_ac_rel_sectet)
    RelativeLayout setting_ac_rel_sectet;
    @Bind(R.id.setting_ac_rel_clearcach)
    RelativeLayout setting_ac_rel_clearcach;
    @Bind(R.id.setting_ac_rel_aboutour)
    RelativeLayout setting_ac_rel_aboutour;
    @Bind(R.id.setting_ac_rel_version)
    RelativeLayout setting_ac_rel_version;
    @Bind(R.id.setting_ac_rel_declear)
    RelativeLayout setting_ac_rel_declear;
    @Bind(R.id.setting_ac_exit)
    Button setting_ac_exit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initEvent() {
        re_back.setOnClickListener(this);
        setting_ac_rel_modifypwd.setOnClickListener(this);
        setting_ac_rel_sectet.setOnClickListener(this);
        setting_ac_rel_clearcach.setOnClickListener(this);
        setting_ac_rel_aboutour.setOnClickListener(this);
        setting_ac_rel_version.setOnClickListener(this);
        setting_ac_rel_declear.setOnClickListener(this);
        setting_ac_exit.setOnClickListener(this);
        navigation_name.setText("设置");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                ToastUtils.showToast(SettingActivity.this, "你点击了返回按钮");
                finish();
                break;
            case R.id.setting_ac_rel_modifypwd:
//                ToastUtils.showToast(SettingActivity.this, "你点击了修改密码按钮");
                Intent intent_modify = new Intent(SettingActivity.this, ModifyPwdActivity.class);
                startActivity(intent_modify);
                break;
            case R.id.setting_ac_rel_sectet:
//                ToastUtils.showToast(SettingActivity.this, "你点击了隐私按钮");
                break;
            case R.id.setting_ac_rel_clearcach:
//                ToastUtils.showToast(SettingActivity.this, "你点击了清理缓存按钮");
                showSnackBar("手机棒棒哒，继续保持哦！");
                break;
            case R.id.setting_ac_rel_aboutour:
//                ToastUtils.showToast(SettingActivity.this, "你点击了关于我们按钮");
                Intent intent_about = new Intent(SettingActivity.this, AboutOurActivity.class);
                startActivity(intent_about);
                break;
            case R.id.setting_ac_rel_version:
//                ToastUtils.showToast(SettingActivity.this, "你点击了版本信息按钮");
                Intent intent_version = new Intent(SettingActivity.this, VersionActivity.class);
                startActivity(intent_version);
                break;
            case R.id.setting_ac_rel_declear:
//                ToastUtils.showToast(SettingActivity.this, "你点击了申明按钮");
                Intent intent_agree = new Intent(SettingActivity.this, AgreementActivity.class);
                startActivity(intent_agree);
                break;
            case R.id.setting_ac_exit:
                if (EnjoyorApplication.getInstance().getDBHelper().clearUser()) {
                    EnjoyorApplication.getInstance().getDBHelper().clearDoctorInfo();
                    EMClient.getInstance().logout(true);
                    Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    AppManagerUtil.getAppManager().finishAllActivity();
                }
                break;
        }
    }
}
