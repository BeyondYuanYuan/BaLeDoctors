package com.jkb.enjoyor.baledoctor.ui;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mapapi.map.Text;
import com.bumptech.glide.Glide;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.GuideItem;
import com.jkb.enjoyor.baledoctor.entitry.PersonCreateGuide;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/10/18.
 */
public class EditGuideActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.editguid_ac_btn_send)
    Button editguid_ac_btn_send;
    @Bind(R.id.guide_ac_img_logo)
    ImageView guide_ac_img_logo;
    @Bind(R.id.guide_ac_tv_name)
    TextView guide_ac_tv_name;
    @Bind(R.id.guide_ac_tv_sex)
    TextView guide_ac_tv_sex;
    @Bind(R.id.guide_ac_tv_age)
    TextView guide_ac_tv_age;
    @Bind(R.id.guide_ac_tv_time)
    TextView guide_ac_tv_time;
    @Bind(R.id.guide_ac_tv_shuzhangya)
    TextView guide_ac_tv_shuzhangya;
    @Bind(R.id.guide_ac_img_bp_up)
    ImageView guide_ac_img_bp_up;
    @Bind(R.id.guide_ac_tv_shousuoya)
    TextView guide_ac_tv_shousuoya;
    @Bind(R.id.guide_ac_img_bp_down)
    ImageView guide_ac_img_bp_down;
    @Bind(R.id.guide_ac_tv_kongfu)
    TextView guide_ac_tv_kongfu;
    @Bind(R.id.guide_ac_img_kongfu)
    ImageView guide_ac_img_kongfu;
    @Bind(R.id.guide_ac_tv_wancanhou)
    TextView guide_ac_tv_wancanhou;
    @Bind(R.id.guide_ac_img_wancanhou)
    ImageView guide_ac_img_wancanhou;
    @Bind(R.id.guide_ac_img_shuiqian)
    ImageView guide_ac_img_shuiqian;
    @Bind(R.id.guide_ac_tv_shuiqian)
    TextView guide_ac_tv_shuiqian;
    @Bind(R.id.guide_ac_img_wancanqian)
    ImageView guide_ac_img_wancanqian;
    @Bind(R.id.guide_ac_tv_wancanqian)
    TextView guide_ac_tv_wancanqian;
    @Bind(R.id.guide_ac_tv_limit)
    TextView guide_ac_tv_limit;
    @Bind(R.id.guide_ac_tv_allenergency)
    TextView guide_ac_tv_allenergency;
    @Bind(R.id.guide_ac_tv_danbai)
    TextView guide_ac_tv_danbai;
    @Bind(R.id.guide_ac_tv_danbaizhi)
    TextView guide_ac_tv_danbaizhi;
    @Bind(R.id.guide_ac_li_danbaizhi)
    LinearLayout guide_ac_li_danbaizhi;
    @Bind(R.id.spori_item)
    LinearLayout spori_item;
    @Bind(R.id.guide_limit_tv)
    TextView guide_limit_tv;
    @Bind(R.id.guide_ac_tv_sportall)
    TextView guide_ac_tv_sportall;
    @Bind(R.id.guide_tv_baizhi)
    TextView guide_tv_baizhi;
    @Bind(R.id.guide_ac_tv_shiliangdanbaizhi)
    TextView guide_ac_tv_shiliangdanbaizhi;
    @Bind(R.id.guide_ac_li_limitdanbaizhi)
    LinearLayout guide_ac_li_limitdanbaizhi;
    @Bind(R.id.shanshi_item)
    LinearLayout shanshi_item;
    @Bind(R.id.guide_ac_tv_jkxjiao)
    TextView guide_ac_tv_jkxjiao;
    @Bind(R.id.xuanjiao_item)
    LinearLayout xuanjiao_item;
    @Bind(R.id.guide_ac_cb_limit)
    CheckBox guide_ac_cb_limit;
    @Bind(R.id.guide_ac_cb_danbaizhi)
    CheckBox guide_ac_cb_danbaizhi;
    @Bind(R.id.guide_ac_cb_sportlimit)
    CheckBox guide_ac_cb_sportlimit;
    @Bind(R.id.guide_ac_cb_sport_danbaizhi)
    CheckBox guide_ac_cb_sport_danbaizhi;
    @Bind(R.id.guide_ac_tv_beizhu)
    EditText guide_ac_tv_beizhu;

    StringBuilder stringbuder;
    private long accountId = -1;
    private String id = "";
    long reportId = -1;
    String remark = "";
    private String recordIds= "0,0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editguide_ac_layout);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        accountId = intent.getIntExtra("accountId", -1);
        initview();
        initClick();
        initData();
    }

    private void initData() {
//        指导患者个人资料，血压，血糖
        Subscription s;
        s = Server.HomeProtocolBuild(this).createguide(accountId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
//        膳食指导数据
        getguideItem(1);
//        运动指导
        getguideItem(2);
//        健康宣教
        getguideItem(3);
    }

    private void getguideItem(int i) {
        Subscription shanshi;
        shanshi = Server.HomeProtocolBuild(this).getGuideItem(i, "hypertension")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            shaishiWithResponse(i, data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(shanshi);

    }

    private void shaishiWithResponse(int i, BaseResponse<List<GuideItem>> data) {
        if (i == 1) {
            List<GuideItem> list = data.getData();
            if (list.size() >= 2) {
                if (list.get(0).getId() != null) {
                    guide_ac_cb_limit.setTag(list.get(0).getId() + ",");
                }
                if (list.get(1).getId() != null) {
                    guide_ac_cb_danbaizhi.setTag(list.get(1).getId() + ",");
                }
                if (!StringUtils.isEmpty(list.get(0).getKey())) {
                    guide_ac_tv_limit.setText(list.get(0).getKey());
                }
                if (!StringUtils.isEmpty(list.get(0).getVal())) {
                    guide_ac_tv_allenergency.setText(list.get(0).getVal());
                }
                if (!StringUtils.isEmpty(list.get(1).getKey())) {
                    guide_ac_tv_danbai.setText(list.get(1).getKey());
                }
                if (!StringUtils.isEmpty(list.get(1).getVal())) {
                    guide_ac_tv_danbaizhi.setText(list.get(1).getVal());
                }
            }
            if (list.size() == 1) {
                if (list.get(0).getId() != null) {
                    guide_ac_cb_limit.setTag(list.get(0).getId() + ",");
                }
                if (!StringUtils.isEmpty(list.get(0).getKey())) {
                    guide_ac_tv_limit.setText(list.get(0).getKey());
                }
                if (!StringUtils.isEmpty(list.get(0).getVal())) {
                    guide_ac_tv_allenergency.setText(list.get(0).getVal());
                }
                guide_ac_li_danbaizhi.setVisibility(View.GONE);
            }
            if (list.size() == 0) {
                spori_item.setVisibility(View.GONE);
            }
        }
        if (i == 2) {
            List<GuideItem> list = data.getData();
            if (list.size() == 2) {
                if (list.get(0).getId() != null) {
                    guide_ac_cb_sportlimit.setTag(list.get(0).getId() + ",");
                }
                if (list.get(1).getId() != null) {
                    guide_ac_cb_sport_danbaizhi.setTag(list.get(1).getId() + ",");
                }
                if (!StringUtils.isEmpty(list.get(0).getKey())) {
                    guide_limit_tv.setText(list.get(0).getKey());
                }
                if (!StringUtils.isEmpty(list.get(0).getVal())) {
                    guide_ac_tv_sportall.setText(list.get(0).getVal());
                }
                if (!StringUtils.isEmpty(list.get(1).getKey())) {
                    guide_tv_baizhi.setText(list.get(1).getKey());
                }
                if (!StringUtils.isEmpty(list.get(1).getVal())) {
                    guide_ac_tv_shiliangdanbaizhi.setText(list.get(1).getVal());
                }
            }
            if (list.size() == 1) {
                if (list.get(0).getId() != null) {
                    guide_ac_cb_sportlimit.setTag(list.get(0).getId() + ",");
                }
                if (!StringUtils.isEmpty(list.get(0).getKey())) {
                    guide_limit_tv.setText(list.get(0).getKey());
                }
                if (!StringUtils.isEmpty(list.get(0).getVal())) {
                    guide_ac_tv_sportall.setText(list.get(0).getVal());
                }
                guide_ac_li_limitdanbaizhi.setVisibility(View.GONE);
            }
            if (list.size() == 0) {
                shanshi_item.setVisibility(View.GONE);
            }


        }
        if (i == 3) {
            List<GuideItem> list = data.getData();
            if (list.size() == 0) {
                xuanjiao_item.setVisibility(View.GONE);
            } else if (list.size() == 1) {
                if (list.get(0).getId() != null) {
                    id = list.get(0).getId() + ",";
                }
                if (!StringUtils.isEmpty(list.get(0).getVal())) {
                    guide_ac_tv_jkxjiao.setText(list.get(0).getVal());
                }
            }
        }
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof PersonCreateGuide) {
            PersonCreateGuide personguide = (PersonCreateGuide) o;
            Log.d("personguide", personguide.toString());
            if (personguide != null) {
                pasteinfo(personguide);
            }
        } else {
            ToastUtils.showToast(this, "暂无数据");
        }
    }

    //赋值，个人资料，血压血糖数据
    private void pasteinfo(PersonCreateGuide personguide) {
        if (null!=personguide.getRecordBP() ) {
            if (null!=personguide.getRecordBP().getRecordId()){
                recordIds = personguide.getRecordBP().getRecordId() + ",0";
            }
        } else {
            recordIds = "0,0";
        }
        if (personguide.getReportId() != null) {
            reportId = personguide.getReportId();
        }
        if (!StringUtils.isEmpty(personguide.getHeadImg())) {
            Glide.with(this).load(Constant.TEST_URL + personguide.getHeadImg()).into(guide_ac_img_logo);
        }
        if (!StringUtils.isEmpty(personguide.getUsername())) {
            guide_ac_tv_name.setText("姓名 ：" + personguide.getUsername());
        }else if (!StringUtils.isEmpty(personguide.getPhoneNumber())){
            guide_ac_tv_name.setText(personguide.getPhoneNumber());
        }else {
            guide_ac_tv_name.setText("无名");
        }
        if (!StringUtils.isEmpty(personguide.getSex())) {
            guide_ac_tv_sex.setText(personguide.getSex());
        }
        if (personguide.getAge() != null) {
            guide_ac_tv_age.setText(personguide.getAge() + " 岁");
        }
        if (personguide.getRecordBP() != null) {
            if (personguide.getRecordBP().getSystolicPressure() != null) {
                guide_ac_tv_shuzhangya.setText(personguide.getRecordBP().getSystolicPressure());
                if (personguide.getRecordBP().getSystolicPressure() > 140) {
                    guide_ac_img_bp_up.setImageResource(R.mipmap.hong);
                } else if (personguide.getRecordBP().getSystolicPressure() < 120) {
                    guide_ac_img_bp_up.setImageResource(R.mipmap.lan);
                }
            }
            if (personguide.getRecordBP().getDiastolicPressure() != null) {
                guide_ac_tv_shousuoya.setText(personguide.getRecordBP().getDiastolicPressure());
                if (personguide.getRecordBP().getDiastolicPressure() > 90) {
                    guide_ac_img_bp_down.setImageResource(R.mipmap.hong);
                } else if (personguide.getRecordBP().getDiastolicPressure() < 60) {
                    guide_ac_img_bp_down.setImageResource(R.mipmap.lan);
                }
            }
        }
        List<PersonCreateGuide.RecordBSesEntity> personguidelist = personguide.getRecordBSes();
        if (personguidelist.size() > 0) {
            for (int i = 0; i < personguidelist.size(); i++) {
                if (personguidelist.get(i).getBloodSugarType() == 1) {
                    if (!StringUtils.isEmpty(personguidelist.get(i).getBloodSugar())) {
                        guide_ac_tv_kongfu.setText(personguidelist.get(i).getBloodSugar());
                        if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) > 7.0) {
                            guide_ac_img_kongfu.setImageResource(R.mipmap.hong);
                        } else if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_kongfu.setImageResource(R.mipmap.lan);
                        }
                    }
                }
                if (personguidelist.get(i).getBloodSugarType() == 9) {
                    if (!StringUtils.isEmpty(personguidelist.get(i).getBloodSugar())) {
                        guide_ac_tv_wancanhou.setText(personguidelist.get(i).getBloodSugar());
                        if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) > 10.0) {
                            guide_ac_img_wancanhou.setImageResource(R.mipmap.hong);
                        } else if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_wancanhou.setImageResource(R.mipmap.lan);
                        }
                    }
                }
                if (personguidelist.get(i).getBloodSugarType() == 9) {
                    if (!StringUtils.isEmpty(personguidelist.get(i).getBloodSugar())) {
                        guide_ac_tv_shuiqian.setText(personguidelist.get(i).getBloodSugar());
                        if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) > 10.0) {
                            guide_ac_img_shuiqian.setImageResource(R.mipmap.hong);
                        } else if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_shuiqian.setImageResource(R.mipmap.lan);
                        }
                    }
                }
                if (personguidelist.get(i).getBloodSugarType() == 8) {
                    if (!StringUtils.isEmpty(personguidelist.get(i).getBloodSugar())) {
                        guide_ac_tv_wancanqian.setText(personguidelist.get(i).getBloodSugar());
                        if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) > 10.0) {
                            guide_ac_img_wancanqian.setImageResource(R.mipmap.hong);
                        } else if (Float.parseFloat(personguidelist.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_wancanqian.setImageResource(R.mipmap.lan);
                        }
                    }
                }
            }
        }

    }

    private void initClick() {
        re_back.setOnClickListener(this);
        editguid_ac_btn_send.setOnClickListener(this);
    }

    private void initview() {
        navigation_name.setText("创建指导");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.editguid_ac_btn_send:
                sendRecord();
                break;
        }
    }


    //医生创建报告
    private void sendRecord() {
        stringbuder = new StringBuilder();
        if (!StringUtils.isEmpty(id)) {
            stringbuder.append(id);
        }
        if (guide_ac_cb_limit.isChecked()) {
            String id = (String) guide_ac_cb_limit.getTag();
            if (!StringUtils.isEmpty(id)) {
                stringbuder.append(id);
            }
        }
        if (guide_ac_cb_danbaizhi.isChecked()) {
            String id1 = (String) guide_ac_cb_danbaizhi.getTag();
            if (!StringUtils.isEmpty(id1)) {
                stringbuder.append(id1);
            }
        }
        if (guide_ac_cb_sportlimit.isChecked()) {
            String id2 = (String) guide_ac_cb_sportlimit.getTag();
            if (!StringUtils.isEmpty(id2)) {
                stringbuder.append(id2);
            }
        }
        if (guide_ac_cb_sport_danbaizhi.isChecked()) {
            String id3 = (String) guide_ac_cb_sport_danbaizhi.getTag();
            if (StringUtils.isEmpty(id3)) {
                stringbuder.append(id3);
            }
        }
//        Log.d("stringbuder", stringbuder.toString().trim());
        if (stringbuder.toString().trim().length() > 0) {
            String items = stringbuder.toString().replaceAll("null", "").trim();
            if (items.length() > 0) {
                String item = items.substring(0, items.length() - 1);
                sendGuide(item);
            } else {
                ToastUtils.showToast(EditGuideActivity.this, "请选择指导项目");
            }
        }
    }


    private void sendGuide(String items) {
        if (StringUtils.isEmpty(guide_ac_tv_beizhu.getText().toString().trim())) {
            remark = "暂无备注信息";
        } else {
            if (StringUtils.checkNameChese(guide_ac_tv_beizhu.getText().toString().trim())){
                remark = guide_ac_tv_beizhu.getText().toString().trim();
            }else {
//                ToastUtils.showToast(EditGuideActivity.this,"备注只能为中文！");
            }
        }
//        Log.d("recordIds",recordIds);
        if (!StringUtils.isEmpty(recordIds)){
            Subscription s;
            s = Server.HomeProtocolBuild(this).saveGuide(EnjoyorApplication.getInstance().getDBHelper().getUser().getId(), accountId, "hypertension", items, remark, recordIds, "saveGuide")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            data -> {
                                saveWithResponse(data);
                            },
                            e -> {
                                onError(e);
                            }
                    );
            addSubscription(s);
        }else {
//            ToastUtils.showToast(EditGuideActivity.this, "发送报告失败");
        }
    }

    private void saveWithResponse(BaseResponse data) {
//        Log.d("stri", data.getData().toString());
        if (data.getResult() == 1) {
            ToastUtils.showToast(EditGuideActivity.this, "发送报告成功");
            finish();
        } else {
            ToastUtils.showToast(EditGuideActivity.this, "发送报告失败");
        }
    }
}
