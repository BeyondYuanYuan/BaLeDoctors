package com.jkb.enjoyor.baledoctor.protocol;

import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;

import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by YuanYuan on 2016/9/13.
 */
public interface OtherProtocol {
    //    用户注册接口
    @POST("/doctor/info/register.action")
    Observable<BaseResponse<UserInfo>> getRegist(@Query("loginName") String loginName,
                                                 @Query("loginPwd") String loginPwd,
                                                 @Query("mcode") String mcode,
                                                 @Body String string);

    //    获取验证码接口
    @POST("/doctor/msg/send.do")
    Observable<BaseResponse> getMessage(@Query("service") String mob,
                                        @Query("phone") String phone,
                                        @Body String string);

    //用户登录接口
    @POST("/doctor/info/login.action")
    Observable<BaseResponse<UserInfo>> login(@Query("loginName") String loginName,
                                             @Query("loginPwd") String loginPwd,
                                             @Body String login);

    //    用户忘记密码接口
    @POST("/doctor/info//pwd/unknown.action")
    Observable<BaseResponse> forgetPwd(@Query("loginName") String loginName,
                                       @Query("pwd") String pwd,
                                       @Query("mcode") String mcode,
                                       @Body String forget);

    //    验证验证码接口
    @POST("/doctor/msg/verify.action")
    Observable<BaseResponse> yanzhengma(@Query("service") String service,
                                        @Query("phone") String phone,
                                        @Query("code") String code,
                                        @Body String forget);
}
