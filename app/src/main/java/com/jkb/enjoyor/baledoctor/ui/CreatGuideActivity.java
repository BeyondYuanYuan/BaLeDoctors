package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.MyPatient;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.CharacterParser;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.PinyinComparator;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.SideBar;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.SortModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/9.
 * 创建指导页面
 */
public class CreatGuideActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.tv_right)
    TextView tv_right;

    @Bind(R.id.createguide_filter_edit)
    EditText createguide_filter_edit;
    @Bind(R.id.createguide_ll_search)
    LinearLayout createguide_ll_search;
    @Bind(R.id.createguide_lv_patient)
    ListView createguide_lv_patient;
    @Bind(R.id.createguide_sidrbar)
    SideBar createguide_sidrbar;
    @Bind(R.id.createguide_dialog)
    TextView createguide_dialog;
    @Bind(R.id.createguide_delete)
    ImageView createguide_delete;
    private List<SortModel> sortModel_list = new ArrayList<>();
    private List<MyPatient> patient_list = new ArrayList<>();

    private CharacterParser characterParser;
    private PinyinComparator pinyinComparator;

    private PatientAdapter adapter;
    private int currentID = -1;
    private int accountId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creatguide_ac_layout);
        ButterKnife.bind(this);
        initEvent();
        initData(null);
//        initListView();
        initEditText();
    }

    private void initEditText() {
        createguide_filter_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s.toString())) {
                    createguide_ll_search.setVisibility(View.GONE);
                } else {
                    createguide_ll_search.setVisibility(View.VISIBLE);
                }
//                filterData(s.toString());
//                Log.d("ssssssssssss",s.toString());
                if (StringUtils.isEmpty(s.toString())) {
                    createguide_delete.setVisibility(View.GONE);
                    currentID = -1;
                    initData(null);
                } else {
                    patient_list.clear();
                    createguide_delete.setVisibility(View.VISIBLE);
                    initData(s.toString());
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                Log.i("ED", "beforeTextChanged------" + s);
            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.i("ED", "afterTextChanged------" + s);
            }
        });
    }

    private void filterData(String filterStr) {
        List<SortModel> filterDateList = new ArrayList<SortModel>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = sortModel_list;
        } else {
            filterDateList.clear();
            for (SortModel sortModel : sortModel_list) {
                String name = sortModel.getName();
                if (name.indexOf(filterStr.toString()) != -1 || characterParser.getSelling(name).startsWith(filterStr.toString())) {
                    filterDateList.add(sortModel);
                }
            }
        }
        Collections.sort(filterDateList, pinyinComparator);
        adapter.updateListView(filterDateList);
    }

    private void initData(String name) {
        Subscription s;
        s = Server.HomeProtocolBuild(this).searchmypatient(EnjoyorApplication.getInstance().getDBHelper().getUser().getId(), name, 100)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            if(data.getResult() == 0){
                                createguide_lv_patient.setVisibility(View.GONE);
                            }
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        if (o instanceof ArrayList) {
            createguide_lv_patient.setVisibility(View.VISIBLE);
            List<MyPatient> _list = (List<MyPatient>) o;
            patient_list.clear();
            patient_list.addAll(_list);
        }
        initListView();
    }

    private void initListView() {
        if (patient_list.size() > 0) {
            initSideBar();
            filledData(patient_list);
            adapter = new PatientAdapter(sortModel_list);
            Collections.sort(sortModel_list, pinyinComparator);
            createguide_lv_patient.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            createguide_lv_patient.setAdapter(adapter);
            createguide_lv_patient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position != currentID) {
                        adapter.setCurrentID(position);
                        adapter.notifyDataSetChanged();
                    }
                    currentID = position;
                    accountId = sortModel_list.get(position).getMyPatient().getAccountId();
                }
            });
        }
    }


    private void filledData(List<MyPatient> date) {
        sortModel_list.clear();
        for (int i = 0; i < date.size(); i++) {
            SortModel sortModel = new SortModel();
            sortModel.setName(date.get(i).getUserName());
            sortModel.setMyPatient(date.get(i));
            String pinyin = characterParser.getSelling(date.get(i).getUserName());
            if (TextUtils.isEmpty(pinyin)) {
                pinyin = "无名";
            }
            String sortString = pinyin.substring(0, 1).toUpperCase();

            if (sortString.matches("[A-Z]")) {
                sortModel.setSortLetters(sortString.toUpperCase());
            } else {
                sortModel.setSortLetters("#");
            }
            sortModel_list.add(sortModel);

        }

    }

    private void initSideBar() {
        createguide_sidrbar.setTextView(createguide_dialog);
        createguide_sidrbar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    createguide_lv_patient.setSelection(position);
                }
            }
        });
    }

    private void initEvent() {
        tv_right.setOnClickListener(this);
        re_back.setOnClickListener(this);
        createguide_delete.setOnClickListener(this);
        navigation_name.setText("创建指导");
        tv_right.setText("确定");
        tv_right.setTextColor(getResources().getColor(R.color.theme_color));
        tv_right.setVisibility(View.VISIBLE);
        characterParser = CharacterParser.getInstance();
        pinyinComparator = new PinyinComparator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        currentID = -1;
        if (!StringUtils.isEmpty(createguide_filter_edit.getText().toString().trim())) {
            initData(createguide_filter_edit.getText().toString().trim());
        } else {
            initData(null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.tv_right:
                creatGuide();
                break;
            case R.id.createguide_delete:
                createguide_filter_edit.setText("");
                break;
        }
    }

    private void creatGuide() {
        if (currentID != -1) {
            Intent intent = new Intent(CreatGuideActivity.this, EditGuideActivity.class);
            intent.putExtra("accountId", accountId);
            currentID = -1;
            startActivity(intent);
        } else {
            ToastUtils.showToast(this, "请选择要指导的患者！");
        }
    }

    public class PatientAdapter extends BaseAdapter implements SectionIndexer {
        private List<SortModel> list = null;
        int currentID = -1;

        public PatientAdapter(List<SortModel> list) {

            this.list = list;
        }

        public void updateListView(List<SortModel> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        public void setCurrentID(int currentID) {
            this.currentID = currentID;
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return list.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup arg2) {
//            ImageView createguide_picture = null;
            final ViewHolder viewHolder;
            final SortModel mContent = list.get(position);
            if (view == null) {
                view = LayoutInflater.from(CreatGuideActivity.this).inflate(R.layout.creatguide_eplv_item_layout, null);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            int section = getSectionForPosition(position);


            if (position == getPositionForSection(section)) {
                viewHolder.createguide_catalog.setVisibility(View.VISIBLE);
                viewHolder.createguide_catalog.setText(mContent.getSortLetters());
            } else {
                viewHolder.createguide_catalog.setVisibility(View.GONE);
            }
            if (!StringUtils.isEmpty(list.get(position).getName())) {
                viewHolder.createguide_name.setText(list.get(position).getName());
            } else {
                viewHolder.createguide_name.setText(list.get(position).getMyPatient().getPhoneNum());
            }
            if (StringUtils.isEmpty(list.get(position).getMyPatient().getSex())) {
                viewHolder.createguide_sex.setText("男");
            } else {
                viewHolder.createguide_sex.setText(list.get(position).getMyPatient().getSex());
            }
            if ((list.get(position).getMyPatient().getAge()) == null) {
                viewHolder.createguide_age.setText("18岁");
            } else {
                viewHolder.createguide_age.setText(list.get(position).getMyPatient().getAge() + "岁");
            }
            if (position == this.currentID) {
                viewHolder.createguide_checkbox.setBackgroundDrawable(getResources().getDrawable(R.mipmap.xuanzhong));
            } else {
                viewHolder.createguide_checkbox.setBackgroundDrawable(getResources().getDrawable(R.mipmap.daixuanze));
            }
            if (!StringUtils.isEmpty(list.get(position).getMyPatient().getHeadImg())) {
                ImageLoader.getInstance().displayImage(Constant.TEST_URL + list.get(position).getMyPatient().getHeadImg(), viewHolder.createguide_picture, EnjoyorApplication.option);
            }
            return view;

        }

        public class ViewHolder {
            @Bind(R.id.createguide_catalog)
            TextView createguide_catalog;
            @Bind(R.id.createguide_checkbox)
            ImageView createguide_checkbox;
            @Bind(R.id.createguide_picture)
            ImageView createguide_picture;
            @Bind(R.id.createguide_name)
            TextView createguide_name;
            @Bind(R.id.createguide_sex)
            TextView createguide_sex;
            @Bind(R.id.createguide_age)
            TextView createguide_age;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        public int getSectionForPosition(int position) {
            return list.get(position).getSortLetters().charAt(0);
        }

        public int getPositionForSection(int section) {
            for (int i = 0; i < getCount(); i++) {
                String sortStr = list.get(i).getSortLetters();
                char firstChar = sortStr.toUpperCase().charAt(0);
                if (firstChar == section) {
                    return i;
                }
            }

            return -1;
        }

        private String getAlpha(String str) {
            String sortStr = str.trim().substring(0, 1).toUpperCase();

            if (sortStr.matches("[A-Z]")) {
                return sortStr;
            } else {
                return "#";
            }
        }

        @Override
        public Object[] getSections() {
            return null;
        }
    }
}
