package com.jkb.enjoyor.baledoctor.utils.phoneticsearch;

import com.jkb.enjoyor.baledoctor.entitry.MyPatient;

public class SortModel {

	private String name;
	private String sortLetters;
	private MyPatient myPatient;

	public MyPatient getMyPatient() {
		return myPatient;
	}

	public void setMyPatient(MyPatient myPatient) {
		this.myPatient = myPatient;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSortLetters() {
		return sortLetters;
	}
	public void setSortLetters(String sortLetters) {
		this.sortLetters = sortLetters;
	}
}
