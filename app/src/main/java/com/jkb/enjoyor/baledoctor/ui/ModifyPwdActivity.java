package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.BaseDate;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.AppManagerUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/10.
 * 修改密码页面
 */
public class ModifyPwdActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.modify_save)
    Button modify_save;
    @Bind(R.id.modifypwd_et_access)
    EditText modifypwd_et_access;
    @Bind(R.id.modify_pwd)
    EditText modify_pwd;
    @Bind(R.id.modify_again_pwd)
    EditText modify_again_pwd;

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initEvent() {
        re_back.setOnClickListener(this);
        navigation_name.setText("修改密码");
        modify_save.setOnClickListener(this);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modifypwd_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.modify_save:
//                ToastUtils.showToast(ModifyPwdActivity.this, "您点击了修改密码保存按钮");
                modify();
                break;
        }
    }

    private void modify() {

        if (StringUtils.isBlank(modifypwd_et_access.getText().toString().trim())) {
            showSnackBar("请输入原密码");
        } else if (StringUtils.isBlank(modify_pwd.getText().toString().trim())||modify_again_pwd.getText().toString().trim().length()<6||modify_again_pwd.getText().toString().trim().length()>12) {
            showSnackBar("请输入新密码,新密码必须是6--12位有效字符");
        } else if (StringUtils.isBlank(modify_again_pwd.getText().toString().trim())||modify_again_pwd.getText().toString().trim().length()<6||modify_again_pwd.getText().toString().trim().length()>12) {
            showSnackBar("请再次输入新密码，新密码必须是6--12位有效字符");
        } else if (!modify_pwd.getText().toString().trim().equals(modify_again_pwd.getText().toString().trim())) {
            showSnackBar("两次密码不一致，请重新输入");
        } else if (modifypwd_et_access.getText().toString().trim().equals(modify_again_pwd.getText().toString().trim())){
           showSnackBar("原密码与新密码一致，请重新输入");
        }else {

            modify_save.setBackgroundResource(R.drawable.btn_solid_greedyellow_radius);
            modify_save.setClickable(true);
            Log.d("id",EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo().getId()+"");
            Subscription s;
            s = Server.HomeProtocolBuild(this).modify(EnjoyorApplication.getInstance().getDBHelper().getUser().getId(),
                    modifypwd_et_access.getText().toString(),
                    modify_pwd.getText().toString(), "str")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            data -> {
                                dealWithResponse(data);
                            },
                            e -> {
                                onError(e);
                            }
                    );
            addSubscription(s);
        }
        hideKeyBoard();
    }


    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if ((boolean) o) {
            showSnackBar("密码修改成功");
            if (BaseDate.getSessionId(ModifyPwdActivity.this) != null) {
                if (EnjoyorApplication.getInstance().getDBHelper().clearUser()) {
                    Intent intent = new Intent(ModifyPwdActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                AppManagerUtil.getAppManager().finishAllActivity();
            }
            finish();
        } else {
            showSnackBar("密码修改失败");
        }
    }

    @Override
    public void onError(int errorCode) {
        super.onError(errorCode);
        ToastUtils.showToast(ModifyPwdActivity.this,"原密码错误，请输入正确的原密码！");
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        ToastUtils.showToast(ModifyPwdActivity.this,"原密码错误，请输入正确的原密码！");
    }
}
