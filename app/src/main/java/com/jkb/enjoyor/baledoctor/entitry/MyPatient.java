package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by YuanYuan on 2016/9/19.
 */
public class MyPatient {

    /**
     * accountId : 11
     * age : null
     * headImg :
     * nickName :
     * sex : 男
     * userId : 195
     * userName : 高官厚禄
     */

    private Integer accountId;
    private Integer age;
    private String headImg;
    private String nickName;
    private String sex;
    private Integer userId;
    private String userName;
    private String phoneNum;

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
