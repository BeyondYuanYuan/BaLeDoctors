package com.jkb.enjoyor.baledoctor.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/10.
 * 意见反馈页面
 */
public class SuggestActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.tv_right)
    TextView tv_right;
    @Bind(R.id.suggest_et)
    EditText suggest_et;

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initEvent() {
        navigation_name.setText("意见与反馈");
        tv_right.setOnClickListener(this);
        re_back.setOnClickListener(this);
        tv_right.setText("发送");
        tv_right.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.suggest_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.tv_right:
                suggest();
                break;
        }
    }

    private void suggest() {
        if (StringUtils.isEmpty(suggest_et.getText().toString().trim())) {
            ToastUtils.showToast(this, "意见不能为空，请输入您的宝贵意见");
        } else {
            ToastUtils.showToast(SuggestActivity.this, "您的宝贵意见是我们前进的动力！");
            finish();
        }
    }
}
