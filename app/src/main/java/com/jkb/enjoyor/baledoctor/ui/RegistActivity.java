package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.MatcherUtil;
import com.jkb.enjoyor.baledoctor.utils.SharePreferenceUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/10.
 * 注册页面
 */
public class RegistActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.regist)
    Button regist;
    @Bind(R.id.regist_choice)
    CheckBox regist_choice;
    @Bind(R.id.regist_agreement)
    TextView regist_agreement;
    @Bind(R.id.registphonenumber)
    EditText registphonenumber;
    @Bind(R.id.regist_password)
    EditText regist_password;
    @Bind(R.id.regist_tv_yanzheng)
    EditText regist_tv_yanzheng;
    @Bind(R.id.regist_yanzheng)
    TextView regist_yanzheng;
    private int count = 60;//30秒倒计时
    private Handler handler;
    private String tellphone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regist_ac_layout);
        ButterKnife.bind(this);
        initCode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initCode() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);
                if (msg.what > 0) {
                    regist_yanzheng.setText("剩余" + msg.what + "秒");
                }
            }
        };
    }

    private void initEvent() {
        re_back.setOnClickListener(this);
        navigation_name.setText("注册");
        regist_yanzheng.setOnClickListener(this);
        regist.setOnClickListener(this);
        regist_choice.setOnClickListener(this);
        regist_agreement.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.regist_yanzheng:
//                ToastUtils.showToast(RegistActivity.this, "您点击了获取验证码按钮");
                achieveRegist();
                break;
            case R.id.regist:
                regist();
//                ToastUtils.showToast(RegistActivity.this, "您点击了注册按钮");
                break;
            case R.id.regist_choice:
//                ToastUtils.showToast(RegistActivity.this, "您已经同意芭乐健康服务协议");
                break;
            case R.id.regist_agreement:
//                ToastUtils.showToast(RegistActivity.this, "您点击了芭乐服务协议按钮");
                Intent intent_agreement = new Intent(RegistActivity.this, AgreementActivity.class);
                startActivity(intent_agreement);
                break;
        }
    }

    //    获取验证码方法
    private void achieveRegist() {
        if (StringUtils.isBlank(registphonenumber.getText().toString())) {
            showSnackBar("手机号码不能为空");
            registphonenumber.requestFocus();
        } else if (!MatcherUtil.isMobileNumber(registphonenumber.getText().toString())) {
            showSnackBar("请输入正确的手机号");
            registphonenumber.requestFocus();
        } else {
            sendMsg();
            sendMsgtoPhone();
        }
    }

    private void sendMsgtoPhone() {
        Subscription s;
        s = Server.otherProtocolBuild(this).getMessage("mob", registphonenumber.getText().toString(), "str")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if ((boolean) o) {
            ToastUtils.showToast(this, "发送成功");
        } else {
            ToastUtils.showToast(this, "信息发送失败");
        }
    }

    private void sendMsg() {
        regist_yanzheng.setEnabled(false);
        regist_yanzheng.setText("获取验证码");
        //倒记时
        final Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() { // UI thread
                    @Override
                    public void run() {
                        count--;
                        handler.sendEmptyMessage(count);
                        if (count == 0) {
                            timer.cancel();
                            reSet();
                        }
                    }
                });
            }
        };
        timer.schedule(task, 1000, 1000);
    }

    private void reSet() {
        if (tellphone != null && tellphone.equals(registphonenumber.getText().toString().trim())) {
            regist_yanzheng.setText("重新获取");
        } else {
            regist_yanzheng.setText("获取验证码");
        }
        regist_yanzheng.setEnabled(true);
        count = 60;
    }

    //用户注册方法
    private void regist() {
        if (isRight()) {
            Subscription s;
            s = Server.otherProtocolBuild(this).getRegist(phonenumber, password, yanzheng, "str")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            data -> {
                                doWithResponse(data);
                            },
                            e -> {
                                onError(e);
                            }
                    );
            addSubscription(s);
        }
    }

    private void doWithResponse(BaseResponse data) {
        if (data.getResult() == 1) {
//            ToastUtils.showToast(this, "用户注册成功，请返回登录");
            UserInfo userInfo = (UserInfo) data.getData();
            new SharePreferenceUtil(this).setUserId(userInfo.getId());
            EnjoyorApplication.getInstance().getDBHelper().saveUser(userInfo);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            registerEM(phonenumber);
            finish();
        } else if (data.getResult()==0){
            ToastUtils.showToast(this, "该用户已被注册");
        }
    }

    String phonenumber;
    String password;
    String yanzheng;

    private boolean isRight() {
        phonenumber = registphonenumber.getText().toString().trim();
        password = regist_password.getText().toString().trim();
        yanzheng = regist_tv_yanzheng.getText().toString().trim();

        if (StringUtils.isBlank(phonenumber)) {
            showSnackBar("手机号不能为空");
            registphonenumber.requestFocus();
            return false;
        } else if (!MatcherUtil.isMobileNumber(phonenumber)) {
            showSnackBar("手机号不正确");
            registphonenumber.requestFocus();
            return false;
        } else if (StringUtils.isBlank(password)) {
            showSnackBar("密码不能为空");
            regist_password.requestFocus();
            return false;
        } else if (!MatcherUtil.isPWD(password)) {
            showSnackBar("请输入6-12位密码");
            regist_password.requestFocus();
            return false;
        } else if (StringUtils.isBlank(yanzheng)) {
            showSnackBar("请输入验证码");
            regist_tv_yanzheng.requestFocus();
            return false;
        }
        return true;
    }

    private void registerEM(final String s) {

        //注册失败会抛出HyphenateException
//        EMClient.getInstance().createAccount(username, pwd);//同步方法 服务器返回
        new Thread(new Runnable() {
            public void run() {
                try {
                    EMClient.getInstance().createAccount(s, Constant.EMPWD);//同步方法 测试

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(RegistActivity.this, "注册成功", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (final HyphenateException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            int errorCode = e.getErrorCode();
                            Log.i("EMErrorCode", errorCode + "");
                        }
                    });
                }
            }
        }).start();

    }
}
