package com.jkb.enjoyor.baledoctor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.pinyinsort.AssortPinyinList;
import com.jkb.enjoyor.baledoctor.pinyinsort.LanguageComparator_CN;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by YuanYuan on 2016/9/9.
 */
public class PinYinAdapter extends BaseExpandableListAdapter {

    // 字符串
    private AssortPinyinList assort = new AssortPinyinList();
    private Context context;
    private LayoutInflater inflater;
    // 中文排序
    private LanguageComparator_CN cnSort = new LanguageComparator_CN();
    //    private ArrayList<YiShengHZLB> shengHZLBs = new ArrayList<YiShengHZLB>();
//    private HashMap<String, YiShengHZLB> maps;
    private ArrayList<String> strList;

//    public PinYinAdapter(Context context, HashMap<String, YiShengHZLB> maps,
//                         ArrayList<String> strList) {
//        super();
//        this.context = context;
//        this.inflater = LayoutInflater.from(context);
//        // if (this.maps == null) {
//        this.maps = maps;
//        this.strList = strList;
//        // }
//        sort();
//    }

    public PinYinAdapter() {
    }

    private void sort() {
        // TODO Auto-generated method stub

        // 分类
        for (String str : strList) {
            assort.getHashList().add(str);
        }
        assort.getHashList().sortKeyComparator(cnSort);
        for (int i = 0, length = assort.getHashList().size(); i < length; i++) {
            Collections.sort((assort.getHashList().getValueListIndex(i)),
                    cnSort);
        }
    }

    @Override
    public Object getChild(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return assort.getHashList().getValueIndex(arg0, arg1);
    }

    @Override
    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return arg1;
    }

    @Override
    public View getChildView(int arg0, int arg1, boolean arg2, View arg3,
                             ViewGroup arg4) {
        // TODO Auto-generated method stub
        ViewHolder holder = null;
        if (arg3 == null) {
            holder = new ViewHolder();
            arg3 = inflater.inflate(R.layout.creatguide_eplv_item_layout, null);
//            holder.yishenghzlb_item_photo = (ImageView) arg3
//                    .findViewById(R.id.yishenghzlb_item_photo);
//            holder.yishenghzlb_item_name = (TextView) arg3
//                    .findViewById(R.id.yishenghzlb_item_name);
//            holder.yishenghzlb_item_type = (TextView) arg3
//                    .findViewById(R.id.yishenghzlb_item_type);
//            holder.yishenghzlb_item_beforetime = (TextView) arg3
//                    .findViewById(R.id.yishenghzlb_item_beforetime);
//            arg3.setTag(holder);
        } else {
            holder = (ViewHolder) arg3.getTag();
        }
//        // YiShengHZLB yiShengHZLB = shengHZLBs.get(arg0);
//        String name = assort.getHashList().getValueIndex(arg0, arg1);
//        YiShengHZLB yiShengHZLB = maps.get(name);
//        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        try {
//            Date di = sf.parse(yiShengHZLB.getShangCiZDSJ());
//            holder.yishenghzlb_item_beforetime.setText("上次指导："
//                    + formatBeforeTimeDate(di.getTime()));
//        } catch (ParseException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

//        holder.yishenghzlb_item_type.setText(yiShengHZLB.getYiShengFW());
//        holder.yishenghzlb_item_name.setText(name);
//        holder.yishenghzlb_item_name.setTag(yiShengHZLB.getYongHuID() + "");

        return arg3;
    }

    /**
     * 处理当前时间与最后指导时间之间关系，将制定时间与当前时间进行对比，如果小于一分钟则显示刚刚，
     * 小于一小时则显示多少分钟前，小于24小时则显示多少小时 ，大于24小时显示几天前，大于24小时小于一周显示几天前，大于一周显示年月日
     *
     * @author Administrator
     */
    public String formatBeforeTimeDate(long time) {
        String ret = "";
        Date date = new Date();
        long currentTime = date.getTime();
        long distance = (currentTime - time) / 1000;
        if (distance < 0) {
            distance = 0;
        }
        if (distance < 60) {
            ret = "刚刚";
        } else if (distance < 60 * 60) {
            distance = distance / 60;
            ret = distance + "分钟前";
        } else if (distance < 60 * 60 * 24) {
            distance = distance / (60 * 60);
            ret = distance + "小时前";
        } else {
            distance = distance / (60 * 60 * 24);
            if (distance >= 7) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time);
                Date calendarTime = calendar.getTime();
                SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
                ret = format.format(calendarTime);
            } else {
                ret = distance + "天前";
            }
        }
        return ret;
    }

    public class ViewHolder {
        private ImageView yishenghzlb_item_photo;
        private TextView yishenghzlb_item_name, yishenghzlb_item_type,
                yishenghzlb_item_beforetime;
    }

    @Override
    public int getChildrenCount(int arg0) {
        // TODO Auto-generated method stub
        return assort.getHashList().getValueListIndex(arg0).size();
    }

    @Override
    public Object getGroup(int arg0) {
        // TODO Auto-generated method stub
        return assort.getHashList().getValueListIndex(arg0);
    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return assort.getHashList().size();
    }

    @Override
    public long getGroupId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
        // TODO Auto-generated method stub
        if (arg2 == null) {
            arg2 = inflater.inflate(R.layout.creatguide_group_layout, null);
            arg2.setClickable(true);
        }
        TextView textView = (TextView) arg2.findViewById(R.id.name);
        textView.setText(assort.getFirstChar(assort.getHashList()
                .getValueIndex(arg0, 0)));
        // 禁止伸展

        return arg2;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }

    public AssortPinyinList getAssort() {
        return assort;
    }
}
