package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMError;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.BaseDate;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.MyPatient;
import com.jkb.enjoyor.baledoctor.entitry.PatientRecord;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.CharacterParser;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.PinyinComparator;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.SideBar;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.SortModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/11.
 * 我的患者页面
 */
public class MyPatientActivity extends ToolBarActivity {
    private String type = "";
    @Bind(R.id.container)
    CoordinatorLayout container;
    private StringBuilder stringBuilder = new StringBuilder();
    @Bind(R.id.filter_edit)
    EditText filter_edit;
    @Bind(R.id.ll_search)
    LinearLayout ll_search;
    @Bind(R.id.lv_patient)
    ListView lv_patient;
    @Bind(R.id.sidrbar)
    SideBar sidrbar;
    @Bind(R.id.dialog)
    TextView dialog;
    @Bind(R.id.mypatient_img_delete)ImageView mypatient_img_delete;

    private List<SortModel> sortModel_list = new ArrayList<>();
    private List<MyPatient> patient_list = new ArrayList<>();

    private CharacterParser characterParser;
    private PinyinComparator pinyinComparator;

    private PatientAdapter adapter;

    private boolean isExpend = false;

    private Long id;
    private String registName;
    private ArrayList<PatientRecord> patientrecord_list = new ArrayList();
    private String EMurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mypatient_ac_layout, false);
        ButterKnife.bind(this);
        if (EnjoyorApplication.getInstance().getDBHelper().getUser() != null) {

            registName = EnjoyorApplication.getInstance().getDBHelper().getUser().getUserPhoneNumber();
            EMurl = Constant.TEST_URL +EnjoyorApplication.getInstance().getDBHelper().getUser().getHeadImg();
        }
        if (StringUtils.isEmpty(EMurl)) {
            if (null != EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo()) {
                EMurl = Constant.TEST_URL +EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo().getHeadImg();

            }
        }
        if (StringUtils.isEmpty(registName)) {
            if (null != EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo()) {
                registName = EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo().getPhoneNumber();
            }
        }
        if (getIntent().hasExtra("list")) {
            patientrecord_list = getIntent().getParcelableArrayListExtra("list");
        }
        initHead();
        initData();
        initListView();
        initEditText();
    }

    private void initHead() {
        characterParser = CharacterParser.getInstance();
        pinyinComparator = new PinyinComparator();
        setTitle("我的患者");
        mypatient_img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter_edit.setText("");
            }
        });
    }

    private void initEditText() {
        filter_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s)) {
                    filter_edit.setCursorVisible(true);
                    ll_search.setVisibility(View.GONE);
                    mypatient_img_delete.setVisibility(View.VISIBLE);
                } else {
//                    filter_edit.setCursorVisible(false);
                    ll_search.setVisibility(View.VISIBLE);
                    mypatient_img_delete.setVisibility(View.GONE);
                }
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                Log.i("ED", "beforeTextChanged------" + s);
            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.i("ED", "afterTextChanged------" + s);
            }
        });
    }

    private void initData() {
        id = EnjoyorApplication.getInstance().getDBHelper().getUser().getId();
        Subscription s;
        s = Server.HomeProtocolBuild(this).mypatient(id,100)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        if (o instanceof ArrayList) {
            List<MyPatient> _list = (List<MyPatient>) o;
            patient_list.clear();
            patient_list.addAll(_list);

        }
        initListView();
    }

    private void initListView() {
        if (patient_list.size() > 0) {
            initSideBar();
            filledData(patient_list);
            adapter = new PatientAdapter(sortModel_list);
            Collections.sort(sortModel_list, pinyinComparator);
            lv_patient.setAdapter(adapter);
        }
    }

    private void initSideBar() {
        sidrbar.setTextView(dialog);
        sidrbar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
            @Override
            public void onTouchingLetterChanged(String s) {
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    lv_patient.setSelection(position);
                }
            }
        });
    }

    private void filterData(String filterStr) {
        List<SortModel> filterDateList = new ArrayList<SortModel>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = sortModel_list;
        } else {
//            filterDateList.clear();
            for (SortModel sortModel : sortModel_list) {
                String name = sortModel.getName();
                if (name.indexOf(filterStr.toString()) != -1 || characterParser.getSelling(name).startsWith(filterStr.toString())) {
                    filterDateList.add(sortModel);
                }
            }
        }
        Collections.sort(filterDateList, pinyinComparator);
        if (adapter != null) {
            adapter.updateListView(filterDateList);
        } else {
            adapter = new PatientAdapter(filterDateList);
        }
    }

    private void filledData(List<MyPatient> date) {

        for (int i = 0; i < date.size(); i++) {
            SortModel sortModel = new SortModel();
            sortModel.setName(date.get(i).getUserName());
            sortModel.setMyPatient(date.get(i));
            String pinyin = characterParser.getSelling(date.get(i).getUserName());
            if (TextUtils.isEmpty(pinyin)) {
                pinyin = "无名";
            }
            String sortString = pinyin.substring(0, 1).toUpperCase();

            if (sortString.matches("[A-Z]")) {
                sortModel.setSortLetters(sortString.toUpperCase());
            } else {
                sortModel.setSortLetters("#");
            }
//            sortModel_list.clear();
            sortModel_list.add(sortModel);
        }
    }

    public class PatientAdapter extends BaseAdapter implements SectionIndexer {
        private List<SortModel> list = null;

        public PatientAdapter(List<SortModel> list) {

            this.list = list;
        }

        public void updateListView(List<SortModel> list) {
            this.list = list;
            notifyDataSetChanged();
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return list.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup arg2) {
            final ViewHolder viewHolder;
            final SortModel mContent = list.get(position);
            if (view == null) {
                view = LayoutInflater.from(MyPatientActivity.this).inflate(R.layout.mypatient_lv_item, null);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            final String EMName = list.get(position).getMyPatient().getPhoneNum();
            int section = getSectionForPosition(position);

            if (position == getPositionForSection(section)) {
                viewHolder.catalog.setVisibility(View.VISIBLE);
                viewHolder.catalog.setText(mContent.getSortLetters());
            } else {
                viewHolder.catalog.setVisibility(View.GONE);
            }
            if (!StringUtils.isEmpty(list.get(position).getMyPatient().getUserName())) {
                viewHolder.tv_name.setText(list.get(position).getMyPatient().getUserName());
            } else if (!StringUtils.isEmpty(list.get(position).getMyPatient().getPhoneNum())) {
                viewHolder.tv_name.setText(list.get(position).getMyPatient().getPhoneNum());
            } else {
                viewHolder.tv_name.setText("无名");
            }
            if (!StringUtils.isEmpty(list.get(position).getMyPatient().getSex())) {

                viewHolder.tv_sex.setText(list.get(position).getMyPatient().getSex() + "");
            } else {
                viewHolder.tv_sex.setText("男");
            }

            if (list.get(position).getMyPatient().getAge() != null) {
                viewHolder.tv_age.setText(list.get(position).getMyPatient().getAge() + "");
            } else {
                viewHolder.tv_age.setText("18岁");
            }

            ImageLoader.getInstance().displayImage(Constant.TEST_URL + list.get(position).getMyPatient().getHeadImg(), viewHolder.iv_headimage, EnjoyorApplication.option);
            viewHolder.ll_expend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isExpend) {
                        viewHolder.ll_expendinfo.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.ll_expendinfo.setVisibility(View.GONE);
                    }
                    isExpend = !isExpend;
                }
            });

            int accountId = list.get(position).getMyPatient().getAccountId();
            if (patientrecord_list.size() > 0) {
                for (int i = 0; i < patientrecord_list.size(); i++) {
                    if (patientrecord_list.get(i).getAccountId() == accountId) {
                        stringBuilder.append(patientrecord_list.get(i).getType() + ",");
                    }
                }
                if (stringBuilder.length() > 0) {

                    type = stringBuilder.substring(0, stringBuilder.length() - 1);
                }
                if (!TextUtils.isEmpty(type)) {
                    viewHolder.ll_record.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(MyPatientActivity.this, RecordActivity.class);
                            intent.putExtra("accountId", accountId);
                            intent.putExtra("type", type);
//                            Log.i("ID", "accountId------------" + accountId);
                            startActivity(intent);
                        }
                    });
                } else {
                    viewHolder.ll_record.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ToastUtils.showToast(MyPatientActivity.this, "用户未提交报告");
                        }
                    });
                }
            } else {
                ToastUtils.showToast(MyPatientActivity.this, "用户未提交报告");
            }
            viewHolder.ll_createrecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ToastUtils.showToast(MyPatientActivity.this, "创建指导给" + list.get(position).getName());
                    if (list.get(position).getMyPatient().getAccountId() != null) {
                        Intent intent = new Intent(MyPatientActivity.this, EditGuideActivity.class);
                        intent.putExtra("accountId", list.get(position).getMyPatient().getAccountId());
                        startActivity(intent);
                    } else {
                        ToastUtils.showToast(MyPatientActivity.this, "暂时无法为" + list.get(position).getName() + "创建指导");
                    }

                }
            });

            final String EMNickName = nonEmptyStr(list.get(position).getMyPatient().getUserName(),EMName);
            BaseDate.setEMList(list);
            viewHolder.ll_minemessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean b = EMClient.getInstance().isLoggedInBefore();
                    Log.i("boolean", b + "");
                    if (EMClient.getInstance().isLoggedInBefore()) {
                        Intent intent = new Intent(MyPatientActivity.this, EMmessageActivity.class);
                        intent.putExtra("EMName", EMName);
                        intent.putExtra("EMurl", EMurl);
                        intent.putExtra("EMNickName", EMNickName);
                        startActivity(intent);
                    } else {
                        emLogin();
                    }
                }
            });

            return view;

        }

        public class ViewHolder {
            @Bind(R.id.tv_sex)
            TextView tv_sex;
            @Bind(R.id.tv_age)
            TextView tv_age;
            @Bind(R.id.iv_headimage)
            ImageView iv_headimage;
            @Bind(R.id.tv_name)
            TextView tv_name;
            @Bind(R.id.catalog)
            TextView catalog;
            @Bind(R.id.ll_expend)
            RelativeLayout ll_expend;
            @Bind(R.id.ll_expendinfo)
            LinearLayout ll_expendinfo;
            @Bind(R.id.ll_record)
            LinearLayout ll_record;
            @Bind(R.id.ll_createrecord)
            LinearLayout ll_createrecord;
            @Bind(R.id.ll_minemessage)
            LinearLayout ll_minemessage;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        public int getSectionForPosition(int position) {
            return list.get(position).getSortLetters().charAt(0);
        }

        public int getPositionForSection(int section) {
            for (int i = 0; i < getCount(); i++) {
                String sortStr = list.get(i).getSortLetters();
                char firstChar = sortStr.toUpperCase().charAt(0);
                if (firstChar == section) {
                    return i;
                }
            }

            return -1;
        }

        private String getAlpha(String str) {
            String sortStr = str.trim().substring(0, 1).toUpperCase();

            if (sortStr.matches("[A-Z]")) {
                return sortStr;
            } else {
                return "#";
            }
        }

        @Override
        public Object[] getSections() {
            return null;
        }
    }

    public void emLogin() {
        EMClient.getInstance().login(registName, Constant.EMPWD, new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
                Snackbar.make(container, "成功登录聊天", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onProgress(int progress, String status) {
            }

            @Override
            public void onError(int code, String message) {
                Log.i("code", code + "     " + message);
                if (204 == code) {
                    registerEM();
                    Snackbar.make(container, "正在注册聊天信息", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void registerEM() {

        new Thread(new Runnable() {
            public void run() {
                try {
                    EMClient.getInstance().createAccount(registName, Constant.EMPWD);//同步方法 测试
                    runOnUiThread(new Runnable() {
                        public void run() {
                            emLogin();
                            Snackbar.make(container, "注册成功", Snackbar.LENGTH_SHORT).show();
                        }
                    });
                } catch (final HyphenateException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            int errorCode = e.getErrorCode();
                            if (errorCode == EMError.NETWORK_ERROR) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_anomalies), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_ALREADY_EXIST) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.User_already_exists), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_AUTHENTICATION_FAILED) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.registration_failed_without_permission), Toast.LENGTH_SHORT).show();
                            } else if (errorCode == EMError.USER_ILLEGAL_ARGUMENT) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.illegal_user_name), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.Registration_failed), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        }).start();

    }

    private String nonEmptyStr(String s1, String s2) {
        return !StringUtils.isEmpty(s1) ? s1 : s2;
    }
}
