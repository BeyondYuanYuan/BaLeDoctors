package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by YuanYuan on 2016/10/17.
 */
public class GuideListInfo {


    /**
     * age : null
     * createTime : 2016-10-13 14:19:52
     * guideId : 1
     * guideItems : null
     * recordBP : null
     * recordBSes : null
     * remark : test
     * reportId : 28
     * sex : 女
     * type : hypertension
     * username : g     g
     * headImg : 
     */

    private Integer age;
    private String createTime;
    private Integer guideId;
    private Integer guideItems;
    private Integer recordBP;
    private Integer recordBSes;
    private String remark;
    private Integer reportId;
    private String sex;
    private String type;
    private String username;
    private String headImg;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getGuideId() {
        return guideId;
    }

    public void setGuideId(Integer guideId) {
        this.guideId = guideId;
    }

    public Integer getGuideItems() {
        return guideItems;
    }

    public void setGuideItems(Integer guideItems) {
        this.guideItems = guideItems;
    }

    public Integer getRecordBP() {
        return recordBP;
    }

    public void setRecordBP(Integer recordBP) {
        this.recordBP = recordBP;
    }

    public Integer getRecordBSes() {
        return recordBSes;
    }

    public void setRecordBSes(Integer recordBSes) {
        this.recordBSes = recordBSes;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }
}
