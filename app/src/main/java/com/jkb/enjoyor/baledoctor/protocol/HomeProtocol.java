package com.jkb.enjoyor.baledoctor.protocol;

import com.jkb.enjoyor.baledoctor.entitry.DoctorInfo;
import com.jkb.enjoyor.baledoctor.entitry.GuideInfo;
import com.jkb.enjoyor.baledoctor.entitry.GuideItem;
import com.jkb.enjoyor.baledoctor.entitry.MyPatient;
import com.jkb.enjoyor.baledoctor.entitry.MyRecordInfo;
import com.jkb.enjoyor.baledoctor.entitry.MyZijianInfo;
import com.jkb.enjoyor.baledoctor.entitry.NoteInfo;
import com.jkb.enjoyor.baledoctor.entitry.PatientRecord;
import com.jkb.enjoyor.baledoctor.entitry.PersonCreateGuide;
import com.jkb.enjoyor.baledoctor.entitry.RecordInfo;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;

import java.util.List;
import java.util.Map;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import rx.Observable;

/**
 * Created by YuanYuan on 2016/9/14.
 */
public interface HomeProtocol {
    //    医生资料接口  /doctor/info/{id}.do
    @GET("/doctor/info/{id}.do")
//    void getUserPhoto(@Path("id") int id, Callback<Photo> cb);
    Observable<BaseResponse<DoctorInfo>> getDoctorInfo(@Path("id") Long id);

    //    用户修改密码接口
    @POST("/doctor/info//pwd/reset.action")
    Observable<BaseResponse> modify(@Query("id") long id,
                                    @Query("oldPwd") String oldPwd,
                                    @Query("newPwd") String newPwd,
                                    @Body String modify);

    //    我的患者接口
    @GET("/doctor/info/accounts.do")
    Observable<BaseResponse<List<MyPatient>>> mypatient(@Query("id") long id
            , @Query("pageSize") int pageSize);

    //    搜索我的患者接口
    @GET("/doctor/info/accounts.do")
    Observable<BaseResponse<List<MyPatient>>> searchmypatient(@Query("id") long id,
                                                              @Query("name") String name
            , @Query("pageSize") int pageSize);

    //    健康报告
    @GET("/doctor/report/records.do")
    Observable<BaseResponse<List<RecordInfo>>> getReord(@Query("accountId") int accountId
            , @Query("pageSize") int pageSize
            , @Query("doctorId") long doctorId);

    //    医生修改资料接口-----姓名
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinfoname(@Path("id") long id,
                                            @Query("name") String name);

    //    医生修改资料接口-----sex
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinfosex(@Path("id") long id,
                                           @Query("sex") String sex);

    //    医生修改资料接口----age
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinfonage(@Path("id") long id,
                                            @Query("age") String age);

    //    医生修改资料接口-----headImg
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinfoheadImg(@Path("id") long id,
                                               @Query("headImg") String headImg);

    //    医生修改资料接口-----phoneOpen
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinfophoneOpen(@Path("id") long id,
                                                 @Query("phoneOpen") String phoneOpen);

    //    医生修改资料接口-----domain
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinfodomain(@Path("id") long id,
                                              @Query("domain") String domain);

    //    医生修改资料接口-----regionName
    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> modifyinforegionName(@Path("id") long id,
                                                  @Query("regionName") String regionName);

    //    首页医生获取病人列表
    @GET("/doctor/report/records.do")
    Observable<BaseResponse<List<PatientRecord>>> patientrecord(@Query("doctorId") long doctorId);

    //    医生查看机器体检用户报告详情
    @GET("/doctor/report/record/{rid}.do")
    Observable<BaseResponse<MyRecordInfo>> lookinfo(@Path("rid") int recordId);
//    医生查看随手记用户报告详情

    @GET("/doctor/report/note/{rid}.do")
    Observable<BaseResponse<List<NoteInfo>>> loonote(@Path("rid") int recordId);

    //    医生查看自测用户报告详情
    @GET("/doctor/report/self/{rid}.do")
    Observable<BaseResponse<MyZijianInfo>> loocheck(@Path("rid") int recordId);

    @PUT("/doctor/info/{id}.do")
    Observable<BaseResponse> changeDoctorInfo(@Path("id") long id, @QueryMap Map<String, String> options);

    //    医生个人创建指导接口
    @GET("/doctor/guide/createGuide.do")
    Observable<BaseResponse<PersonCreateGuide>> createguide(@Query("accountId") long accountId);

    //    指导项目接口
    @GET("/doctor/guide/items.do")
    Observable<BaseResponse<List<GuideItem>>> getGuideItem(@Query("itemType") int itemType,
                                                           @Query("type") String type);

    //保存医生指导接口
    @POST("/doctor/guide/save.action")
    Observable<BaseResponse> saveGuide(@Query("doctorId") long doctorId,
                                       @Query("accountId") long accountId,
                                       @Query("type") String type,
                                       @Query("items") String items,
                                       @Query("remark") String remark,
                                       @Query("recordIds") String recordIds,
                                       @Body String modify);

    //医生获取指导与接诊患者数目接口
    @GET("/doctor/info/count.do")
    Observable<BaseResponse<GuideInfo>> getGuideNum(@Query("doctorId") long doctorId);

}
