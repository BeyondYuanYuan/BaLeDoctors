package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by YuanYuan on 2016/10/20.
 * 医生获取指导患者数目与接诊患者数目
 */
public class GuideInfo {


    /**
     * guide : 10
     * user : 2
     */

    private Integer guide;
    private Integer user;

    public Integer getGuide() {
        return guide;
    }

    public void setGuide(Integer guide) {
        this.guide = guide;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }
}
