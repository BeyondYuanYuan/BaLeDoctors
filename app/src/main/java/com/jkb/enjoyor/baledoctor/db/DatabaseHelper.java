package com.jkb.enjoyor.baledoctor.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.jkb.enjoyor.baledoctor.entitry.DoctorInfo;
import com.jkb.enjoyor.baledoctor.entitry.EMModel;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private Context mContext;
    public static final String DB_NAME = "enjoyor.db";

    //	public static final int DB_VERSION = DegApplication.getInstance().getDBVersion();
    public static final int DB_VERSION = 1;

    public DatabaseHelper(Context context) {

        this(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;

    }

    public DatabaseHelper(Context context, String databaseName,
                          SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource cs) {
        // TODO Auto-generated method stub
        try {
            TableUtils.createTableIfNotExists(cs, UserInfo.class);
            TableUtils.createTableIfNotExists(cs, DoctorInfo.class);
            TableUtils.createTableIfNotExists(cs, EMModel.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sd, ConnectionSource cs, int arg2,
                          int arg3) {
        // TODO Auto-generated method stub
        try {
            TableUtils.dropTable(cs, UserInfo.class, true);
            TableUtils.dropTable(cs, DoctorInfo.class, true);
            TableUtils.dropTable(cs, EMModel.class, true);
            onCreate(sd, cs);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
