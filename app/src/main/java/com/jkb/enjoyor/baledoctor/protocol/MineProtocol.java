package com.jkb.enjoyor.baledoctor.protocol;

import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;
import com.jkb.enjoyor.baledoctor.model.MineModel;
import com.squareup.okhttp.RequestBody;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import rx.Observable;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public interface MineProtocol {
    @GET("/record/notes/947.action")
    Observable<BaseResponse<List<MineModel>>> getMine();

    @Multipart
    @POST("/doctor/upload/save/headimg.action")
    Observable<BaseResponse> uploadImage(@Part("origin") String origin, @Part("doctorId") String doctorId,
                                         @Part("file\"; filename=\"image.png\"") RequestBody imgs);

//    //获取人体成分接口
//    @GET("/app/body/evaluate.do")
//    Observable<BaseResponse<BodyConsist>> getBodyInfo(@Query("userId") long userId);
//
//    //    @GET("/{appName}/hs/list")
////    Observable<BaseResponse<List<Hospital>>> getInfo(@Query("areaId") long areaId);
////@POST("/{appName}/u/acct/update")
//Observable<BaseResponse> updateUserInfo(@QueryMap Map<String,Object>map, @Body UserInfo info );
    //用户上传头像接口
//    @POST("/healthstationserver/doupload/saveheadimg.do")
//    Observable<BaseResponse<UpLoadFile>> upLoadFile(@Query("file")File file,@Query("origin")String origin,@Query("accountId")long accountId,@Body String bodyConsist);

}
