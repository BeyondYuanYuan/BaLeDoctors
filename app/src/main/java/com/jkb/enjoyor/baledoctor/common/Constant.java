package com.jkb.enjoyor.baledoctor.common;

/**
 * Created by YuanYuan on 2016/8/24.
 * 常量
 */
public class Constant {
    //      测试连接
    public static final String TEST_URL = "http://115.28.37.145:9008/healthstationserver/";
//    public static final String TEST_URL = "http://192.168.66.37:8080/bale/";

    public static final String UPLOAD_HEAD = TEST_URL + "doctor/upload/save/headimg.action";


    public static String EMPWD = "passwordAAA";
    /*	体检类型：1.机器体检机录入2.手动录入3.随手记 */
    public static final int TYPE_MACHINE = 1;
    public static final int TYPE_MANUAL = 2;
    public static final int TYPE_NOTE = 3;
    public static final int DOMAIN = 100;
    public static final int NAME = 101;
    public static final int SEX = 102;

    public static final String VALUE_VOICE = "VALUE_VOICE";
    public static String getResources(int voice) {
        return TEST_URL + "resources/file/" + voice + ".action";
    }

    /**
     * 根据类型获取血糖最小参考值
     *
     * @param type
     * @return
     */
    public static double getBloodSugarMin(Integer type) {
        double min = 0;
        if (type != null) {
            switch (type.intValue()) {
                case 1://空腹血糖
                    min = 4.4;
                    break;
                case 2://餐后血糖
                    min = 4.4;
                    break;
                case 3://尿酸血糖
                    min = 4.4;
                    break;

                case 4://随机血糖
                    min = 4.4;
                    break;
                case 5://5早餐后
                    min = 4.4;
                    break;
                case 6://6午餐前
                    min = 4.4;
                    break;
                case 7://7午餐后
                    min = 4.4;
                    break;
                case 8://8晚餐前
                    min = 4.4;
                    break;
                case 9://9晚餐后
                    min = 4.4;
                    break;
                case 10://10睡前
                    min = 4.4;
                    break;
                default:
                    min = 4.4;
                    break;
            }
        }
        return min;
    }

    /**
     * 根据类型获取血糖最大参考值
     *
     * @param type
     * @return
     */
    public static double getBloodSugarMax(Integer type) {
        double min = 0;
        if (type != null) {
            switch (type) {
                case 1://空腹血糖
                    min = 7.0;
                    break;
                case 2://餐后血糖
                    min = 10.0;
                    break;
                case 3://尿酸血糖
                    min = 10.0;
                    break;
                case 4://随机血糖
                    min = 10.0;
                    break;
                case 5://5早餐后
                    min = 10.0;
                    break;
                case 6://6午餐前
                    min = 10.0;
                    break;
                case 7://7午餐后
                    min = 10.0;
                    break;
                case 8://8晚餐前
                    min = 10.0;
                    break;
                case 9://9晚餐后
                    min = 10.0;
                    break;
                case 10://10睡前
                    min = 10.0;
                    break;
                default:
                    min = 7.0;
                    break;
            }
        }
        return min;
    }

    /**
     * 血氧最小参考值
     */
    public static final double BO_BEST_MIN = 95;
    /**
     * 血氧最大参考值
     */
    public static final double BO_BEST_MAX = 99;
    /**
     * 舒张压-低压-理想最小值
     */
    public static final int diastolicPressure_BEST_MIN = 60;
    /**
     * 舒张压-低压-理想最大值
     */
    public static final int diastolicPressure_BEST_MAX = 90;
    /**
     * 收缩压-高压-理想最小值
     */
    public static final int systolicPressure_BEST_MIN = 90;
    /**
     * 收缩压-高压-理想最大值
     */
    public static final int systolicPressure_BEST_MAX = 140;
    //类型：1:空腹，4：随机血糖，5早餐后，6午餐前，7午餐后，8晚餐前，9晚餐后，10睡前
    public static final int TYPE_KONGFU = 1;
    public static final int TYPE_SUIJI = 4;
    public static final int TYPE_ZAOCANHOU = 5;
    public static final int TYPE_WUCANQIAN = 6;
    public static final int TYPE_WUCANHOU = 7;
    public static final int TYPE_WANCANQIAN = 8;
    public static final int TYPE_WANCANHOU = 9;
    public static final int TYPE_SHUIQIAN = 10;

    /**
     * 心率最佳最小参考值
     */
    public static final double ECG_BEST_MIN = 60;
    /**
     * 心率最佳最大参考值
     */
    public static final double ECG_BEST_MAX = 100;
    /**
     * 体温最小参考值
     */
    public static final double temperature_BEST_MIN = 36.0;
    /**
     * 体温最大参考值
     */
    public static final double temperature_BEST_MAX = 37.0;
}
