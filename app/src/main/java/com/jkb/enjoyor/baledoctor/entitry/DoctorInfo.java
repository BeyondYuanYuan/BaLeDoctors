package com.jkb.enjoyor.baledoctor.entitry;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by YuanYuan on 2016/9/14.
 */
public class DoctorInfo implements Parcelable {
    /**
     * ability :
     * age : null
     * atte : 0
     * barcode :
     * city : 杭州市
     * createTime : 2016-09-12 17:22:43
     * dept : 内一科
     * domain :
     * headImg :
     * hospital : 浙江第一人民医院
     * hospitalDeptId : 1
     * hospitalProTitleId : 1
     * id : 1
     * loginName : test
     * loginPwd :
     * name :
     * phoneNumber :
     * phoneOpen : 0
     * profTitle : 院长
     * province : 浙江
     * region : 西湖区
     * regionId : 1
     * sex : 0
     * state : 1
     */
    @DatabaseField(id = true)
    private int infoid;

    public int getInfoid() {
        return infoid;
    }

    public void setInfoid(int infoid) {
        this.infoid = infoid;
    }

    @DatabaseField
    private String ability;
    @DatabaseField
    private Integer age;
    @DatabaseField
    private int atte;
    @DatabaseField
    private String barcode;
    @DatabaseField
    private String city;
    @DatabaseField
    private String createTime;
    @DatabaseField
    private String dept;
    @DatabaseField
    private String domain;
    @DatabaseField
    private String headImg;
    @DatabaseField
    private String hospital;
    @DatabaseField
    private int hospitalDeptId;
    @DatabaseField
    private int hospitalProTitleId;
    @DatabaseField
    private long id;
    @DatabaseField
    private String loginName;
    @DatabaseField
    private String loginPwd;
    @DatabaseField
    private String name;
    @DatabaseField
    private String phoneNumber;
    @DatabaseField
    private int phoneOpen;
    @DatabaseField
    private String profTitle;
    @DatabaseField
    private String province;
    @DatabaseField
    private String region;
    @DatabaseField
    private int regionId;
    @DatabaseField
    private String sex;
    @DatabaseField
    private int state;
    @DatabaseField
    private String regionName;
    @DatabaseField
    private String dbHeadImage;

    public String getDbHeadImage() {
        return dbHeadImage;
    }

    public void setDbHeadImage(String dbHeadImage) {
        this.dbHeadImage = dbHeadImage;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }


    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public int getAtte() {
        return atte;
    }

    public void setAtte(int atte) {
        this.atte = atte;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDept() {
        return dept;
    }

    @Override
    public String toString() {
        return "DoctorInfo{" +
                "infoid=" + infoid +
                ", ability='" + ability + '\'' +
                ", age=" + age +
                ", atte=" + atte +
                ", barcode='" + barcode + '\'' +
                ", city='" + city + '\'' +
                ", createTime='" + createTime + '\'' +
                ", dept='" + dept + '\'' +
                ", domain='" + domain + '\'' +
                ", headImg='" + headImg + '\'' +
                ", hospital='" + hospital + '\'' +
                ", hospitalDeptId=" + hospitalDeptId +
                ", hospitalProTitleId=" + hospitalProTitleId +
                ", id=" + id +
                ", loginName='" + loginName + '\'' +
                ", loginPwd='" + loginPwd + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", phoneOpen=" + phoneOpen +
                ", profTitle='" + profTitle + '\'' +
                ", province='" + province + '\'' +
                ", region='" + region + '\'' +
                ", regionId=" + regionId +
                ", sex='" + sex + '\'' +
                ", state=" + state +
                '}';
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public int getHospitalDeptId() {
        return hospitalDeptId;
    }

    public void setHospitalDeptId(int hospitalDeptId) {
        this.hospitalDeptId = hospitalDeptId;
    }

    public int getHospitalProTitleId() {
        return hospitalProTitleId;
    }

    public void setHospitalProTitleId(int hospitalProTitleId) {
        this.hospitalProTitleId = hospitalProTitleId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPhoneOpen() {
        return phoneOpen;
    }

    public void setPhoneOpen(int phoneOpen) {
        this.phoneOpen = phoneOpen;
    }

    public String getProfTitle() {
        return profTitle;
    }

    public void setProfTitle(String profTitle) {
        this.profTitle = profTitle;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.infoid);
        dest.writeString(this.ability);
        dest.writeValue(this.age);
        dest.writeInt(this.atte);
        dest.writeString(this.barcode);
        dest.writeString(this.city);
        dest.writeString(this.createTime);
        dest.writeString(this.dept);
        dest.writeString(this.domain);
        dest.writeString(this.headImg);
        dest.writeString(this.hospital);
        dest.writeInt(this.hospitalDeptId);
        dest.writeInt(this.hospitalProTitleId);
        dest.writeLong(this.id);
        dest.writeString(this.loginName);
        dest.writeString(this.loginPwd);
        dest.writeString(this.name);
        dest.writeString(this.phoneNumber);
        dest.writeInt(this.phoneOpen);
        dest.writeString(this.profTitle);
        dest.writeString(this.province);
        dest.writeString(this.region);
        dest.writeInt(this.regionId);
        dest.writeString(this.sex);
        dest.writeInt(this.state);
        dest.writeString(this.regionName);
    }

    public DoctorInfo() {
    }

    protected DoctorInfo(Parcel in) {
        this.infoid = in.readInt();
        this.ability = in.readString();
        this.age = (Integer) in.readValue(Integer.class.getClassLoader());
        this.atte = in.readInt();
        this.barcode = in.readString();
        this.city = in.readString();
        this.createTime = in.readString();
        this.dept = in.readString();
        this.domain = in.readString();
        this.headImg = in.readString();
        this.hospital = in.readString();
        this.hospitalDeptId = in.readInt();
        this.hospitalProTitleId = in.readInt();
        this.id = in.readLong();
        this.loginName = in.readString();
        this.loginPwd = in.readString();
        this.name = in.readString();
        this.phoneNumber = in.readString();
        this.phoneOpen = in.readInt();
        this.profTitle = in.readString();
        this.province = in.readString();
        this.region = in.readString();
        this.regionId = in.readInt();
        this.sex = in.readString();
        this.state = in.readInt();
        this.regionName = in.readString();
    }

    public static final Parcelable.Creator<DoctorInfo> CREATOR = new Parcelable.Creator<DoctorInfo>() {
        @Override
        public DoctorInfo createFromParcel(Parcel source) {
            return new DoctorInfo(source);
        }

        @Override
        public DoctorInfo[] newArray(int size) {
            return new DoctorInfo[size];
        }
    };
}
