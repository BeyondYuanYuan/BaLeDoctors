package com.jkb.enjoyor.baledoctor.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.adapter.PatientRecordAdapter;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.entitry.GuideInfo;
import com.jkb.enjoyor.baledoctor.entitry.PatientRecord;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.ui.CreatGuideActivity;
import com.jkb.enjoyor.baledoctor.ui.MyNotesActivity;
import com.jkb.enjoyor.baledoctor.ui.MyPatientActivity;
import com.jkb.enjoyor.baledoctor.ui.MyRecordActivity;
import com.jkb.enjoyor.baledoctor.ui.MySelfCheckActivity;
import com.jkb.enjoyor.baledoctor.ui.PercentActivity;
import com.jkb.enjoyor.baledoctor.utils.QRCodeUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/5.
 * 首页
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {
    @Bind(R.id.home_fg_picture)
    ImageView home_fg_picture;
    @Bind(R.id.home_fg_code)
    ImageView home_fg_code;
    @Bind(R.id.home_fg_lin_jiezhen)
    LinearLayout home_fg_lin_jiezhen;
    @Bind(R.id.home_fg_lin_create)
    LinearLayout home_fg_lin_create;
    private PopupWindow mPopWindow;
    private View view;
    private boolean isClick = true;
    @Bind(R.id.home_fg_lv)
    ListView home_fg_lv;
    @Bind(R.id.home_fg_name)
    TextView home_fg_name;
    @Bind(R.id.home_fgjiezhen)
    TextView home_fgjiezhen;
    @Bind(R.id.home_fg_create)
    TextView home_fg_create;
    private int type = 0;//体检类型

    ArrayList<PatientRecord> list = new ArrayList<>();
    PatientRecordAdapter patientrecordAdapter;

    private UserInfo userInfo;
//    private DoctorInfo doctorInfo;

    private String mName;
    private String mImage;
    private String mQrduty;
    private String mDoctorId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fg_layout, container, false);
        ButterKnife.bind(this, view);
        userInfo = EnjoyorApplication.getInstance().getDBHelper().getUser();
//        doctorInfo = EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo();
        initView();//初始化医生信息
        initEvent();
        initData();
        return view;
    }

    private void initPopWindow(String mImage) {
        View contentView = LayoutInflater.from(getActivity()).inflate(R.layout.code_popwindow, null);
        ImageView popwiindow_picture = (ImageView) contentView.findViewById(R.id.popwiindow_picture);
        ImageView qrshow = (ImageView) contentView.findViewById(R.id.qrshow);
        TextView popwindow_name = (TextView) contentView.findViewById(R.id.popwindow_name);
        TextView qrduty = (TextView) contentView.findViewById(R.id.qrduty);

        mPopWindow = new PopupWindow(contentView);
        mPopWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mPopWindow.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
//        String mImages = StringUtils.finalHeadImage(userInfo.getHeadImg(), userInfo.getDbHeadImage());
        if (!StringUtils.isEmpty(mImage)) {
            ImageLoader.getInstance().displayImage(mImage, popwiindow_picture, EnjoyorApplication.option);
        }
        popwindow_name.setText("姓名  :" + mName);
        qrduty.setText("职称  ：" + mQrduty);

        //显示PopupWindow

        Drawable logo = getResources().getDrawable(R.mipmap.bale);
        Bitmap bm = ((BitmapDrawable) logo).getBitmap();

        Bitmap mBitMap = QRCodeUtil.createQRImage(mDoctorId, 1000, 1000, bm, "");
        qrshow.setImageBitmap(mBitMap);

        mPopWindow.setBackgroundDrawable(getDrawable());
        mPopWindow.setOutsideTouchable(false);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopWindow.dismiss();
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPopWindow.isShowing()) {
            mPopWindow.dismiss();
        }
    }

    private void itemClick() {
        home_fg_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ToastUtils.showToast(getActivity(), "您点击了第" + list.get(position).getType() + "条数据");
                type = list.get(position).getType();
                switch (type) {
                    case 1:
//                        ToastUtils.showToast(getActivity(), "您点击了体检机体检");
                        Intent intent_myrecord = new Intent(getActivity(), MyRecordActivity.class);
                        intent_myrecord.putExtra("recordId", list.get(position).getRecordId());
//                        Log.i("listid", list.get(position).getRecordId() + "");
                        startActivity(intent_myrecord);
                        break;
                    case 2:
//                        ToastUtils.showToast(getActivity(), "您点击了自测体检");
                        Intent intent_myself = new Intent(getActivity(), MySelfCheckActivity.class);
                        intent_myself.putExtra("recordId", list.get(position).getRecordId());
//                        Log.i("listid", list.get(position).getRecordId() + "");
                        if (!StringUtils.isEmpty(list.get(position).getRecordTime())) {
                            intent_myself.putExtra("recordTime", list.get(position).getRecordTime());
                        }
                        startActivity(intent_myself);
                        break;
                    case 3:
//                        ToastUtils.showToast(getActivity(), "您点击了随手记体检");
                        Intent intent_mynote = new Intent(getActivity(), MyNotesActivity.class);
                        intent_mynote.putExtra("recordId", list.get(position).getRecordId());
//                        Log.i("listid", list.get(position).getRecordId() + "");
                        startActivity(intent_mynote);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void initView() {
        if (userInfo != null) {
            mName = StringUtils.finalName(userInfo.getName(), userInfo.getLoginName(), userInfo.getUserPhoneNumber());
            home_fg_name.setText("姓名  :" + mName);
            mImage = StringUtils.finalHeadImage(userInfo.getHeadImg(), userInfo.getDbHeadImage());
//            mImage = userInfo.getHeadImg();
            if (!StringUtils.isEmpty(mImage)) {
                ImageLoader.getInstance().displayImage(mImage, home_fg_picture, EnjoyorApplication.option);

            }
            mQrduty = userInfo.getPerfTitle();
            mDoctorId = userInfo.getId() + "";
            initPopWindow(mImage);
        } else {
            home_fgjiezhen.setText("已接诊患者 ：" + 0 + " 人");
            home_fg_create.setText("已创建指导 ：" + 0 + " 人");
        }
    }


    //获取指导人数医生
    private void achieveGuide(UserInfo userInfo) {
        Subscription s;
        s = Server.HomeProtocolBuild(getActivity()).getGuideNum(EnjoyorApplication.getInstance().getDBHelper().getUser().getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            getGuideWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    private void getGuideWithResponse(BaseResponse<GuideInfo> data) {
        GuideInfo guideinfo = data.getData();
        if (null != guideinfo) {
            if (null != guideinfo.getGuide()) {
                home_fgjiezhen.setText("已接诊患者 ：" + guideinfo.getGuide() + " 人");
            } else {
                home_fgjiezhen.setText("已接诊患者 ：" + 0 + " 人");
            }
            if (null != guideinfo.getUser()) {
                home_fg_create.setText("已创建指导 ：" + guideinfo.getUser() + " 人");
            } else {
                home_fg_create.setText("已创建指导 ：" + 0 + " 人");
            }
        } else {
            home_fgjiezhen.setText("已接诊患者 ：" + 0 + " 人");
            home_fg_create.setText("已创建指导 ：" + 0 + " 人");
        }
    }


    private void initEvent() {
        home_fg_picture.setOnClickListener(this);
        home_fg_code.setOnClickListener(this);
        home_fg_lin_jiezhen.setOnClickListener(this);
        home_fg_lin_create.setOnClickListener(this);
    }

    private void initData() {
        Subscription s;
        s = Server.HomeProtocolBuild(getActivity()).patientrecord(EnjoyorApplication.getInstance().getDBHelper().getUser().getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            doWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);

    }

    private void doWithResponse(BaseResponse<List<PatientRecord>> data) {
        if (data.getData() instanceof ArrayList) {
            List<PatientRecord> _lsit = data.getData();
            list.clear();
            list.addAll(_lsit);
            if (list.size() > 0) {
                patientrecordAdapter = new PatientRecordAdapter(getActivity(), list, R.layout.home_fg_li_item);
                home_fg_lv.setAdapter(patientrecordAdapter);
                itemClick();
            }
        }


    }

    @Override
    public void onResume() {
        super.onResume();
//        initView();//初始化医生信息
        userInfo = EnjoyorApplication.getInstance().getDBHelper().getUser();
        if (null != userInfo) {
            achieveGuide(userInfo);
//            initView();
        } else {
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_fg_picture:
                Intent intent_persent = new Intent(getActivity(), PercentActivity.class);
                startActivityForResult(intent_persent, PercentActivity.FROM_HOME);
                break;
            case R.id.home_fg_code:
                if (mPopWindow != null) {
                    mPopWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
                } else {
                    showSnackBar("请先登录");
                }
                break;
            case R.id.home_fg_lin_jiezhen:
//                ToastUtils.showToast(getActivity(), "点击了我的患者");
                Intent intent_patient = new Intent(getActivity(), MyPatientActivity.class);
                intent_patient.putParcelableArrayListExtra("list", list);
                startActivity(intent_patient);
                break;
            case R.id.home_fg_lin_create:
//                ToastUtils.showToast(getActivity(), "点击了创建患者");
                Intent intent_creat = new Intent(getActivity(), CreatGuideActivity.class);
                startActivity(intent_creat);
                break;
        }
    }


    /**
     * 生成一个 透明的背景图片
     *
     * @return
     */
    private Drawable getDrawable() {
        ShapeDrawable bgdrawable = new ShapeDrawable(new OvalShape());
        bgdrawable.getPaint().setColor(getResources().getColor(R.color.transparent));
        return bgdrawable;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == PercentActivity.FROM_HOME) {
                userInfo = EnjoyorApplication.getInstance().getDBHelper().getUser();
                initView();
            }
        }
    }
}