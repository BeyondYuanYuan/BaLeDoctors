package com.jkb.enjoyor.baledoctor.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/10.
 * 版本信息页面
 */
public class VersionActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.version_check)
    TextView version_check;

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initEvent() {
        re_back.setOnClickListener(this);
        navigation_name.setText("版本信息");
        version_check.setOnClickListener(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.version_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.version_check:
                showSnackBar("当前已经是最新版本");
                break;
        }
    }
}
