package com.jkb.enjoyor.baledoctor.protocol;

import com.jkb.enjoyor.baledoctor.entitry.GuideListInfo;
import com.jkb.enjoyor.baledoctor.entitry.MyGuide;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by YuanYuan on 2016/10/17.
 */
public interface GuideProtocol {
    //健康指导列表接口
    @GET("/doctor/guide/list.do")
    Observable<BaseResponse<List<GuideListInfo>>> getguide(@Query("doctorId") Long doctorId,
                                                           @Query("pageSize") int pageSize);

    ///我的指导详细页面
    @GET("/doctor/guide/{guiduId}.do")
//    void getUserPhoto(@Path("id") int id, Callback<Photo> cb);
    Observable<BaseResponse<MyGuide>> getMyGuide(@Path("guiduId") Long id);
}
