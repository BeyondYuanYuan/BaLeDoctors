package com.jkb.enjoyor.baledoctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.adapter.HealthGuideAdapter;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.entitry.GuideListInfo;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.ui.GuideActivity;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/5.
 * 健康指导
 */
public class GuideFragment extends BaseFragment {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.guide_fg_lv)
    ListView guide_fg_lv;
    private long doctorid;
    @Bind(R.id.guide_fg_tv)
    TextView guide_fg_tv;
    private HealthGuideAdapter healthGuideAdapter;
    private List<GuideListInfo> listInfos = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.guide_fg_layout, container, false);
        ButterKnife.bind(this, view);
        doctorid = EnjoyorApplication.getInstance().getDBHelper().getUser().getId();
        initview();
        initData();
        return view;
    }

    private void initClick() {
        guide_fg_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ToastUtils.showToast(getActivity(), "您点击了创建指导给" + listInfos.get(position).getUsername());
                Intent intent_showguide = new Intent(getActivity(), GuideActivity.class);
                if (listInfos.get(position).getGuideId()!=null){
                    intent_showguide.putExtra("guiduId", listInfos.get(position).getGuideId());
                    Log.d("guiduId",listInfos.get(position).getGuideId()+"");
                    startActivity(intent_showguide);
                }else {
                    ToastUtils.showToast(getActivity(),"无法查看该用户健康指导");
                }
            }
        });
    }

    private void initData() {
        Subscription s;
        s = Server.GuideProtocolBuild(getActivity()).getguide(EnjoyorApplication.getInstance().getDBHelper().getUser().getId(), 500)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);

    }

    private void initview() {
        navigation_name.setText("健康指导");
        re_back.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof ArrayList) {
            listInfos = (List<GuideListInfo>) o;
            healthGuideAdapter = new HealthGuideAdapter(getActivity(), listInfos, R.layout.healthguide_fg_lv_item);
            guide_fg_tv.setVisibility(View.GONE);
            guide_fg_lv.setVisibility(View.VISIBLE);
//            Log.d("guideinfo", listInfos.toString());
            if (listInfos.size() > 0) {
                guide_fg_lv.setAdapter(healthGuideAdapter);
                initClick();
            }
        } else {
            guide_fg_tv.setVisibility(View.VISIBLE);
            guide_fg_lv.setVisibility(View.GONE);
        }

    }

}
