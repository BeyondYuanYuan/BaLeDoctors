package com.jkb.enjoyor.baledoctor.utils;
/***
 *
 * @param <K>
 * @param <V>
 */
public interface KeySort<K, V> {
	public K getKey(V v);
}
