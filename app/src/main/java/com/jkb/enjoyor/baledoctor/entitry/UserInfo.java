package com.jkb.enjoyor.baledoctor.entitry;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by YuanYuan on 2016/9/14.
 * 医生登录注册个人信息
 */
public class UserInfo {
    /**
     * barcode :
     * guides : 0
     * headImg :
     * id : 1
     * loginName : test
     * name :
     * patients : 0
     * perfTitle :
     */
    @DatabaseField(id = true)
    private int userId;
    @DatabaseField
    private String barcode;
    @DatabaseField
    private int guides;
    @DatabaseField
    private String headImg;
    @DatabaseField
    private Long id;
    @DatabaseField
    private String loginName;
    @DatabaseField
    private String name;
    @DatabaseField
    private int patients;
    @DatabaseField
    private String perfTitle;
    @DatabaseField
    private String userPhoneNumber;
    @DatabaseField
    private String userPassword;
    @DatabaseField
    private String dbHeadImage;

    public String getDbHeadImage() {
        return dbHeadImage;
    }

    public void setDbHeadImage(String dbHeadImage) {
        this.dbHeadImage = dbHeadImage;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGuides() {
        return guides;
    }

    public void setGuides(int guides) {
        this.guides = guides;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPatients() {
        return patients;
    }

    public void setPatients(int patients) {
        this.patients = patients;
    }

    public String getPerfTitle() {
        return perfTitle;
    }

    public void setPerfTitle(String perfTitle) {
        this.perfTitle = perfTitle;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userId=" + userId +
                ", barcode='" + barcode + '\'' +
                ", guides=" + guides +
                ", headImg='" + headImg + '\'' +
                ", id=" + id +
                ", loginName='" + loginName + '\'' +
                ", name='" + name + '\'' +
                ", patients=" + patients +
                ", perfTitle='" + perfTitle + '\'' +
                ", userPhoneNumber='" + userPhoneNumber + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", dbHeadImage='" + dbHeadImage + '\'' +
                '}';
    }
}
