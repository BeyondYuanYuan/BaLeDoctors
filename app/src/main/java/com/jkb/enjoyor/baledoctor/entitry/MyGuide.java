package com.jkb.enjoyor.baledoctor.entitry;

import java.util.List;

/**
 * Created by YuanYuan on 2016/10/17.
 */
public class MyGuide {
    @Override
    public String toString() {
        return "MyGuide{" +
                "age=" + age +
                ", createTime='" + createTime + '\'' +
                ", guideId=" + guideId +
                ", recordBP=" + recordBP +
                ", remark='" + remark + '\'' +
                ", reportId=" + reportId +
                ", sex='" + sex + '\'' +
                ", type='" + type + '\'' +
                ", username='" + username + '\'' +
                ", guideItems=" + guideItems +
                ", recordBSes=" + recordBSes +
                '}';
    }

    /**
     * age : null
     * createTime : 2016-10-13 14:19:52
     * guideId : 1
     * guideItems : [{"createtime":"2016-10-13 10:10:45","id":1,"itemType":null,"key":"限制总能量","sort":1,"type":"hypertension","val":"肥胖者应该节食减肥，使体重减轻每周1-1.5kg为宜。"}]
     * recordBP : {"checkTime":"","createTime":"2016-05-18 14:53:00","diastolicPressure":57,"id":23,"pulse":65,"recordId":28,"result":"","systolicPressure":96}
     * recordBSes : [{"bloodSugar":"3.8","bloodSugarType":1,"chol":"2.77","createTime":"2016-05-18 14:53:19","id":21,"recordId":28,"result":"1","us":"0.54"}]
     * remark : test
     * reportId : 28
     * sex : 女
     * type : hypertension
     * username : g     g
     */

    private Integer age;
    private String createTime;
    private String headImg;

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    private Integer guideId;
    /**
     * checkTime :
     * createTime : 2016-05-18 14:53:00
     * diastolicPressure : 57
     * id : 23
     * pulse : 65
     * recordId : 28
     * result :
     * systolicPressure : 96
     */

    private RecordBPEntity recordBP;
    private String remark;
    private Integer reportId;
    private String sex;
    private String type;
    private String username;
    /**
     * createtime : 2016-10-13 10:10:45
     * id : 1
     * itemType : null
     * key : 限制总能量
     * sort : 1
     * type : hypertension
     * val : 肥胖者应该节食减肥，使体重减轻每周1-1.5kg为宜。
     */

    private List<GuideItemsEntity> guideItems;
    /**
     * bloodSugar : 3.8
     * bloodSugarType : 1
     * chol : 2.77
     * createTime : 2016-05-18 14:53:19
     * id : 21
     * recordId : 28
     * result : 1
     * us : 0.54
     */

    private List<RecordBSesEntity> recordBSes;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getGuideId() {
        return guideId;
    }

    public void setGuideId(Integer guideId) {
        this.guideId = guideId;
    }

    public RecordBPEntity getRecordBP() {
        return recordBP;
    }

    public void setRecordBP(RecordBPEntity recordBP) {
        this.recordBP = recordBP;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<GuideItemsEntity> getGuideItems() {
        return guideItems;
    }

    public void setGuideItems(List<GuideItemsEntity> guideItems) {
        this.guideItems = guideItems;
    }

    public List<RecordBSesEntity> getRecordBSes() {
        return recordBSes;
    }

    public void setRecordBSes(List<RecordBSesEntity> recordBSes) {
        this.recordBSes = recordBSes;
    }

    public static class RecordBPEntity {
        private String checkTime;
        private String createTime;
        private Integer diastolicPressure;
        private Integer id;
        private Integer pulse;
        private Integer recordId;
        private String result;
        private Integer systolicPressure;

        public String getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(String checkTime) {
            this.checkTime = checkTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Integer getDiastolicPressure() {
            return diastolicPressure;
        }

        public void setDiastolicPressure(Integer diastolicPressure) {
            this.diastolicPressure = diastolicPressure;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPulse() {
            return pulse;
        }

        public void setPulse(Integer pulse) {
            this.pulse = pulse;
        }

        public Integer getRecordId() {
            return recordId;
        }

        public void setRecordId(Integer recordId) {
            this.recordId = recordId;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public Integer getSystolicPressure() {
            return systolicPressure;
        }

        public void setSystolicPressure(Integer systolicPressure) {
            this.systolicPressure = systolicPressure;
        }
    }

    public static class GuideItemsEntity {
        private String createtime;
        private Integer id;
        private Integer itemType;
        private String key;
        private Integer sort;
        private String type;
        private String val;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getItemType() {
            return itemType;
        }

        public void setItemType(Integer itemType) {
            this.itemType = itemType;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getSort() {
            return sort;
        }

        public void setSort(Integer sort) {
            this.sort = sort;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getVal() {
            return val;
        }

        public void setVal(String val) {
            this.val = val;
        }
    }

    public static class RecordBSesEntity {
        private String bloodSugar;
        private Integer bloodSugarType;
        private String chol;
        private String createTime;
        private Integer id;
        private Integer recordId;
        private String result;
        private String us;

        public String getBloodSugar() {
            return bloodSugar;
        }

        public void setBloodSugar(String bloodSugar) {
            this.bloodSugar = bloodSugar;
        }

        public Integer getBloodSugarType() {
            return bloodSugarType;
        }

        public void setBloodSugarType(Integer bloodSugarType) {
            this.bloodSugarType = bloodSugarType;
        }

        public String getChol() {
            return chol;
        }

        public void setChol(String chol) {
            this.chol = chol;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRecordId() {
            return recordId;
        }

        public void setRecordId(Integer recordId) {
            this.recordId = recordId;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getUs() {
            return us;
        }

        public void setUs(String us) {
            this.us = us;
        }
    }
}
