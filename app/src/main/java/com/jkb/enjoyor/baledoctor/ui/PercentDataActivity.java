package com.jkb.enjoyor.baledoctor.ui;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.OptionsPickerView;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.DoctorInfo;
import com.jkb.enjoyor.baledoctor.entitry.ProvinceBean;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.ImageUtils;
import com.jkb.enjoyor.baledoctor.utils.JsonFileReader;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.net.AsyncHttpUtil;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/9.
 * 个人资料页面
 */
public class PercentDataActivity extends ToolBarActivity implements View.OnClickListener {

    public static final int FROM_PERCENT = 1;
    @Bind(R.id.percent_data_tv_name)
    TextView percent_data_tv_name;
    @Bind(R.id.percent_data_tv_sex)
    TextView percent_data_tv_sex;
    @Bind(R.id.percent_data_tv_age)
    TextView percent_data_tv_age;
    @Bind(R.id.percent_data_tv_phone)
    TextView percent_data_tv_phone;
    @Bind(R.id.percent_data_tv_hospital)
    TextView percent_data_tv_hospital;
    @Bind(R.id.percent_data_tv_dept)
    TextView percent_data_tv_dept;
    @Bind(R.id.percent_data_tv_address)
    TextView percent_data_tv_address;
    @Bind(R.id.percent_data_tv_duty)
    TextView percent_data_tv_duty;

    @Bind(R.id.percentdata_rel_advantage)
    RelativeLayout percentdata_rel_advantage;
    @Bind(R.id.percentdata_rel_sex)
    RelativeLayout percentdata_rel_sex;
    @Bind(R.id.percentdata_rel_pic)
    RelativeLayout percentdata_rel_pic;
    @Bind(R.id.percentdata_rel_name)
    RelativeLayout percentdata_rel_name;
    @Bind(R.id.percentdata_rel_age)
    RelativeLayout percentdata_rel_age;
    @Bind(R.id.percent_data_isopen)
    CheckBox percent_data_isopen;
    @Bind(R.id.percentdata_rel_phone)
    RelativeLayout percentdata_rel_phone;
    @Bind(R.id.percentdata_rel_district)
    RelativeLayout percentdata_rel_district;
    @Bind(R.id.percent_data_tv_domain)
    TextView percent_data_tv_domain;
    @Bind(R.id.percent_data_img_logo)
    ImageView percent_data_img_logo;
    //    @Bind(R.id.re_back)RelativeLayout re_back;
    private static final int PHOTO_REQUEST_CAREMA = 1;// 拍照
    private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
    private static final int PHOTO_REQUEST_CUT = 3;// 结果
    /* 头像名称 */
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private File tempFile;
    File path_camera;
    String path;


    //    省市三级联动
    OptionsPickerView pvOptions;
    //  省份
    ArrayList<ProvinceBean> provinceBeanList = new ArrayList<>();
    //  城市
    ArrayList<String> cities;
    ArrayList<List<String>> cityList = new ArrayList<>();
    //  区/县
    ArrayList<String> district;
    ArrayList<List<String>> districts;
    ArrayList<List<List<String>>> districtList = new ArrayList<>();
    private DoctorInfo doctorInfo;
    private UserInfo userInfo;

    //医生修改信息
    private Map<String, String> map = new HashMap<>();
    String isopen = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.percentdata_ac_layout, false);
        ButterKnife.bind(this);

        userInfo = EnjoyorApplication.getInstance().getDBHelper().getUser();

        initHead();
        initEvent();
        initIsOpen();
        initPickerView();
        doctorInfo = EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo();
        initDoctorInfo();
    }

    private void initHead() {
        setTitle("个人资料");
        getLeftBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                setResult(FROM_PERCENT, intent);
                finish();
            }
        });
    }

    private void initEvent() {
        percentdata_rel_district.setOnClickListener(this);
        percentdata_rel_advantage.setOnClickListener(this);
        percentdata_rel_sex.setOnClickListener(this);
        percentdata_rel_name.setOnClickListener(this);
        percentdata_rel_age.setOnClickListener(this);
        percent_data_isopen.setOnClickListener(this);
        percentdata_rel_phone.setOnClickListener(this);
        percentdata_rel_pic.setOnClickListener(this);
    }

    private void initIsOpen() {
        map.clear();
        percent_data_isopen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    ToastUtils.showToast(PercentDataActivity.this, "亲！您已经同意公开资料了哦");
                    isopen = "1";
                    doctorInfo.setState(1);
                    map.put("phoneOpen", isopen);
                } else {
//                    ToastUtils.showToast(PercentDataActivity.this, "亲！为了更好的为您服务，请公开联系方式哦！");
                    isopen = "0";
                    doctorInfo.setState(0);
                    map.put("phoneOpen", isopen);
                }
//                Log.d("-------------", "isopen");
//                changeDoctorInfo();
            }
        });
    }

    private void initPickerView() {
        //  创建选项选择器
        pvOptions = new OptionsPickerView(this);

        //  获取json数据
        String province_data_json = JsonFileReader.getJson(this, "province_data.json");
        //  解析json数据
        parseJson(province_data_json);
        //  设置三级联动效果
        pvOptions.setPicker(provinceBeanList, cityList, districtList, true);
        //  设置选择的三级单位
        //pvOptions.setLabels("省", "市", "区");
        //pvOptions.setTitle("选择城市");
        //  设置是否循环滚动
        pvOptions.setCyclic(false, false, false);
        // 设置默认选中的三级项目
//        pvOptions.setSelectOptions(0, 0, 0);

//        if (!StringUtils.isEmpty(percent_data_tv_address.getText())) {
//            pvOptions.setLabels("甘肃省","张掖市","山丹县");
//        }else{
        pvOptions.setSelectOptions(0, 0, 0);
//        }

        //  监听确定选择按钮
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                //返回的分别是三个级别的选中位置
                String city = provinceBeanList.get(options1).getPickerViewText();
                String address = null;
                //  如果是直辖市或者特别行政区只设置市和区/县
                if ("北京市".equals(city) || "上海市".equals(city) || "天津市".equals(city) || "重庆市".equals(city) || "澳门".equals(city) || "香港".equals(city)) {
                    address = provinceBeanList.get(options1).getPickerViewText()
                            + " " + districtList.get(options1).get(option2).get(options3);
                } else {
                    address = provinceBeanList.get(options1).getPickerViewText()
                            + " " + cityList.get(options1).get(option2)
                            + " " + districtList.get(options1).get(option2).get(options3);
                }
//                Log.i("address", address);
                percent_data_tv_address.setText(address);
                map.put("regionName", address);
                doctorInfo.setRegionName(address);
//                Log.d("-------------", "address");
                changeDoctorInfo();
            }
        });
    }

    //  解析json填充集合
    public void parseJson(String str) {
        try {
            //  获取json中的数组
            JSONArray jsonArray = new JSONArray(str);
            //  遍历数据组
            for (int i = 0; i < jsonArray.length(); i++) {
                //  获取省份的对象
                JSONObject provinceObject = jsonArray.optJSONObject(i);
                //  获取省份名称放入集合
                String provinceName = provinceObject.getString("name");
                provinceBeanList.add(new ProvinceBean(provinceName));
                //  获取城市数组
                JSONArray cityArray = provinceObject.optJSONArray("city");
                cities = new ArrayList<>();//   声明存放城市的集合
                districts = new ArrayList<>();//声明存放区县集合的集合
                //  遍历城市数组
                for (int j = 0; j < cityArray.length(); j++) {
                    //  获取城市对象
                    JSONObject cityObject = cityArray.optJSONObject(j);
                    //  将城市放入集合
                    String cityName = cityObject.optString("name");
                    cities.add(cityName);
                    district = new ArrayList<>();// 声明存放区县的集合
                    //  获取区县的数组
                    JSONArray areaArray = cityObject.optJSONArray("area");
                    //  遍历区县数组，获取到区县名称并放入集合
                    for (int k = 0; k < areaArray.length(); k++) {
                        String areaName = areaArray.getString(k);
                        district.add(areaName);
                    }
                    //  将区县的集合放入集合
                    districts.add(district);
                }
                //  将存放区县集合的集合放入集合
                districtList.add(districts);
                //  将存放城市的集合放入集合
                cityList.add(cities);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        initDoctorInfo();
//    }

    private void initDoctorInfo() {

        String name = StringUtils.finalName(doctorInfo.getName(), doctorInfo.getLoginName(), doctorInfo.getPhoneNumber());
        percent_data_tv_name.setText(name);

        String headimage = StringUtils.finalHeadImage(doctorInfo.getHeadImg(), doctorInfo.getDbHeadImage());
        if (!StringUtils.isEmpty(headimage)) {
            ImageLoader.getInstance().displayImage(headimage, percent_data_img_logo, EnjoyorApplication.option);
        }

        if (doctorInfo.getSex() == null) {
            percent_data_tv_sex.setText("");
        } else if ("1".equals("" + doctorInfo.getSex())) {
            percent_data_tv_sex.setText("男");
        } else if ("2".equals("" + doctorInfo.getSex())) {
            percent_data_tv_sex.setText("女");
        }
        if (doctorInfo.getAge() != null) {
            percent_data_tv_age.setText(doctorInfo.getAge() + "");
        } else {
            percent_data_tv_age.setText("");
        }
        if (!StringUtils.isEmpty(EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo().getState() + "")) {
            if (EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo().getState() == 1) {
                percent_data_isopen.setChecked(true);
            } else {
                percent_data_isopen.setChecked(false);
            }
        }
        percent_data_tv_phone.setText(doctorInfo.getLoginName() + "");
        percent_data_tv_hospital.setText(doctorInfo.getHospital() + "");
        percent_data_tv_dept.setText(doctorInfo.getDept() + "");
        percent_data_tv_address.setText(doctorInfo.getRegionName() + "");
        percent_data_tv_duty.setText(doctorInfo.getProfTitle() + "");
        percent_data_tv_domain.setText(doctorInfo.getDomain() + "");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.percentdata_rel_pic:
                setpicture();
                break;
            case R.id.percentdata_rel_name:
                Intent intent_name = new Intent(this, NameActivity.class);
                if (!StringUtils.isEmpty(percent_data_tv_name.getText())) {
                    intent_name.putExtra("name", percent_data_tv_name.getText());
                }
                startActivityForResult(intent_name, Constant.NAME);
                break;
            case R.id.percentdata_rel_sex:
                Intent intent_sex = new Intent(this, SexActivity.class);
                startActivityForResult(intent_sex, Constant.SEX);
                break;
            case R.id.percentdata_rel_age:
                break;
            case R.id.percentdata_rel_phone:

                break;
            case R.id.percentdata_rel_district:
                pvOptions.show();
                break;
            case R.id.percentdata_rel_advantage:
                Intent intent_advantage = new Intent(this, AdvanageActivity.class);
                startActivityForResult(intent_advantage, Constant.DOMAIN);
                break;
        }
    }

    //设置头像
    private void setpicture() {
        View view = this.getLayoutInflater().inflate(R.layout.photo_choose_dialog,
                null);
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();
        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = this.getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        view.findViewById(R.id.picture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 激活系统图库，选择一张图片
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_GALLERY
                startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 激活相机
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                // 判断存储卡是否可以用，可用进行存储
                if (hasSdcard()) {
                    tempFile = new File(Environment.getExternalStorageDirectory(),
                            PHOTO_FILE_NAME);
                    // 从文件中创建uri
                    Uri uri = Uri.fromFile(tempFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CAREMA
                    startActivityForResult(intent, PHOTO_REQUEST_CAREMA);
                    dialog.dismiss();
                }

            }
        });
        view.findViewById(R.id.cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /*
  * 判断sdcard是否被挂载
   */
    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    //相册跟拍照回调
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PHOTO_REQUEST_CAREMA) {
                // 从相机返回的数据
                if (hasSdcard()) {
                    path_camera = ImageUtils.scal(tempFile.getPath());
                    Uri uri = Uri.parse("file:///" + path_camera.toString());
                    ImageLoader.getInstance().displayImage(uri.toString(), percent_data_img_logo, EnjoyorApplication.option);
                    savePicture(path_camera);
                } else {
                    Toast.makeText(this, "未找到存储卡，无法存储照片！", Toast.LENGTH_LONG).show();
                }
            }
        }
        if (data != null) {
            map.clear();
            switch (requestCode) {
                case Constant.NAME:
                    map.put("name", data.getStringExtra("name"));
                    doctorInfo.setName(data.getStringExtra("name"));
                    userInfo.setName(data.getStringExtra("name"));
                    changeDoctorInfo();
                    break;
                case Constant.SEX:
                    map.put("sex", data.getStringExtra("sexinfo"));
                    doctorInfo.setSex(data.getStringExtra("sexinfo"));
                    changeDoctorInfo();
                    break;
                case Constant.DOMAIN:
                    map.put("domain", data.getStringExtra("domain"));
                    doctorInfo.setDomain(data.getStringExtra("domain"));
                    changeDoctorInfo();
                    break;
            }

            if (requestCode == PHOTO_REQUEST_GALLERY) {
                // 从相册返回的数据
                if (data != null) {
                    // 得到图片的全路径
                    Uri uri = data.getData();
                    String[] proj = {MediaStore.Images.Media.DATA};
                    if (uri!=null){
                        Cursor cursor = this.managedQuery(uri, proj, null, null, null);
                        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//获取用户选择图片的索引值
                        cursor.moveToFirst();
                        path = cursor.getString(column_index);
                        File file = new File(path);
                        File file1 = ImageUtils.scal(file.getPath());
                        Uri uri2 = Uri.parse("file:///" + file1.toString());
                        ImageLoader.getInstance().displayImage(uri2.toString(), percent_data_img_logo, EnjoyorApplication.option);
                        savePicture(file1);
                    }
                }
            }
        }
    }


    /*----------------------------修改用户信息---------------------------*/
    private void changeDoctorInfo() {
        Subscription s;
        s = Server.HomeProtocolBuild(this).changeDoctorInfo(doctorInfo.getId(), map)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if ((boolean) o) {
            EnjoyorApplication.getInstance().getDBHelper().saveDoctorInfo(doctorInfo);
            EnjoyorApplication.getInstance().getDBHelper().saveUser(userInfo);
            doctorInfo = EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo();
            if (doctorInfo != null) {
                initDoctorInfo();
            }
            showSnackBar("修改成功");

        } else {
            showSnackBar("修改失败");
        }
    }


    /**
     * 数据库上传相册返回的头像数据
     *
     * @param file
     */
    private void savePicture(File file) {
        RequestParams param = new RequestParams();
        param.add("doctorId", doctorInfo.getId() + "");
        param.add("origin", "ANDROIDAPP");
        Uri uri = Uri.parse("file:///" + file.toString());

        try {
            param.put("file", file);
            AsyncHttpUtil.post(Constant.UPLOAD_HEAD, param, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    String json = new String(bytes);
                    JSONObject message = null;
                    try {
                        message = new JSONObject(json);
                        int result = message.getInt("result");
                        JSONArray jsonArray = message.getJSONArray("data");
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (result == 1) {
                            String headimg = jsonObject.getString("basePath") + "/" + jsonObject.getString("fileName");
                            map.put("headImg", headimg);

                            String pathfile = file.getAbsolutePath();
                            doctorInfo.setDbHeadImage(uri.toString());
                            userInfo.setHeadImg(headimg);
                            userInfo.setDbHeadImage(uri.toString());
                            changeDoctorInfo();
                            file.delete();
                            showSnackBar("上传成功");
                        } else {
                            showSnackBar("上传失败");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = getIntent();
            setResult(FROM_PERCENT, intent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
