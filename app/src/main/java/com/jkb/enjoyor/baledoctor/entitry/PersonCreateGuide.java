package com.jkb.enjoyor.baledoctor.entitry;

import java.util.List;

/**
 * Created by YuanYuan on 2016/10/18.
 * 医生向个人患者创建指导实体类
 */
public class PersonCreateGuide {

    @Override
    public String toString() {
        return "PersonCreateGuide{" +
                "age=" + age +
                ", headImg='" + headImg + '\'' +
                ", recordBP=" + recordBP +
                ", reportId=" + reportId +
                ", sex='" + sex + '\'' +
                ", username='" + username + '\'' +
                ", recordBSes=" + recordBSes +
                '}';
    }

    /**
     * age : null
     * headImg : /files/images/headimg/6eb232ba-7dc5-43a3-8883-4d39b4518607.jpg
     * recordBP : {"checkTime":"","createTime":"2016-05-18 14:53:00","diastolicPressure":57,"id":23,"pulse":65,"recordId":28,"result":"","systolicPressure":96}
     * recordBSes : [{"bloodSugar":"3.8","bloodSugarType":1,"chol":"2.77","createTime":"2016-05-18 14:53:19","id":21,"recordId":28,"result":"1","us":"0.54"}]
     * reportId : 28
     * sex : 女
     * username : 崔丽
     */

    private Integer age;
    private String headImg;
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * checkTime :
     * createTime : 2016-05-18 14:53:00
     * diastolicPressure : 57
     * id : 23
     * pulse : 65
     * recordId : 28
     * result :
     * systolicPressure : 96
     */

    private RecordBPEntity recordBP;
    private Integer reportId;
    private String sex;
    private String username;
    /**
     * bloodSugar : 3.8
     * bloodSugarType : 1
     * chol : 2.77
     * createTime : 2016-05-18 14:53:19
     * id : 21
     * recordId : 28
     * result : 1
     * us : 0.54
     */

    private List<RecordBSesEntity> recordBSes;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public RecordBPEntity getRecordBP() {
        return recordBP;
    }

    public void setRecordBP(RecordBPEntity recordBP) {
        this.recordBP = recordBP;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<RecordBSesEntity> getRecordBSes() {
        return recordBSes;
    }

    public void setRecordBSes(List<RecordBSesEntity> recordBSes) {
        this.recordBSes = recordBSes;
    }

    public static class RecordBPEntity {
        private String checkTime;
        private String createTime;
        private Integer diastolicPressure;
        private Integer id;
        private Integer pulse;
        private Integer recordId;
        private String result;
        private Integer systolicPressure;

        public String getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(String checkTime) {
            this.checkTime = checkTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Integer getDiastolicPressure() {
            return diastolicPressure;
        }

        public void setDiastolicPressure(Integer diastolicPressure) {
            this.diastolicPressure = diastolicPressure;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPulse() {
            return pulse;
        }

        public void setPulse(Integer pulse) {
            this.pulse = pulse;
        }

        public Integer getRecordId() {
            return recordId;
        }

        public void setRecordId(Integer recordId) {
            this.recordId = recordId;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public Integer getSystolicPressure() {
            return systolicPressure;
        }

        public void setSystolicPressure(Integer systolicPressure) {
            this.systolicPressure = systolicPressure;
        }
    }

    public static class RecordBSesEntity {
        private String bloodSugar;
        private Integer bloodSugarType;
        private String chol;
        private String createTime;
        private Integer id;
        private Integer recordId;
        private String result;
        private String us;

        public String getBloodSugar() {
            return bloodSugar;
        }

        public void setBloodSugar(String bloodSugar) {
            this.bloodSugar = bloodSugar;
        }

        public Integer getBloodSugarType() {
            return bloodSugarType;
        }

        public void setBloodSugarType(Integer bloodSugarType) {
            this.bloodSugarType = bloodSugarType;
        }

        public String getChol() {
            return chol;
        }

        public void setChol(String chol) {
            this.chol = chol;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRecordId() {
            return recordId;
        }

        public void setRecordId(Integer recordId) {
            this.recordId = recordId;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getUs() {
            return us;
        }

        public void setUs(String us) {
            this.us = us;
        }
    }
}
