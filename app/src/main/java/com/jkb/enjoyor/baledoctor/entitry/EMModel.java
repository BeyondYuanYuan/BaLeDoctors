package com.jkb.enjoyor.baledoctor.entitry;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Administrator on 2016/10/12.
 */
public class EMModel {
    @DatabaseField(id = true)
    private int id;
    @DatabaseField
    private String EMUsername;
    @DatabaseField
    private String EMPassWord;
    @DatabaseField
    private Boolean EMLogined;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEMUsername() {
        return EMUsername;
    }

    public void setEMUsername(String EMUsername) {
        this.EMUsername = EMUsername;
    }

    public String getEMPassWord() {
        return EMPassWord;
    }

    public void setEMPassWord(String EMPassWord) {
        this.EMPassWord = EMPassWord;
    }

    public Boolean getEMLogined() {
        return EMLogined;
    }

    public void setEMLogined(Boolean EMLogined) {
        this.EMLogined = EMLogined;
    }
}
