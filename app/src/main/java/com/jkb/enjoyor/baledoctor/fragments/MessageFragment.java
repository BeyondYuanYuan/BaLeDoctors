package com.jkb.enjoyor.baledoctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hyphenate.chat.EMConversation;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.ui.EaseConversationListFragment;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.BaseDate;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.ui.EMmessageActivity;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.phoneticsearch.SortModel;

import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/5.
 * 消息
 */
public class MessageFragment extends BaseFragment {

    private String EMurl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message_fg_layout, container, false);
        ButterKnife.bind(this, view);
        if (EnjoyorApplication.getInstance().getDBHelper().getUser() != null) {
            EMurl = EnjoyorApplication.getInstance().getDBHelper().getUser().getHeadImg();
        }
        if (StringUtils.isEmpty(EMurl)) {
            if (EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo() != null) {
                EMurl = EnjoyorApplication.getInstance().getDBHelper().getDoctorInfo().getHeadImg();
            }
        }
        initView();

        return view;
    }

    private void initView() {

        EaseConversationListFragment conversationListFragment = new EaseConversationListFragment();
        conversationListFragment.setConversationListItemClickListener(new EaseConversationListFragment.EaseConversationListItemClickListener() {

            @Override
            public void onListItemClicked(EMConversation conversation) {
                Intent intent = new Intent(getActivity(), EMmessageActivity.class);
                intent.putExtra(EaseConstant.EXTRA_USER_ID, conversation.getUserName());
                if (!TextUtils.isEmpty(EMurl)) {
                    Log.i("EMurl", "MessageFragment----->" + EMurl);
                    intent.putExtra("EMurl", Constant.TEST_URL + EMurl);
                }
                if (BaseDate.getEMList() != null) {
                    for (SortModel s : BaseDate.getEMList()) {
                        if (s.getMyPatient().getPhoneNum().equals(conversation.getUserName())) {
                            intent.putExtra("EMNickName", nonEmptyStr(s.getName(), conversation.getUserName()));
                        }
                    }
                }
                startActivity(intent);

//                startActivity(new Intent(getActivity(), EMmessageActivity.class).putExtra(EaseConstant.EXTRA_USER_ID, conversation.getUserName()));
            }
        });
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.ll_fragment, conversationListFragment).commit();
    }

    private String nonEmptyStr(String s1, String s2) {
        return !StringUtils.isEmpty(s1) ? s1 : s2;
    }
}
