package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.MatcherUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/10.
 * 忘记密码页面
 */
public class ForgetPwdActivity extends BaseActivity implements View.OnClickListener {
    //
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.tv_code)
    TextView tv_code;
    @Bind(R.id.bt_commit)
    Button bt_commit;
    @Bind(R.id.et_phonenumber)
    EditText et_phonenumber;
    @Bind(R.id.et_password)
    EditText et_password;
    private int count = 60;//30秒倒计时
    private Handler handler;
    private String tellphone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgetpwd_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
        initCode();
    }

    private void initEvent() {
        navigation_name.setText("忘记密码");
        re_back.setOnClickListener(this);
        tv_code.setOnClickListener(this);
        bt_commit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.tv_code:
//                ToastUtils.showToast(ForgetPwdActivity.this, "您点击了获取验证码按钮");
                forgetpwd();
                break;
            case R.id.bt_commit:
//                ToastUtils.showToast(ForgetPwdActivity.this, "您点击了确定按钮");
                determine();
                break;
        }
    }

    private void determine() {
        Subscription s;
        s = Server.otherProtocolBuild(this).yanzhengma("mob", et_phonenumber.getText().toString(), et_password.getText().toString(), "yanzheng")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            doWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    private void doWithResponse(BaseResponse data) {
        if (data.getResult() == 1) {
            showSnackBar("验证验证码成功");
            Intent intent = new Intent(this, NewPwdActivity.class);
            intent.putExtra("phone", et_phonenumber.getText().toString());
            if (!StringUtils.isEmpty(et_password.getText().toString().trim())){
                intent.putExtra("mcode",et_password.getText().toString().trim());
                startActivity(intent);
                finish();
            }
        } else {
            showSnackBar("验证码错误");
        }
    }

    private void forgetpwd() {
        if (StringUtils.isBlank(et_phonenumber.getText().toString())) {
            showSnackBar("手机号码不能为空");
            et_phonenumber.requestFocus();
        } else if (!MatcherUtil.isMobileNumber(et_phonenumber.getText().toString())) {
            showSnackBar("请输入正确的手机号");
            et_phonenumber.requestFocus();
        } else {
            sendMsg();
            sendMsgtoPhone();
        }
    }

    private void sendMsgtoPhone() {
        Subscription s;
        s = Server.otherProtocolBuild(this).getMessage("mob", et_phonenumber.getText().toString(),"str")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if ((boolean) o) {
            ToastUtils.showToast(this, "发送成功");
        } else {
            ToastUtils.showToast(this, "信息发送失败");
        }
    }

    private void sendMsg() {
        tv_code.setEnabled(false);
        tv_code.setText("获取验证码");
        //倒记时
        final Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() { // UI thread
                    @Override
                    public void run() {
                        count--;
                        handler.sendEmptyMessage(count);
                        if (count == 0) {
                            timer.cancel();
                            reSet();
                        }
                    }
                });
            }
        };
        timer.schedule(task, 1000, 1000);
    }

    private void reSet() {
        if (tellphone != null && tellphone.equals(et_phonenumber.getText().toString().trim())) {
            tv_code.setText("重新获取");
        } else {
            tv_code.setText("获取验证码");
        }
        tv_code.setEnabled(true);
        count = 60;
    }

    private void initCode() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);
                if (msg.what > 0) {
                    tv_code.setText("剩余" + msg.what + "秒");
                }
            }
        };
    }
}
