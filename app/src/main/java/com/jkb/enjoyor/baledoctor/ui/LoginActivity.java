package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.BaseDate;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.MatcherUtil;
import com.jkb.enjoyor.baledoctor.utils.SharePreferenceUtil;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.mob.tools.utils.UIHandler;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQClientNotExistException;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.utils.WechatClientNotExistException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/8.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, PlatformActionListener, Handler.Callback {
    //登录按钮
    @Bind(R.id.bt_login)
    Button bt_login;
    //    忘记密码按钮
    @Bind(R.id.tv_forget_password)
    TextView tv_forget_password;
    //    快速注册按钮
    @Bind(R.id.tv_login_quick)
    TextView tv_login_quick;
    //微信登录
    @Bind(R.id.login_wx)
    LinearLayout login_wx;
    //    qq登录
    @Bind(R.id.login_qq)
    LinearLayout login_qq;
    //    新浪微博登录
    @Bind(R.id.login_ipay)
    LinearLayout login_ipay;
    @Bind(R.id.et_phonenumber)
    EditText et_phonenumber;
    @Bind(R.id.et_password)
    EditText et_password;


    String phonenumber;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_ac_layout);
        ButterKnife.bind(this);
        initEvent();
    }

    private void initEvent() {
        bt_login.setOnClickListener(this);
        tv_forget_password.setOnClickListener(this);
        tv_login_quick.setOnClickListener(this);
        login_wx.setOnClickListener(this);
        login_qq.setOnClickListener(this);
        login_ipay.setOnClickListener(this);
    }

    //点击事件
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_login:
                if (isRight()) {
                    progress("");
                    login();
                }
                break;
            case R.id.tv_forget_password:
                Intent intent_forget = new Intent(LoginActivity.this, ForgetPwdActivity.class);
                startActivity(intent_forget);
                break;
            case R.id.tv_login_quick:
                Intent intent_regist = new Intent(LoginActivity.this, RegistActivity.class);
                startActivity(intent_regist);
                break;
            case R.id.login_wx:
//                ToastUtils.showToast(LoginActivity.this, "您点击了微信第三方登录");
                Platform wechat = ShareSDK.getPlatform(this, Wechat.NAME);
                wechat.showUser(null);
                wechat.SSOSetting(false);
                wechat.setPlatformActionListener(this);
//                wechat.authorize();
                if (!wechat.isValid()) {
                    wechat.authorize();
                }
                break;
            case R.id.login_qq:
//                ToastUtils.showToast(LoginActivity.this, "您点击了QQ第三方登录");
                break;
            case R.id.login_ipay:
//                ToastUtils.showToast(LoginActivity.this, "您点击了新浪微博第三方登录");
                break;
        }
    }

    private void login() {
        Subscription s;
        s = Server.otherProtocolBuild(this).login(phonenumber, password, "str")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof UserInfo) {
            UserInfo userinfo = (UserInfo) o;
            userinfo.setUserPhoneNumber(phonenumber);
            userinfo.setUserPassword(password);
            Log.i("userinfo", userinfo.toString());
            new SharePreferenceUtil(this).setUserId(userinfo.getId());
            BaseDate.setSessionId(LoginActivity.this, userinfo.getId());
            if (EnjoyorApplication.getInstance().getDBHelper().saveUser(userinfo)) {
//                AppManagerUtil.getAppManager().finishAllActivity();
                Intent intent = new Intent(this, MainActivity.class);
                progressCancel();
                startActivity(intent);
                finish();
            }
            if (!EMClient.getInstance().isLoggedInBefore()) {
                emLogin();
            }
        } else {
            progressCancel();
            showSnackBar("用户名或密码错误，请重新登录");
//            ToastUtils.showToast(this, "用户名或密码错误，请重新登录");
        }
    }


    private boolean isRight() {
        phonenumber = et_phonenumber.getText().toString().trim();
        password = et_password.getText().toString().trim();
        if (StringUtils.isBlank(phonenumber)) {
            showSnackBar("手机号不能为空");
            et_phonenumber.requestFocus();
            return false;
        } else if (!MatcherUtil.isMobileNumber(phonenumber)) {
            showSnackBar("手机号不正确");
            et_phonenumber.requestFocus();
            return false;
        } else if (StringUtils.isBlank(password)) {
            showSnackBar("密码不能为空");
            et_password.requestFocus();
            return false;
        } else if (!MatcherUtil.isPWD(password)) {
            showSnackBar("请输入6--12位的密码");
            et_password.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
        Message msg = new Message();
        msg.what = 2;
        msg.arg1 = 1;
        msg.arg2 = i;
        msg.obj = platform;
        Log.d("platform", platform.getDb().toString());
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {
        Log.d("wyy------throwable----", throwable.toString());
        Message msg = new Message();
        msg.what = 1;
        msg.arg1 = i;
        msg.obj = throwable;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public void onCancel(Platform platform, int i) {
        Log.d("wyy------i----", i + "");
        Message msg = new Message();
        msg.what = 0;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public boolean handleMessage(Message msg) {
        int what = msg.what;
        switch (what) {
            case 0:
                Toast.makeText(LoginActivity.this, "登录已取消", Toast.LENGTH_LONG).show();
                break;
            case 1:
                String failtext;
                if (msg.obj instanceof WechatClientNotExistException
                        || msg.obj instanceof QQClientNotExistException
                        || msg.obj instanceof WechatClientNotExistException) {
                    failtext = "版本过低或未安装客户端";
                } else if (msg.obj instanceof java.lang.Throwable
                        && msg.obj.toString() != null
                        && msg.obj.toString().contains(
                        "prevent duplicate publication")) {
                    failtext = "请稍后发送";
                } else if (msg.obj.toString().contains("error")) {
                    failtext = "登录失败";
                } else {
                    failtext = "登录失败";
                }
                break;
            case 2:
                Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_LONG).show();
        }
        return false;
    }


    public void emLogin() {
        EMClient.getInstance().login(phonenumber, Constant.EMPWD, new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
            }

            @Override
            public void onProgress(int progress, String status) {
            }

            @Override
            public void onError(int code, String message) {
                Log.i("code", code + "     " + message);
                if (204 == code) {
                    registerEM();
                }
                Log.i("EM", "EM---MineDoctorActivity------>登录聊天服务器失败");
            }
        });
    }


    private void registerEM() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    EMClient.getInstance().createAccount(phonenumber, Constant.EMPWD);//同步方法 测试
                    runOnUiThread(new Runnable() {
                        public void run() {
                            emLogin();
                        }
                    });
                } catch (final HyphenateException e) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            int errorCode = e.getErrorCode();
                        }
                    });
                }
            }
        }).start();
    }
}
