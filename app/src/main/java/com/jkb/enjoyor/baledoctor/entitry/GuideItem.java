package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by YuanYuan on 2016/10/18.
 * 指导项目实体类
 */
public class GuideItem {

    /**
     * createtime : 2016-10-13 10:10:45
     * id : 1
     * itemType : 1
     * key : 限制总能量
     * sort : 1
     * type : hypertension
     * val : 肥胖者应该节食减肥，使体重减轻每周1-1.5kg为宜。
     */

    private String createtime;
    private Integer id;
    private Integer itemType;
    private String key;
    private Integer sort;
    private String type;
    private String val;

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemType() {
        return itemType;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
