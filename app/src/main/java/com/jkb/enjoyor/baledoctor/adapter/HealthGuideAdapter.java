package com.jkb.enjoyor.baledoctor.adapter;

import android.content.Context;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.GuideListInfo;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;

import java.util.List;

/**
 * Created by YuanYuan on 2016/10/17.
 */
public class HealthGuideAdapter extends CommAdapter<GuideListInfo> {

    private Context context;
    private List<GuideListInfo> datas;

    public HealthGuideAdapter(Context context, List<GuideListInfo> datas, int layoutId) {
        super(context, datas, layoutId);
        this.context = context;
        this.datas = datas;
    }

    @Override
    public void convert(ViewHolder holder, GuideListInfo guideListInfo) {
//        ImageView imageView = (ImageView) holder.getmConvertView().findViewById(R.id.healthguide_fg_lv_item_logo);
        if (!StringUtils.isEmpty(guideListInfo.getHeadImg())) {
//            ImageLoader.getInstance().displayImage(Constant.TEST_URL+guideListInfo.getHeadImg(),imageView, EnjoyorApplication.option);
            holder.setImageURL(R.id.healthguide_fg_lv_item_logo, Constant.TEST_URL+guideListInfo.getHeadImg(),EnjoyorApplication.option);
        }
        if (!StringUtils.isEmpty(guideListInfo.getUsername())) {
            holder.setText(R.id.healthguide_fg_lv_item_name, guideListInfo.getUsername());
        } else {
            holder.setText(R.id.healthguide_fg_lv_item_name, "未知用户");
        }
        if (guideListInfo.getAge() != null) {
            holder.setText(R.id.healthguide_fg_lv_item_age, guideListInfo.getAge() + "");
        }else {
            holder.setText(R.id.healthguide_fg_lv_item_age,"18岁");
        }
        if (!StringUtils.isEmpty(guideListInfo.getSex())) {
            holder.setText(R.id.healthguide_fg_lv_item_sex, guideListInfo.getSex());
        }else {
            holder.setText(R.id.healthguide_fg_lv_item_sex, "男");
        }
        if (!StringUtils.isEmpty(guideListInfo.getCreateTime())) {
            holder.setText(R.id.healthguide_fg_lv_item_time, guideListInfo.getCreateTime());
        }
        if (!StringUtils.isEmpty(guideListInfo.getType())) {
            if ("hypertension".equals(guideListInfo.getType())) {
                holder.setImageResource(R.id.healthguide_fg_lv_item_mark, R.mipmap.gaoxueya);
            } else {
                holder.setImageResource(R.id.healthguide_fg_lv_item_mark, R.mipmap.tangniaobing);
            }
        }
    }
}
