package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.PatientRecord;
import com.jkb.enjoyor.baledoctor.entitry.RecordInfo;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/12.
 * 健康报告页面
 */
public class RecordActivity extends ToolBarActivity {

    @Bind(R.id.record_ac_lv)
    ListView record_ac_lv;
    @Bind(R.id.record_tv_nilist)
    TextView record_tv_nilist;
    private List<RecordInfo> record_list = new ArrayList<>();
    private int accountId;
    private long doctorId;

    ArrayList<PatientRecord> patientrecord_list = new ArrayList<>();
    private String type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_ac_layout, false);
        ButterKnife.bind(this);

        initHead();


        if (getIntent().hasExtra("accountId")) {
            accountId = getIntent().getIntExtra("accountId", -1);
        }
//        if (getIntent().hasExtra("type")) {
//            type = getIntent().getStringExtra("type");
//            Log.i("type", "type" + type);
//        }
        if (EnjoyorApplication.getInstance().getDBHelper().getUser() != null) {
            doctorId = EnjoyorApplication.getInstance().getDBHelper().getUser().getId();
        }
        if (accountId > 0 && doctorId > 0) {
            initData();
        }
    }

    private void initHead() {
        setTitle("健康档案");
    }

    private void initListView() {
        record_ac_lv.setAdapter(new RecordAdapter());
        record_ac_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ToastUtils.showToast(RecordActivity.this, "您点击了第" + record_list.get(position).getType() + "条数据");
                int keytype = record_list.get(position).getType();
                switch (keytype) {
                    case 1:
//                        ToastUtils.showToast(RecordActivity.this, "您点击了体检机体检");
                        Intent intent_myrecord = new Intent(RecordActivity.this, MyRecordActivity.class);
                        intent_myrecord.putExtra("recordId", record_list.get(position).getRecordId() + "");
//                        Log.i("listid", record_list.get(position).getRecordId() + "");
                        startActivity(intent_myrecord);
                        break;
                    case 2:
//                        ToastUtils.showToast(RecordActivity.this, "您点击了自测体检");
                        Intent intent_myself = new Intent(RecordActivity.this, MySelfCheckActivity.class);
                        intent_myself.putExtra("recordId", record_list.get(position).getRecordId());
                        if (!StringUtils.isEmpty(record_list.get(position).getRecordTime())){
                            intent_myself.putExtra("recordTime",record_list.get(position).getRecordTime());
                        }
//                        Log.i("listid", record_list.get(position).getRecordId() + "");
                        startActivity(intent_myself);
                        break;
                    case 3:
//                        ToastUtils.showToast(RecordActivity.this, "您点击了随手记体检");
                        Intent intent_mynote = new Intent(RecordActivity.this, MyNotesActivity.class);
                        intent_mynote.putExtra("recordId", record_list.get(position).getRecordId());
//                        Log.i("listid", record_list.get(position).getRecordId() + "");
                        startActivity(intent_mynote);
                        break;
                    default:
//                        ToastUtils.showToast(RecordActivity.this, "您点击了其他体检");
                        break;
                }
            }
        });
    }

    private void initData() {

//        Log.i("initData", "userId------------" + accountId + "\n" + "doctorId------------" + doctorId);
        Subscription s;
        s = Server.HomeProtocolBuild(this).getReord(accountId, 100, doctorId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof ArrayList) {
            List<RecordInfo> _list = (List<RecordInfo>) o;
            record_list.clear();
            record_list.addAll(_list);
            if (record_list.size() > 0) {
                record_ac_lv.setVisibility(View.VISIBLE);
                record_tv_nilist.setVisibility(View.GONE);
                initListView();
            } else {
                record_ac_lv.setVisibility(View.GONE);
                record_tv_nilist.setVisibility(View.VISIBLE);
            }
        }else {
            record_ac_lv.setVisibility(View.GONE);
            record_tv_nilist.setVisibility(View.VISIBLE);
        }
    }

    class RecordAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return record_list.size();
        }

        @Override
        public Object getItem(int position) {
            return record_list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(RecordActivity.this).inflate(R.layout.record_ac_lv_item, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.record_item_name.setText(record_list.get(position).getUserName());
            viewHolder.tv_time.setText(record_list.get(position).getRecordTime());
            ImageLoader.getInstance().displayImage(Constant.TEST_URL+record_list.get(position).getHeadImg(),viewHolder.record_item_picture,EnjoyorApplication.option);
            switch (record_list.get(position).getType()) {
                case Constant.TYPE_MACHINE:
                    viewHolder.tv_type.setText("机器体检机录入");
                    break;
                case Constant.TYPE_MANUAL:
                    viewHolder.tv_type.setText("手动录入");
                    break;
                case Constant.TYPE_NOTE:
                    viewHolder.tv_type.setText("随手记");
                    break;
            }
            return convertView;
        }

        public class ViewHolder {
            @Bind(R.id.record_item_name)
            TextView record_item_name;
            @Bind(R.id.tv_time)
            TextView tv_time;
            @Bind(R.id.tv_type)
            TextView tv_type;
            @Bind(R.id.record_item_picture)
            ImageView record_item_picture;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }
}
