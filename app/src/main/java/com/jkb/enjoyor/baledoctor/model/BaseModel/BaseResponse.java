package com.jkb.enjoyor.baledoctor.model.BaseModel;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class BaseResponse<T> {
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_ERROR = 0;
    Error error;
    T data;
    int result;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
//    public static final int STATUS_NULL = -1;//返回空值或返回的值不是BaseResponse格式;
//    public static final int STATUS_SUCCESS = 1001;
//    public static final int STATUS_ERROR = 0;
//    private String Msg;
//    private int Errcode;
//    private T Data;
//
//    public String getMsg() {
//        return Msg;
//    }
//
//    public void setMsg(String msg) {
//        Msg = msg;
//    }
//
//    public int getErrcode() {
//        return Errcode;
//    }
//
//    public void setErrcode(int errcode) {
//        Errcode = errcode;
//    }
//
//    public T getData() {
//        return Data;
//    }
//
//    public void setData(T data) {
//        Data = data;
//    }
}
