package com.jkb.enjoyor.baledoctor.entitry;

/**
 * Created by Administrator on 2016/9/20.
 */
public class RecordInfo {
    /**
     * accountId : 9
     * addressName : 银江软件园检测G点
     * age : 24
     * compLogo : files/res/app/start/124c6a53-134b-4239-af41-57a5a1a62f4c.JPG
     * compName : JKB
     * doctorState : 0
     * headImg :
     * id : 4
     * idCardpic :
     * nickName :
     * recordId : 26
     * recordNo : 20160316160019
     * recordTime : 2016-05-17 16:04:46
     * reportTime : 2016-09-21 09:43:05
     * sex : 男
     * type : 1
     * userId : 78
     * userName : 神
     */

    private int accountId;
    private String addressName;
    private int age;
    private String compLogo;
    private String compName;
    private int doctorState;
    private String headImg;
    private int id;
    private String idCardpic;
    private String nickName;
    private int recordId;
    private String recordNo;
    private String recordTime;
    private String reportTime;
    private String sex;
    private int type;
    private int userId;
    private String userName;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCompLogo() {
        return compLogo;
    }

    public void setCompLogo(String compLogo) {
        this.compLogo = compLogo;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public int getDoctorState() {
        return doctorState;
    }

    public void setDoctorState(int doctorState) {
        this.doctorState = doctorState;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdCardpic() {
        return idCardpic;
    }

    public void setIdCardpic(String idCardpic) {
        this.idCardpic = idCardpic;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(String recordNo) {
        this.recordNo = recordNo;
    }

    public String getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(String recordTime) {
        this.recordTime = recordTime;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

//    private long id;
//    private int recordId;
//    private int type;//体检类型：1.机器体检机录入2.手动录入3.随手记
//    private int doctorState;
//    private String recordNo;
//    private String recordTime;
//    private String addressName;
//    private String compLogo;
//    private String compName;
//    private String idCardpic;
//    private String userName;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public int getRecordId() {
//        return recordId;
//    }
//
//    public void setRecordId(int recordId) {
//        this.recordId = recordId;
//    }
//
//    public int getType() {
//        return type;
//    }
//
//    public void setType(int type) {
//        this.type = type;
//    }
//
//    public int getDoctorState() {
//        return doctorState;
//    }
//
//    public void setDoctorState(int doctorState) {
//        this.doctorState = doctorState;
//    }
//
//    public String getRecordNo() {
//        return recordNo;
//    }
//
//    public void setRecordNo(String recordNo) {
//        this.recordNo = recordNo;
//    }
//
//    public String getRecordTime() {
//        return recordTime;
//    }
//
//    public void setRecordTime(String recordTime) {
//        this.recordTime = recordTime;
//    }
//
//    public String getAddressName() {
//        return addressName;
//    }
//
//    public void setAddressName(String addressName) {
//        this.addressName = addressName;
//    }
//
//    public String getCompLogo() {
//        return compLogo;
//    }
//
//    public void setCompLogo(String compLogo) {
//        this.compLogo = compLogo;
//    }
//
//    public String getCompName() {
//        return compName;
//    }
//
//    public void setCompName(String compName) {
//        this.compName = compName;
//    }
//
//    public String getIdCardpic() {
//        return idCardpic;
//    }
//
//    public void setIdCardpic(String idCardpic) {
//        this.idCardpic = idCardpic;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }

}
