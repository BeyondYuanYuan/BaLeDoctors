package com.jkb.enjoyor.baledoctor.model;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class MineModel {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
