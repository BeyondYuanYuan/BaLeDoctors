package com.jkb.enjoyor.baledoctor.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.common.BaseDate;
import com.jkb.enjoyor.baledoctor.custom.LoadingView;
import com.jkb.enjoyor.baledoctor.model.BaseModel.BaseResponse;
import com.jkb.enjoyor.baledoctor.utils.AppManagerUtil;
import com.jkb.enjoyor.baledoctor.utils.ScreenUtils;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class BaseActivity extends AppCompatActivity {
    private AlertDialog mAlertDialog;
    private Dialog mDialog;

    private LoadingView loadingView;
     /*----------------------------------网络数据请求----------------------------------*/

    private CompositeSubscription mCompositeSubscription;

    public CompositeSubscription getCompositeSubscription() {
        if (this.mCompositeSubscription == null) {
            this.mCompositeSubscription = new CompositeSubscription();
        }

        return this.mCompositeSubscription;
    }

    public void addSubscription(Subscription s) {
        getCompositeSubscription().add(s);
    }

    public void dealWithResponse(BaseResponse response) {
        if (response != null) {
            if (response.getResult() == BaseResponse.STATUS_SUCCESS) {
                onResult(response.getData());
            } else {
                onResult(response.getError());
            }
        }

//        if (response != null) {
//            if (response.getErrcode() == BaseResponse.STATUS_ERROR) {
//                onError(response.getErrcode());
//            } else {
//                onResult(response.getData());
//            }
//        } else {
//            onError(BaseResponse.STATUS_NULL);
//        }
    }

    public void onError(Throwable e) {
        try {
            e.printStackTrace();
            //分析统计
//            MobclickAgent.reportError(this,e);
        } catch (Exception ee) {
            ee.printStackTrace();
//            MobclickAgent.reportError(this,ee);
        }

    }

    public void onError(int errorCode) {

    }

    public void onResult(Object o) {

    }

    /*----------------------------------添加toolbar----------------------------------*/

    private RelativeLayout contentview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManagerUtil.addActivity(BaseActivity.this);
    }

    public void setContentView(int view, boolean needLoad) {
        View contentview = LayoutInflater.from(this).inflate(view, null);
        initView(contentview, this, needLoad);
    }

    public void initView(View rootView, Context context, boolean needLoad) {
        initView(rootView, context, needLoad, false);
    }

    public void initView(View rootView, Context context, boolean needLoad, boolean needToolBar) {
        contentview = new RelativeLayout(context);

        RelativeLayout.LayoutParams rootLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
//        rootRL.setOrientation(LinearLayout.VERTICAL);
        contentview.setLayoutParams(rootLP);

        contentview.addView(rootView);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        if (needToolBar) {
            View toolbarView = LayoutInflater.from(context).inflate(R.layout.activity_base, contentview, false);
            contentview.addView(toolbarView, LinearLayout.LayoutParams.FILL_PARENT, getBarHeight());
            lp.setMargins(0, getBarHeight(), 0, 0);
        }
        rootView.setLayoutParams(lp);
        if (needLoad) {
            onRetry();
        }
        super.setContentView(contentview);
    }

    public void onRetry() {

    }

    private int getBarHeight() {
        if (BaseDate.getToolbarHeight() > 0)
            return BaseDate.getToolbarHeight();
        BaseDate.setToolbarHeight(ScreenUtils.returnBarHeight(this));
        return BaseDate.getToolbarHeight();
    }


    /*----------------------------------消息提示（可代替土司）----------------------------------*/
    public void showSnackBar(String text, int length, String btnText, View.OnClickListener listener) {
        View view = contentview;
        if (view == null)
            view = getWindow().getDecorView();
        Snackbar.make(view, text, length)
                .setAction(btnText, listener)
                .setActionTextColor(getResources().getColor(R.color.hc_color_white)).show();
    }

    public void showSnackBar(String text) {
        showSnackBar(text, Snackbar.LENGTH_LONG, "", null);
    }

    public void showSnackBar(String text, String btnText, View.OnClickListener listener) {
        showSnackBar(text, Snackbar.LENGTH_INDEFINITE, btnText, listener);
    }


        /*----------------------------------添加自定义对话框----------------------------------*/

    public void dialog(Context context, String tittle, String content, View.OnClickListener left, View.OnClickListener right) {
        mAlertDialog = new AlertDialog.Builder(context).create();
        mAlertDialog.show();
        mAlertDialog.getWindow().setContentView(R.layout.dialog_custom);

        ((TextView) mAlertDialog.findViewById(R.id.tv_dialog_tittle)).setText(tittle);//标题
        ((TextView) mAlertDialog.findViewById(R.id.tv_dialog_content)).setText(content);//文本内容
        ((TextView) mAlertDialog.findViewById(R.id.tv_dialog_left)).setOnClickListener(left);//左边按钮监听
        ((TextView) mAlertDialog.findViewById(R.id.tv_dialog_right)).setOnClickListener(right);//右边按钮监听
    }

    public void dialog(Context context, String tittle, String content, String leftStr, String rightStr, View.OnClickListener left, View.OnClickListener right) {
        dialog(context, tittle, content, left, right);
        ((TextView) mAlertDialog.findViewById(R.id.tv_dialog_left)).setText(leftStr);//左边文字描述
        ((TextView) mAlertDialog.findViewById(R.id.tv_dialog_right)).setText(rightStr);//右边文字描述
    }

    public void dialogCancel() {
        if (mAlertDialog != null) {
            mAlertDialog.cancel();
        }
    }

        /*----------------------------------圆形进度条----------------------------------*/

    public void progress(String msg) {
        loadingView = new LoadingView(msg);
        loadingView.show(getSupportFragmentManager(), "");
    }

    public void progressCancel() {
        if (loadingView != null)
            loadingView.dismiss();
    }

    public boolean isLogin(Context context) {
        if (BaseDate.getSessionId(context) == null) {
            dialog(context, "", "亲,您还未登录，是否立即登录", "取消", "确定", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogCancel();
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });
            return false;
        }
        return true;
    }
    /**
     * 隐藏软键盘的方法
     */
    public void hideKeyBoard() {
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//
//        View currentFocus = getCurrentFocus();
//        if (null != currentFocus && imm.isActive()) {
//            imm.hideSoftInputFromWindow(currentFocus.getWindowToken(),
//                    InputMethodManager.HIDE_NOT_ALWAYS);
//        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(),
                    0);
        }
    }
}
