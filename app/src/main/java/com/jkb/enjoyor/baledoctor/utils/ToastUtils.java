package com.jkb.enjoyor.baledoctor.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by YuanYuan on 2016/9/5.
 */
public class ToastUtils {
    private static Toast toast;
    public static void showToast(Context context, String content) {
        if (toast == null) {
            toast = Toast.makeText(context,
                    content,
                    Toast.LENGTH_SHORT);
        } else {
            toast.setText(content);
        }
        toast.show();
    }
}
