package com.jkb.enjoyor.baledoctor.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceUtil {

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;


    public SharePreferenceUtil(Context context) {
        sp = context.getSharedPreferences("shuoming", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    //    用户唯一id
    public long getUserId() {
        return sp.getLong("userid", -1l);
    }

    public void setUserId(long userid) {
        editor.putLong("userid", userid);
        editor.commit();
    }
}
