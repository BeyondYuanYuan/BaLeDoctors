package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.BaseDate;
import com.jkb.enjoyor.baledoctor.entitry.UserInfo;
import com.jkb.enjoyor.baledoctor.fragments.GuideFragment;
import com.jkb.enjoyor.baledoctor.fragments.HomeFragment;
import com.jkb.enjoyor.baledoctor.fragments.MessageFragment;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.SharePreferenceUtil;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    /**
     * 声明一个在按下Back键时的时间
     */
    private long preTime;

    /**
     * 声明页面控件
     */
    @Bind(R.id.tv_infocount)
    TextView tv_infocount;
    @Bind(R.id.fragment_container)
    LinearLayout fragment_container;
    @Bind(R.id.home)
    LinearLayout home;
    @Bind(R.id.guide)
    LinearLayout guide;
    @Bind(R.id.message)
    LinearLayout message;
    @Bind(R.id.guide_img)
    ImageView guide_img;
    @Bind(R.id.home_img)
    ImageView home_img;
    @Bind(R.id.message_img)
    ImageView message_img;
    @Bind(R.id.home_tv)
    TextView home_tv;
    @Bind(R.id.guide_tv)
    TextView guide_tv;
    @Bind(R.id.message_tv)
    TextView message_tv;

    /**
     * 存储选中状态的三个图标
     */
    private int[] img_selected = {R.mipmap.home, R.mipmap.guide, R.mipmap.message};

    /**
     * 存储未被选中的三个图标
     */
    private int[] img_unselected = {R.mipmap.home_, R.mipmap.guide_, R.mipmap.message_};

    /**
     * 申明一个集合存放图标控件
     */
    private List<ImageView> img_icons = new ArrayList<ImageView>();

    /**
     * 申明一个集合存放对应图标文字
     */
    private List<TextView> tv_icons = new ArrayList<TextView>();

    /**
     * 声明Fragment管理器
     */
    private FragmentManager mFragmentManager;

    private String userPhoneNumber;
    private String userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initIcon();
        UserInfo mUserInfo = EnjoyorApplication.getInstance().getDBHelper().getUser();
        if (mUserInfo != null) {
//            Log.i("mUserInfo", mUserInfo.toString());

            userPhoneNumber = mUserInfo.getUserPhoneNumber();
            userPassword = mUserInfo.getUserPassword();
            login();
            initview();
        } else {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void login() {
        Subscription s;
//        Log.i("login", userPhoneNumber + "    " + userPassword);
        s = Server.otherProtocolBuild(this).login(userPhoneNumber, userPassword, "str")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof UserInfo) {
            UserInfo userinfo = (UserInfo) o;
            new SharePreferenceUtil(this).setUserId(userinfo.getId());
            userinfo.setUserPhoneNumber(userPhoneNumber);
            userinfo.setUserPassword(userPassword);
            EnjoyorApplication.getInstance().getDBHelper().saveUser(userinfo);
            BaseDate.setSessionId(MainActivity.this, userinfo.getId());

        }
    }

    /**
     * 初始化方法
     */
    private void initview() {
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, new HomeFragment());
        transaction.commit();
        home.setOnClickListener(this);
        guide.setOnClickListener(this);
        message.setOnClickListener(this);
    }


    @Override
    protected void onRestart() {
//        initview();
        super.onRestart();
    }

    /**
     * 初始化底部导航图标及文字
     */
    private void initIcon() {
        img_icons.add(home_img);
        img_icons.add(guide_img);
        img_icons.add(message_img);
        tv_icons.add(home_tv);
        tv_icons.add(guide_tv);
        tv_icons.add(message_tv);
        for (int x = 0; x < img_icons.size(); x++) {
            img_icons.get(x).setTag(x);
        }
    }

    /**
     * 改变 底部图标的显示状态的方法
     *
     * @param v
     */
    private void changestate(View v) {
        int tag = (Integer) v.getTag();
        img_icons.get(tag).setImageResource(img_selected[tag]);
        tv_icons.get(tag).setTextColor(getResources().getColor(R.color.theme_color));
        for (int x = 0; x < img_icons.size(); x++) {
            if (x != tag) {
                img_icons.get(x).setImageResource(img_unselected[x]);
                tv_icons.get(x).setTextColor(getResources().getColor(R.color.tv_color));
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof LinearLayout) {
            ImageView imageview = (ImageView) ((LinearLayout) v).getChildAt(0);
            changestate(imageview);
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            switch (v.getId()) {
                case R.id.home:
                    updateUnreadLabel();
                    transaction.replace(R.id.fragment_container, new HomeFragment());
                    break;
                case R.id.guide:
                    updateUnreadLabel();
                    transaction.replace(R.id.fragment_container, new GuideFragment());
                    break;
                case R.id.message:
                    updateUnreadLabel();
                    transaction.replace(R.id.fragment_container, new MessageFragment());
                    break;
            }
            transaction.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUnreadLabel();
    }

    public void updateUnreadLabel() {
        int count = getUnreadMsgCountTotal();
        if (count > 0) {
            tv_infocount.setText(String.valueOf(count));
            tv_infocount.setVisibility(View.VISIBLE);
        } else {
            tv_infocount.setVisibility(View.INVISIBLE);
        }
    }


    /**
     * 获取未读消息数
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        int unreadMsgCountTotal = 0;
        int chatroomUnreadMsgCount = 0;
        unreadMsgCountTotal = EMClient.getInstance().chatManager().getUnreadMsgsCount();
        for (EMConversation conversation : EMClient.getInstance().chatManager().getAllConversations().values()) {
            if (conversation.getType() == EMConversation.EMConversationType.ChatRoom)
                chatroomUnreadMsgCount = chatroomUnreadMsgCount + conversation.getUnreadMsgCount();
        }
        return unreadMsgCountTotal - chatroomUnreadMsgCount;
    }


    //----------------------再按一次返回键退出程序---------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (System.currentTimeMillis() - preTime > 2000) {
                Toast.makeText(this,"再按一次返回键退出程序",Toast.LENGTH_LONG).show();
//                ToastUtils.showToast(this, "再按一次返回键退出程序");
            } else {
                this.finish();
                System.exit(0);
            }
        }
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            preTime = System.currentTimeMillis();
        }
        return true;
    }
}
