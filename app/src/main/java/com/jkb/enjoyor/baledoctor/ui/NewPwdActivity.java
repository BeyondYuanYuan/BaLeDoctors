package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/9/19.
 * 新密码页面
 */
public class NewPwdActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.et_newpassword)
    EditText et_newpassword;
    @Bind(R.id.et_again_newpassword)
    EditText et_again_newpassword;
    @Bind(R.id.bt_commit)
    Button bt_commit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpwd_ac_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initEvent() {
        navigation_name.setText("新密码");
        re_back.setOnClickListener(this);
        bt_commit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
//                showSnackBar("点击了返回按钮");
                finish();
                break;
            case R.id.bt_commit:
//                showSnackBar("点击了提交按钮");
                commit();
                break;
        }
    }

    private void commit() {
        if (StringUtils.isBlank(et_newpassword.getText().toString().trim())) {
            showSnackBar("请输入新密码");
        } else if (StringUtils.isBlank(et_again_newpassword.getText().toString().trim())) {
            showSnackBar("请再次输入新密码");
        } else if (!et_newpassword.getText().toString().trim().equals(et_again_newpassword.getText().toString().trim())) {
            showSnackBar("两次密码输入不一致，请重新输入");
        } else if(et_newpassword.getText().toString().trim().length()>=6&&et_newpassword.getText().toString().trim().length()<=12){
            Intent intent = getIntent();
            String phone = intent.getStringExtra("phone");
            String mcode = intent.getStringExtra("mcode");
            Subscription s;
            s = Server.otherProtocolBuild(this).forgetPwd(phone, et_newpassword.getText().toString(),mcode, "str")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            data -> {
                                dealWithResponse(data);
                            },
                            e -> {
                                onError(e);
                            }
                    );
            addSubscription(s);
        }else {
            showSnackBar("请输入6--12位密码！");
        }
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if ((boolean) o){
            showSnackBar("密码找回成功，请直接登录");
            finish();
        }else {
            showSnackBar("密码找回失败");
        }
    }
}
