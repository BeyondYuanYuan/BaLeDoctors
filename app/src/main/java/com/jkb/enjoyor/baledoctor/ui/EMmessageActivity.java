package com.jkb.enjoyor.baledoctor.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.ui.EaseChatFragment;
import com.jkb.enjoyor.baledoctor.R;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/9/14.
 */
public class EMmessageActivity extends AppCompatActivity {

    @Bind(R.id.ll_fragment)
    LinearLayout ll_fragment;
    private String EMName;
    private String EMurl;
    private String EMNickName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emmessage);
        if (getIntent().hasExtra("EMurl")) {
            EMurl = getIntent().getStringExtra("EMurl");
        }
        if (getIntent().hasExtra("EMNickName")) {
            EMNickName = getIntent().getStringExtra("EMNickName");
    }
        if (getIntent().hasExtra("EMName")) {
            EMName = getIntent().getStringExtra("EMName");
            EaseChatFragment chatFragment = new EaseChatFragment();
            Bundle args = new Bundle();
            args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
            args.putString(EaseConstant.EXTRA_USER_ID, EMName);
            args.putString("EMurl", EMurl);
            args.putString("EMNickName", EMNickName);
            chatFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().add(R.id.ll_fragment, chatFragment).commit();
        } else if (getIntent().hasExtra(EaseConstant.EXTRA_USER_ID)) {
            String name = getIntent().getStringExtra(EaseConstant.EXTRA_USER_ID);
            EaseChatFragment chatFragment = new EaseChatFragment();
            Bundle args = new Bundle();
            args.putInt(EaseConstant.EXTRA_CHAT_TYPE, EaseConstant.CHATTYPE_SINGLE);
            args.putString(EaseConstant.EXTRA_USER_ID, name);
            args.putString("EMurl", EMurl);
            args.putString("EMNickName", EMNickName);
            chatFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().add(R.id.ll_fragment, chatFragment).commit();
        }


    }

}
