package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.application.EnjoyorApplication;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.entitry.MyGuide;
import com.jkb.enjoyor.baledoctor.protocol.Server;
import com.jkb.enjoyor.baledoctor.utils.StringUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by YuanYuan on 2016/10/12.
 * 用户创建指导页面
 */
public class GuideActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    private int type = 0;
    private Bundle bundle;
    @Bind(R.id.guide_ac_img_logo)
    ImageView guide_ac_img_logo;
    @Bind(R.id.guide_ac_img_bp_up)
    ImageView guide_ac_img_bp_up;
    @Bind(R.id.guide_ac_img_bp_down)
    ImageView guide_ac_img_bp_down;
    @Bind(R.id.guide_ac_img_kongfu)
    ImageView guide_ac_img_kongfu;
    @Bind(R.id.guide_ac_img_wancanhou)
    ImageView guide_ac_img_wancanhou;
    @Bind(R.id.guide_ac_img_shuiqian)
    ImageView guide_ac_img_shuiqian;
    @Bind(R.id.guide_ac_img_wancanqian)
    ImageView guide_ac_img_wancanqian;
    @Bind(R.id.guide_ac_tv_name)
    TextView guide_ac_tv_name;
    @Bind(R.id.guide_ac_tv_time)
    TextView guide_ac_tv_time;
    @Bind(R.id.guide_ac_tv_sex)
    TextView guide_ac_tv_sex;
    @Bind(R.id.guide_ac_tv_age)
    TextView guide_ac_tv_age;
    @Bind(R.id.guide_ac_tv_shuzhangya)
    TextView guide_ac_tv_shuzhangya;
    @Bind(R.id.guide_ac_tv_shousuoya)
    TextView guide_ac_tv_shousuoya;
    @Bind(R.id.guide_ac_tv_kongfu)
    TextView guide_ac_tv_kongfu;
    @Bind(R.id.guide_ac_tv_wancanhou)
    TextView guide_ac_tv_wancanhou;
    @Bind(R.id.guide_ac_tv_shuiqian)
    TextView guide_ac_tv_shuiqian;
    @Bind(R.id.guide_ac_tv_wancanqian)
    TextView guide_ac_tv_wancanqian;
    @Bind(R.id.guide_ac_tv_allenergency)
    TextView guide_ac_tv_allenergency;
    @Bind(R.id.guide_ac_tv_jkxjiao)
    TextView guide_ac_tv_jkxjiao;
    @Bind(R.id.guide_ac_tv_shiliangdanbaizhi)
    TextView guide_ac_tv_shiliangdanbaizhi;
    @Bind(R.id.guide_ac_tv_sportall)
    TextView guide_ac_tv_sportall;
    @Bind(R.id.guide_ac_tv_danbaizhi)
    TextView guide_ac_tv_danbaizhi;
    @Bind(R.id.tv_normal)
    TextView tv_normal;
    @Bind(R.id.fl_abnormal)
    FrameLayout fl_abnormal;
    @Bind(R.id.guide_ac_cb_danbaizhi)
    CheckBox guide_ac_cb_danbaizhi;
    @Bind(R.id.guide_ac_cb_sportlimit)
    CheckBox guide_ac_cb_sportlimit;
    @Bind(R.id.guide_ac_cb_sport_danbaizhi)
    CheckBox guide_ac_cb_sport_danbaizhi;
    @Bind(R.id.guide_ac_cb_limit)
    CheckBox guide_ac_cb_limit;
    @Bind(R.id.guide_ac_li_xuanjiao)
    LinearLayout guide_ac_li_xuanjiao;
    @Bind(R.id.guide_ac_li_limit)
    LinearLayout guide_ac_li_limit;
    @Bind(R.id.guide_ac_li_limitdanbaizhi)
    LinearLayout guide_ac_li_limitdanbaizhi;
    @Bind(R.id.guide_ac_li_sport)
    LinearLayout guide_ac_li_sport;
    @Bind(R.id.guide_ac_li_danbaizhi)
    LinearLayout guide_ac_li_danbaizhi;
    Intent intent;
    @Bind(R.id.guide_ac_tv_limit)
    TextView guide_ac_tv_limit;
    @Bind(R.id.guide_ac_tv_danbai)
    TextView guide_ac_tv_danbai;
    @Bind(R.id.guide_limit_tv)
    TextView guide_limit_tv;
    @Bind(R.id.guide_tv_baizhi)
    TextView guide_tv_baizhi;
    @Bind(R.id.guide_li_sport)
    LinearLayout guide_li_sport;
    @Bind(R.id.guide_li_danbaizhi)
    LinearLayout guide_li_danbaizhi;
    @Bind(R.id.guide_li_xuanjiao)
    LinearLayout guide_li_xuanjiao;
    @Bind(R.id.guide_li_bp)
    LinearLayout guide_li_bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide_ac_layout);
        ButterKnife.bind(this);
        intent = getIntent();
        initview();
        initClick();
        initShow();
    }

    private void initShow() {
        initData();
    }

    private void initData() {
        long guiduId = intent.getIntExtra("guiduId", -1);
//        Log.d("guidu", guiduId + "");
        Subscription s;
        s = Server.GuideProtocolBuild(this).getMyGuide(guiduId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        data -> {
                            dealWithResponse(data);
                        },
                        e -> {
                            onError(e);
                        }
                );
        addSubscription(s);
    }

    @Override
    public void onResult(Object o) {
        super.onResult(o);
        if (o instanceof MyGuide) {
            MyGuide myguide = (MyGuide) o;
            setterInfo(myguide);
        }

    }

    private void setterInfo(MyGuide myguide) {

        if (!StringUtils.isEmpty(myguide.getHeadImg())) {
            ImageLoader.getInstance().displayImage(Constant.TEST_URL + myguide.getHeadImg(), guide_ac_img_logo, EnjoyorApplication.option);
        }
        if (!StringUtils.isEmpty(myguide.getUsername())) {
            guide_ac_tv_name.setText("姓名 ：" + myguide.getUsername());
        }
        if (!StringUtils.isEmpty(myguide.getCreateTime())) {
            guide_ac_tv_time.setText(myguide.getCreateTime());
        }
        if (!StringUtils.isEmpty(myguide.getSex())) {
            guide_ac_tv_sex.setText(myguide.getSex());
        }
        if (myguide.getAge() != null) {
            guide_ac_tv_age.setText(myguide.getAge() + " 岁");
        }
        if (null != myguide.getRecordBP()) {
            if (null != myguide.getRecordBP().getDiastolicPressure()) {
                String pre = myguide.getRecordBP().getDiastolicPressure() + "";
                guide_ac_tv_shuzhangya.setText(pre);
                if (myguide.getRecordBP().getDiastolicPressure() > 140) {
                    guide_ac_img_bp_up.setImageResource(R.mipmap.hong);
                }
            }
            if (null != myguide.getRecordBP().getSystolicPressure()) {
                guide_ac_tv_shousuoya.setText(myguide.getRecordBP().getSystolicPressure() + "");
                if (myguide.getRecordBP().getSystolicPressure() < 90) {
                    guide_ac_img_bp_down.setImageResource(R.mipmap.lan);
                }
            }
        } else {
            guide_ac_img_bp_up.setVisibility(View.INVISIBLE);
            guide_ac_img_bp_down.setVisibility(View.INVISIBLE);
        }
        List<MyGuide.RecordBSesEntity> li = myguide.getRecordBSes();
        if (null != li) {
            List<MyGuide.RecordBSesEntity> list = myguide.getRecordBSes();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getBloodSugarType() == 1) {
                    if (!StringUtils.isEmpty(list.get(i).getBloodSugar())) {
                        guide_ac_tv_kongfu.setText(list.get(i).getBloodSugar());
                        if (Float.parseFloat(list.get(i).getBloodSugar()) > 7.0) {
                            guide_ac_img_kongfu.setImageResource(R.mipmap.hong);
                        }
                    }

                } else if (list.get(i).getBloodSugarType() == 9) {
                    if (!StringUtils.isEmpty(list.get(i).getBloodSugar())) {
                        guide_ac_tv_wancanhou.setText(list.get(i).getBloodSugar());
                        if (Float.parseFloat(list.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_wancanhou.setImageResource(R.mipmap.lan);
                        }
                    }
                } else if (list.get(i).getBloodSugarType() == 10) {
                    if (!StringUtils.isEmpty(list.get(i).getBloodSugar())) {
                        guide_ac_tv_shuiqian.setText(list.get(i).getBloodSugar());
                        if (Float.parseFloat(list.get(i).getBloodSugar()) > 10.0) {
                            guide_ac_img_shuiqian.setImageResource(R.mipmap.hong);
                        } else if (Float.parseFloat(list.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_shuiqian.setImageResource(R.mipmap.lan);
                        }
                    }

                } else if (list.get(i).getBloodSugarType() == 8) {
                    if (!StringUtils.isEmpty(list.get(i).getBloodSugar())) {
                        guide_ac_tv_wancanqian.setText(list.get(i).getBloodSugar());
                        if (Float.parseFloat(list.get(i).getBloodSugar()) > 10.0) {
                            guide_ac_img_wancanqian.setImageResource(R.mipmap.hong);
                        } else if (Float.parseFloat(list.get(i).getBloodSugar()) < 4.4) {
                            guide_ac_img_wancanqian.setImageResource(R.mipmap.lan);
                        }
                    }
                }
            }

        }
        if (myguide.getGuideItems().size() > 0) {
            List<MyGuide.GuideItemsEntity> list = myguide.getGuideItems();
            List<MyGuide.GuideItemsEntity> list1 = new ArrayList<>();
            List<MyGuide.GuideItemsEntity> list2 = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {
                if ("hypertension".equals(list.get(i).getType())) {
                    list1.add(list.get(i));

                } else if ("diabetesmellitus".equals(list.get(i).getType())) {
                    list2.add(list.get(i));
                }
            }
            if (null != list1) {

                List<MyGuide.GuideItemsEntity> list11 = new ArrayList<>();
                List<MyGuide.GuideItemsEntity> list12 = new ArrayList<>();
                List<MyGuide.GuideItemsEntity> list13 = new ArrayList<>();
                for (int i = 0; i < list1.size(); i++) {
                    if (list1.get(i).getItemType() != null) {
                        if (list1.get(i).getItemType() == 1) {
                            list11.add(list1.get(i));
                        } else if (list1.get(i).getItemType() == 2) {
                            list12.add(list1.get(i));
                        } else if (list1.get(i).getItemType() == 3) {
                            list13.add(list1.get(i));
                        }
                    }
                }
                if (list11.size() == 1) {
                    guide_li_sport.setVisibility(View.VISIBLE);
                    guide_ac_li_sport.setVisibility(View.VISIBLE);
                    list11.get(0).getKey();
                    list11.get(0).getVal();
                    guide_ac_tv_limit.setText(list11.get(0).getKey());
                    guide_ac_tv_allenergency.setText(list11.get(0).getVal());
                    guide_ac_cb_limit.setVisibility(View.INVISIBLE);

                } else if (list11.size() == 2) {
                    guide_li_sport.setVisibility(View.VISIBLE);
                    guide_ac_li_sport.setVisibility(View.VISIBLE);
                    guide_ac_li_danbaizhi.setVisibility(View.VISIBLE);
                    list11.get(0).getKey();
                    list11.get(0).getVal();
                    guide_ac_tv_limit.setText(list11.get(0).getKey());
                    guide_ac_tv_allenergency.setText(list11.get(0).getVal());
                    guide_ac_cb_limit.setVisibility(View.INVISIBLE);
                    list11.get(1).getKey();
                    list11.get(1).getVal();
                    guide_ac_tv_danbai.setText(list11.get(1).getKey());
                    guide_ac_tv_danbaizhi.setText(list11.get(1).getVal());
                } else {
                    guide_li_sport.setVisibility(View.GONE);
                }
                if (list12.size() == 0) {
                    guide_li_danbaizhi.setVisibility(View.GONE);

                } else if (list12.size() == 1) {
                    guide_li_danbaizhi.setVisibility(View.VISIBLE);
                    guide_ac_li_limit.setVisibility(View.VISIBLE);
                    guide_limit_tv.setText(list12.get(0).getKey());
                    guide_ac_tv_sportall.setText(list12.get(0).getVal());
                } else if (list12.size() == 2) {
                    guide_li_danbaizhi.setVisibility(View.VISIBLE);
                    guide_ac_li_limit.setVisibility(View.VISIBLE);
                    guide_limit_tv.setText(list12.get(0).getKey());
                    guide_ac_tv_sportall.setText(list12.get(0).getVal());
                    guide_tv_baizhi.setText(list12.get(1).getKey());
                    guide_ac_tv_shiliangdanbaizhi.setText(list12.get(1).getVal());
                    guide_ac_li_limitdanbaizhi.setVisibility(View.VISIBLE);
                }
                if (list13.size() == 0) {
                    guide_li_xuanjiao.setVisibility(View.GONE);
                } else if (list13.size() == 1) {
                    guide_ac_tv_jkxjiao.setText(list13.get(0).getVal());
                    guide_ac_li_xuanjiao.setVisibility(View.VISIBLE);
                }
            }
            //ta
        }
    }

    private void format() {
    }

    private void initClick() {
        re_back.setOnClickListener(this);
    }

    private void initview() {
        navigation_name.setText("我的指导");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.fl_normal:
                break;
            case R.id.fl_abnormal:
                break;
        }
    }
}
