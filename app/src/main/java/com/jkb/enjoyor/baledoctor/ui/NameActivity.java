package com.jkb.enjoyor.baledoctor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jkb.enjoyor.baledoctor.R;
import com.jkb.enjoyor.baledoctor.common.Constant;
import com.jkb.enjoyor.baledoctor.utils.ToastUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by YuanYuan on 2016/9/11.
 * 姓名设置页面
 */
public class NameActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.container)
    CoordinatorLayout container;
    @Bind(R.id.re_back)
    RelativeLayout re_back;
    @Bind(R.id.navigation_name)
    TextView navigation_name;
    @Bind(R.id.name_et)
    EditText name_et;
    @Bind(R.id.name_delete)
    ImageView name_delete;
    @Bind(R.id.tv_right)
    TextView tv_right;
private String name = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.name_ac_layout);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        if (name.length()<=5){
            name_et.setText(name);
        }else {
            name_et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
            name_et.setText(name);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvent();
    }

    private void initEvent() {
        re_back.setOnClickListener(this);
        navigation_name.setText("姓名");
        tv_right.setText("保存");
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setOnClickListener(this);
        name_delete.setOnClickListener(this);
        nameetlistener();
    }

    private void nameetlistener() {
        name_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    name_delete.setVisibility(View.VISIBLE);
                } else
                    name_delete.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_back:
                finish();
                break;
            case R.id.tv_right:
//                ToastUtils.showToast(NameActivity.this, "您点击了保存姓名按钮");
                saveName();
                break;
            case R.id.name_delete:
                name_et.setText("");
                break;
        }
    }

    private void saveName() {
        String name = name_et.getText().toString().trim();
        if(TextUtils.isEmpty(name)){
            Snackbar.make(container, "姓名不能为空！", Snackbar.LENGTH_SHORT).show();
        }else{
            if (checkNameChese(name_et.getText().toString().trim())){
                Intent intent = getIntent();
                intent.putExtra("name", name_et.getText().toString().trim());
                setResult(Constant.NAME, intent);
                finish();
            }else {
                ToastUtils.showToast(this,"姓名只能是中文");
            }
        }
    }
    /*
    * 判断字符串是不是中文*/
    public boolean checkNameChese(String name) {
        boolean res = true;
        char[] cTemp = name.toCharArray();
        for (int i = 0; i < name.length(); i++) {
            if (!isChinese(cTemp[i])) {
                res = false;
                break;
            }
        }
        return res;

    }
    public boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }
}
